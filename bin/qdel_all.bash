id1=`qstat | grep $USER | head -1 | awk '{print $1}'`
id2=`qstat | grep $USER | tail -1 | awk '{print $1}'`

id1=${id1%\.*}
id2=${id2%\.*}

for i in `seq $id2 -1 $id1 ` ; do
 echo $i
 qdel $i 
done
