program inisnow


integer,parameter :: mx1=130 
integer,parameter :: my1=144
real,parameter    :: dx1=50

integer,parameter :: mx2=100 ! iceland
integer,parameter :: my2=90
real,parameter    :: dx2=7.5

!integer,parameter :: mx2=140 ! ellesmere
!integer,parameter :: my2=340
!real,parameter    :: dx2=7.5

integer,parameter :: mz=30

integer,parameter :: imez=68
integer,parameter :: jmez=68

real,parameter    :: pi   = 3.141592

real lat1(mx1,my1),lat2(mx2,my2),lon1(mx1,my1),lon2(mx2,my2),msk1(mx1,my1),msk2(mx2,my2)
real sh1(mx1,my1),sh2(mx2,my2),tmp2(mx2,my2,mz),ice1(mx1,my1),ice2(mx2,my2),ro(mx1,my1)
real tmp1(mx1,my1,mz),tmp4(mx2,my2,mz),type1(mx1,my1),type2(mx2,my2)
real distance,dist,min_dist,dimval(500),dist2,min_ro
real NS1(mx1,my1),ni1(mx1,my1),ns2(mx2,my2)

integer i,j,k,l,m,n,o,p,kk1,kk2,ll1,ll2,yyyy
integer kk(mx2,my2),ll(mx2,my2)


character*4   yyyyc
character*100 fnamNC,fnamNC2

fnamNC="ICE.20070901.GRu.nc"
print *,"use of "//fnamNC
call CF_READ2D(fnamNC,'LON',1,mx1,my1,1,lon1)
call CF_READ2D(fnamNC,'LAT',1,mx1,my1,1,lat1)
call CF_READ2D(fnamNC,'SH' ,1,mx1,my1,1, sh1)
call CF_READ2D(fnamNC,'MSK',1,mx1,my1,1,ice1)
call CF_READ2D(fnamNC,'SRF',1,mx1,my1,1,msk1)

!fnamNC="NST_7_5km_ellesmere.nc"
fnamNC="NST_7_5km_iceland.nc"
print *,"use of "//fnamNC
call CF_READ2D(fnamNC,'LON',1,mx2,my2,1,lon2)
call CF_READ2D(fnamNC,'LAT',1,mx2,my2,1,lat2)
call CF_READ2D(fnamNC,'SH' ,1,mx2,my2,1, sh2)
call CF_READ2D(fnamNC,'SFR',1,mx2,my2,1,ice2)
call CF_READ2D(fnamNC,'SVT',1,mx2,my2,1,msk2)

do i=1,mx2 ; do j=1,my2
 if(msk2(i,j)/=13) ice2(i,j)=0
enddo      ; enddo

do i=1,mx1 ; do j=1,my1
                   type1(i,j)=0
 if(ice1(i,j)>0) then
                   type1(i,j)=1     
 endif

enddo      ; enddo


do i=1,mx2 ; do j=1,my2

                   type2(i,j)=0
 if(ice2(i,j)>0) then
                   type2(i,j)=1     
 endif

enddo      ; enddo

lon1 = lon1 * pi/180.
lat1 = lat1 * pi/180.

lon2 = lon2 * pi/180.
lat2 = lat2 * pi/180.

kk1=1 ; kk2=mx1
ll1=1 ; ll2=my1

kk=0 ; ll=0

do i=1,mx2 ; do j=1,my2

 if(type2(i,j)>0) then
  
  min_dist=100000 ; min_ro=1000 ;  dist2=(dx1+dx2)/2.

  1000 continue

  do k=kk1,kk2; do l=ll1,ll2
   dist = distance(lon1(k,l),lat1(k,l),lon2(i,j),lat2(i,j))
   if(dist<=min_dist.and.type1(k,l)==type2(i,j)) then
     min_dist=dist
     min_ro  =ro(k,l)
     kk(i,j) = k
     ll(i,j) = l
   endif
  enddo     ; enddo

! kk1=max(1,min(mx1,kk(i,j)-10))
! kk2=max(1,min(mx1,kk(i,j)+10))

! ll1=max(1,min(my1,ll(i,j)-10))
! ll2=max(1,min(my1,ll(i,j)+10))

  if(min_dist>=100000) then

  kk1=1 ; kk2=mx1
  ll1=1 ; ll2=my1

  do k=kk1,kk2; do l=ll1,ll2
   dist = distance(lon2(i,j),lat2(i,j),lon1(k,l),lat1(k,l))
   if(dist<=min_dist.and.type1(k,l)==type2(i,j)) then
     min_dist=dist
     kk(i,j) = k
     ll(i,j) = l
   endif
  enddo     ; enddo

  endif

  print *,i,j,kk(i,j),ll(i,j), min_dist
 
 else
     kk(i,j) = -1
     ll(i,j) = -1
 endif

enddo      ; enddo

!open(unit=10,form='unformatted',file="index.DAT",status="replace")
!write(10) kk,ll
!close(10)


!open(unit=10,form='unformatted',file="index.DAT",status="old")
!read(10) kk,ll
!close(10)

lon2 = lon2 / pi*180.
lat2 = lat2 / pi*180.


do yyyy=1979,2014

 print *,yyyy

write(YYYYc,'(i4)') yyyy

fnamNC="MARini-GR7_5km-100x90-"//YYYYc//".cdf"

call CF_INI_FILE(fnamNC,fnamNC)

call CF_CREATE_DIM("time","-",1,1.)

DO i=1,mx2
 dimval(i)= (i-imez)*dx2
ENDDO
call CF_CREATE_DIM("x","km",mx2,dimval)


DO j=1,my2
 dimval(j)= (j-jmez)*dx2
ENDDO
call CF_CREATE_DIM("y","km",my2,dimval)

DO j=1,mz
 dimval(j)= real(j)
ENDDO
call CF_CREATE_DIM("zz","snow level",mz,dimval)

call CF_CREATE_VAR("LON","Longitude","degrees","-","x","y","-" )
call CF_CREATE_VAR("LAT","Latitude" ,"degrees","-","x","y","-" )
call CF_CREATE_VAR("SH1","surface height","-","-","x","y","-" )
call CF_CREATE_VAR("SH2","surface height","-","-","x","y","-" )

call CF_CREATE_VAR("NS1","Nbr of Snow Layers","-","-","x","y","-" )
call CF_CREATE_VAR("NI1","Nbr of ICE Layers","-","-","x","y","-" )

call CF_CREATE_VAR("DZ1","Snow Depth","-","time","x","y","zz" )
call CF_CREATE_VAR("NH1","Snow Historics","-","time","x","y","zz" )
call CF_CREATE_VAR("G11","Snow g1","-","time","x","y","zz" )
call CF_CREATE_VAR("G21","Snow g2","-","time","x","y","zz" )
call CF_CREATE_VAR("RO1","Snow Density","-","time","x","y","zz" )
call CF_CREATE_VAR("TI1","Snow Temperature","-","time","x","y","zz" )
call CF_CREATE_VAR("WA1","Snow Humidity Content","-","time","x","y","zz" )

call CF_CREATE_FILE(fnamNC)

call CF_write (fnamNC, 'LON '   , 1  , mx2   , my2, 1 , lon2)
call CF_write (fnamNC, 'LAT '   , 1  , mx2   , my2, 1 , lat2)


fnamNC2="ICE."//YYYYc//"0901.GRu.nc"

call CF_READ2D(fnamNC2,'nSSN',1,mx1,my1,1,ns1)
call CF_READ2D(fnamNC2,'nISN',1,mx1,my1,1,ni1)

call interpol(mx1,my1,mx2,my2,1,kk,ll,ns1,ns2)
call CF_write (fnamNC, 'NS1',1,mx2,my2,mz,ns2)

call interpol(mx1,my1,mx2,my2,1,kk,ll,ni1,ns2)
call CF_write (fnamNC, 'NI1',1,mx2,my2,mz,ns2)

call CF_READ3D(fnamNC2,'dzSN1',1,mx1,my1,mz,tmp1)
call interpol(mx1,my1,mx2,my2,mz,kk,ll,tmp1,tmp2)
call CF_write (fnamNC, 'DZ1',1,mx2,my2,mz,tmp2)

call CF_READ3D(fnamNC2,'nhSN1',1,mx1,my1,mz,tmp1)
call interpol(mx1,my1,mx2,my2,mz,kk,ll,tmp1,tmp2)
call CF_write (fnamNC, 'NH1',1,mx2,my2,mz,tmp2)

call CF_READ3D(fnamNC2,'g1SN1',1,mx1,my1,mz,tmp1)
call interpol(mx1,my1,mx2,my2,mz,kk,ll,tmp1,tmp2)
call CF_write (fnamNC, 'G11',1,mx2,my2,mz,tmp2)

call CF_READ3D(fnamNC2,'g2SN1',1,mx1,my1,mz,tmp1)
call interpol(mx1,my1,mx2,my2,mz,kk,ll,tmp1,tmp2)
call CF_write (fnamNC, 'G21',1,mx2,my2,mz,tmp2)

call CF_READ3D(fnamNC2,'roSN1',1,mx1,my1,mz,tmp1)
call interpol(mx1,my1,mx2,my2,mz,kk,ll,tmp1,tmp2)
do i=1,mx2 ; do j=1,my2 ; do k=1,mz
 if(type2(i,j)>0) then
  if(tmp2(i,j,k)>0) then 
   tmp2(i,j,k)=min(950.,max(tmp2(i,j,k),100.))
  else
   tmp2(i,j,k)=300.
  endif
 endif 
enddo      ; enddo      ; enddo
call CF_write (fnamNC, 'RO1',1,mx2,my2,mz,tmp2)

call CF_READ3D(fnamNC2,'tiSN1',1,mx1,my1,mz,tmp1)
call interpol(mx1,my1,mx2,my2,mz,kk,ll,tmp1,tmp2)
do i=1,mx2 ; do j=1,my2 ; do k=1,mz
 if(type2(i,j)>0) then
  if(tmp2(i,j,k)>-200) then 
   tmp2(i,j,k)=min(273.,max(tmp2(i,j,k)+273.15,220.))
  else
   tmp2(i,j,k)=273.
  endif
 endif 
enddo      ; enddo      ; enddo
call CF_write (fnamNC, 'TI1',1,mx2,my2,mz,tmp2)

call CF_READ3D(fnamNC2,'waSN1',1,mx1,my1,mz,tmp1)
call interpol(mx1,my1,mx2,my2,mz,kk,ll,tmp1,tmp2)
call CF_write (fnamNC, 'WA1',1,mx2,my2,mz,tmp2)


call CF_write (fnamNC, 'SH2'    , 1  , mx2   , my2, 1 , sh2)

call interpol(mx1,my1,mx2,my2,1,kk,ll,sh1,tmp2)

call CF_write (fnamNC, 'SH1'    , 1  , mx2   , my2, 1 , tmp2)

enddo

end program

!--------------------------------------------------------------------------------

! distance

function distance(lon2,lat2,lon1,lat1)	

implicit none

 real :: lon1,lat1
 real :: lon2,lat2
 real :: dlat,dlon,a,c,distance
 real,parameter    :: R    = 6371.

! distance = acos(sin(lat1) * sin(lat2) + &
!	    cos(lat1) * cos(lat2) * cos(lon2 - lon1)) * R

    dlat = (lat2-lat1)
    dlon = (lon2-lon1)
       a =  sin(dLat/2.) * sin(dLat/2.) + cos(lat1) * cos(lat2) * sin(dLon/2) * sin(dLon/2)
       c =  2.d0 * atan2(sqrt(a), sqrt(1-a))
distance =  R * c

end function


!--------------------------------------------------------------------------------


subroutine interpol(mx1,my1,mx2,my2,mz,kk,ll,var1,var2)

implicit none

integer mx1,my1,mx2,my2,mz
integer kk(mx2,my2),ll(mx2,my2)
real var1(mx1,my1,mz),var2(mx2,my2,mz)
integer i,j,k

do k=1,mz
do i=1,mx2 ; do j=1,my2
 if(kk(i,j)>0.and.ll(i,j)>0) then
  var2(i,j,k)=var1(kk(i,j),ll(i,j),k)
 endif
enddo ; enddo 
enddo

end subroutine
