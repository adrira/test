#!/bin/bash

export OMP_NUM_THREADS=6

Dom=y ; Run=1

for YYYY in `seq 1981 2016` ; do


#                      Run="10"
#[ $YYYY -ge 1956 ] && Run="11"
#[ $YYYY -ge 1966 ] && Run="22"
#[ $YYYY -ge 1976 ] && Run="13"
#[ $YYYY -ge 1986 ] && Run="14"
#![ $YYYY -ge 1996 ] && Run="15"
#![ $YYYY -ge 2006 ] && Run="16"

Run=${Run#0}
Run=$(( $Run + 1 ))
[ $Run -le 9 ] && Run=0$Run


rm -rf               MAR-GR${Dom}-${Dom}${Run}-$YYYY &> /dev/null
cp -r   MAR-GR${Dom} MAR-GR${Dom}-${Dom}${Run}-$YYYY
cd                   MAR-GR${Dom}-${Dom}${Run}-$YYYY

cp ~/MAR/out/GR${Dom}/input/NESTOR/$YYYY/*.09.01-30.DAT.gz .
Rename "_GR${Dom}.$YYYY.09.01-30" ""
tar xzf ~/MAR/out/GR${Dom}/input/MARsim/${Dom}${Run}/$YYYY/MARsim_${Dom}${Run}.$YYYY.09.01.DAT.tar.gz
cp ~/MAR/out/GR${Dom}/${Dom}${Run}/code/MAR_${Dom}${Run}.exe MAR_${Dom}${Run}.exe
cp ~/MAR/sim/GR${Dom}/input/MARdom/${Dom}${Run}/MARdom_*  MARdom.dat.gz
gunzip *.gz
ulimit -s unlimited
time ./MAR_${Dom}${Run}.exe  &> MAR_${Dom}${Run}.log 

cd ..
done

