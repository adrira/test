
C +  ==========
C +--MAR_WK.inc
C +  ==========

      real          WKxyz1           ,WKxyz2           ,
     &              WKxyz3           ,WKxyz4           ,
     &              WKxyz5           ,WKxyz6           ,
     &              WKxyz7           ,WKxyz8           
      common/wkxyz/ WKxyz1(mx,my,mz ),WKxyz2(mx,my,mz ),
     &              WKxyz3(mx,my,mz ),WKxyz4(mx,my,mz ),
     &              WKxyz5(mx,my,mzz),WKxyz6(mx,my,mzz),
     &              WKxyz7(mx,my,mzz),WKxyz8(mx,my,mzz)
      real          WKxy1           ,WKxy2           ,WKxy3
     &            , WKxy4           ,WKxy5           ,WKxy6
     &            , WKxy7           ,WKxy8           ,WKxy9
      common/wkxy/  WKxy1 (mx,my)   ,WKxy2 (mx,my)   ,WKxy3 (mx,my)
     &            , WKxy4 (mx,my)   ,WKxy5 (mx,my)   ,WKxy6 (mx,my)
     &            , WKxy7 (mx,my)   ,WKxy8 (mx,my)   ,WKxy9 (mx,my)
      real          WKxza           ,WKxzb           ,WKxzc
     &            , WKxzd           ,WKxzx
     &            , WKxzp           ,WKxzq
      common/wkxz/  WKxza (mx,mz)   ,WKxzb (mx,mz)   ,WKxzc (mx,mz)
     &            , WKxzd (mx,mz)   ,WKxzx (mx,mz)
     &            , WKxzp (mx,mz)   ,WKxzq (mx,mz)
C +...wkXXX variables define a work area 
C +         in order to minimize memory requirements
