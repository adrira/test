#!/bin/bash

cp bkp/* .

for file in y*.F90 ; do

 file2=${file%.F90}.f90

 size=`wc -l $file | awk '{print $1}'`
 
 head -n $(( $size -1 )) $file    > $file2
 echo "!\$OMP threadprivate ( &" >> $file2

 i=0

grep INTEGER_M $file | while read line
do
 line1=$line
 line2=${line%PARAMETER*}
 line=${line##*\:\:\ }
 line=${line%\=*}
 line=${line%\(*}
 if [ $i -eq 0 ] ; then 
  [ ${#line1} -eq ${#line2} ] &&  echo "!\$OMP   $line &" >> $file2  && i=1
 else
  [ ${#line1} -eq ${#line2} ] &&  echo "!\$OMP , $line &" >> $file2  
 fi
done

grep REAL_B $file | while read line
do
 line1=$line
 line2=${line#\!}
 line=${line##*\:\:\ }
 line=${line%\=*}
 line=${line%\(*}
 if [ $i -eq 0 ] ; then 
  [ ${line1:0:1} == "R" ] && echo "!\$OMP   $line &" >> $file2  && i=1
 else
  [ ${line1:0:1} == "R" ] && echo "!\$OMP , $line &" >> $file2  
 fi
done 

 echo "!\$OMP )" >> $file2

 tail -n 1 $file >> $file2 

 mv $file2 $file

 [ -f ok/$file ] && cp ok/$file .

done
