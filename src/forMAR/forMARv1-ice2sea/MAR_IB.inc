
C +  ==========
C +--MAR_IB.inc (05/07/2004)
C +  ==========


      integer    OutdyIB                            ! Nbr Outputs by Day  
      parameter (OutdyIB=1)  

      integer    ml                                 ! Nbr levels for atmosph. var.
      parameter (ml=mz)

      integer    ml2                                ! Nbr levels for atmosph. var. for 6-hourly output
      parameter (ml2=3)
      
      integer    mi                                 ! Nbr snow height levels 
      parameter (mi=12)

      real       OutshIB(mi)                        ! Snow height levels
      data       OutshIB/0.01,0.05, 0.1,0.25, 0.5, 0.75,
     .                    1.0,1.25, 1.5, 2.0, 5.0,10.0/   


C +--Surface Mass Balance
C +  --------------------

      real     wet_IB(mx,my,nsx),wet0IB(mx,my,nsx)  ! Total                    (mmWE)
      real     wes_IB(mx,my,nsx),wes0IB(mx,my,nsx)  ! Sublimation              (mmWE)
      real     wee_IB(mx,my,nsx),wee0IB(mx,my,nsx)  ! Evapotranspiration       (mmWE)
      real     wem_IB(mx,my,nsx),wem0IB(mx,my,nsx)  ! Melting                  (mmWE)
      real     wer_IB(mx,my,nsx),wer0IB(mx,my,nsx)  ! Refreezing               (mmWE)
      real     werr0IB(mx,my)                       ! Rain                     (mmWE)
      real     wesf0IB(mx,my)                       ! Snow                     (mmWE)
      real     wei0IB(mx,my)                        ! Bottom Ice Added         (mmWE) 
      real     wero0IB(mx,my)                       ! RunOff                   (mmWE)
      real     wesw0IB(mx,my,nsx)                   ! Surface Water            (mmWE)

C +--Atmospheric Variables averaged
C +  ------------------------------

      real     mintIB(mx,my,ml)                     ! Min. Temp.                  (C)
      real     maxtIB(mx,my,ml)                     ! Max. Temp.                  (C)
      real     ttIB(mx,my,ml)                       ! Temperature                 (C)
      real     uuIB(mx,my,ml)                       ! x-Wind Speed component    (m/s)
      real     vvIB(mx,my,ml)                       ! y-Wind Speed component    (m/s)
      real     qqIB(mx,my,ml)                       ! Specific Humidity        (g/kg)
      real     zzIB(mx,my,ml)                       ! Model Levels Height         (m)
      real     pddIB(mx,my,ml)                      ! PDD quantity                (C)    
      real     spIB(mx,my)                          ! Surface Pressure          (hPa)
      real     ccIB(mx,my)                          ! Cloud cover                 (-)
      real     codIB(mx,my)                         ! Cloud Optical Depth         (-) 
      real     qwIB(mx,my)                          ! Cl Dropplets Concent.   (kg/kg)
      real     qiIB(mx,my)                          ! Cl Ice Crystals Concent.(kg/kg)
      real     qsIB(mx,my)                          ! Snow Flakes Concent.    (kg/kg)
      real     qrIB(mx,my)                          ! Rain Concentration      (kg/kg)

      real     ttIB6H(mx,my,ml2,0:3) 		    ! 6-hourly Temperature	   (C) 
      real     qqIB6H(mx,my,ml2,0:3) 		    ! 6-hourly Specific Humidit(g/kg)
      real     uuIB6H(mx,my,ml2,0:3) 		    ! 6-hourly x-Wind Speed	 (m/s)
      real     vvIB6H(mx,my,ml2,0:3) 		    ! 6-hourly y-Wind Speed	 (m/s)
      real     zzIB6H(mx,my,ml2,0:3) 		    ! 6-hourly Model Levels	   (m) 
      real     spIB6H(mx,my,0:3)    		    ! 6-hourly Surface Pression  (hPa) 
      real     stIB6H(mx,my,nsx,0:3)		    ! 6-hourly Surf. Temp.	   (C)


C +--Surface Variables averaged
C +  --------------------------

      real     swdIB(mx,my)                         ! Shortwave inc. Rad.      (W/m2)
      real     lwdIB(mx,my)                         ! Longwave  inc. Rad.      (W/m2)
      real     lwuIB(mx,my)                         ! Longwave  out. Rad.      (W/m2)
      real     shfIB(mx,my)                         ! Sensible  Heat           (W/m2)
      real     lhfIB(mx,my)                         ! Latent    Heat           (W/m2)
      real     al1IB(mx,my)                         ! Albedo (SW out/SW in)       (-)
      real     al2IB(mx,my)                         ! Albedo (temporal mean)      (-)
      real     siIB(mx,my,nsx)                      ! Superimposed Ice            (m)
      real     suIB(mx,my,nsx)                      ! Slush                       (m)
      real     stIB(mx,my,nsx)                      ! Surface Temperature         (m)
      real     z0IB(mx,my,nsx)                      ! Roughness length for Moment.(m)
      real     r0IB(mx,my,nsx)                      ! Roughness length for Heat   (m)
      real     uusIB(mx,my,nsx)                     ! Friction Velocity         (m/s)
      real     utsIB(mx,my,nsx)                     ! Sfc Pot. Tp. Turb. Flux (K.m/s)
      real     uqsIB(mx,my,nsx)                     ! Water Vapor Flux    (kg/kg.m/s)
      real     ussIB(mx,my,nsx)                     ! Blowing Snow Flux   (kg/kg.m/s)
      real     sltIB(mx,my,nsx,nsol+1)              ! Soil Temperature            (C)
      real     slqIB(mx,my,nsx,nsol+1)              ! Soil Humidity Content    (g/kg)


C +--Snow pack Variables averaged
C +  ----------------------------

      real     dzIB(mx,my,nsx,mi)                    ! Effectiv Snow Height       (m)
      real     g1IB(mx,my,nsx,mi)                    ! g1                         (-)
      real     g2IB(mx,my,nsx,mi)                    ! g2                         (-)
      real     lcIB(mx,my,nsx,mi)                    ! Water Content (in % by volume)
      real     roIB(mx,my,nsx,mi)                    ! Density                (kg/m3)
      real     tiIB(mx,my,nsx,mi)                    ! Temperature                (C)
      real     waIB(mx,my,nsx,mi)                    ! Water Content              (%)  
      real     zn0IB(mx,my,nsx),zn1IB(mx,my,nsx)     ! Snow Height                (m)
      real     zn2IB(mx,my,nsx),zn3IB(mx,my,nsx)     !                                
      real     mb0IB(mx,my,nsx),mbIB (mx,my,nsx)     ! Mass Balance            (mmWE)
      real     siiceIB(mx,my,nsx)                    ! Superimposed Ice Height    (m)
      real     slushIB(mx,my,nsx)                    ! Slush Height               (m)

      integer  nsiiIB (mx,my,nsx)                    ! Nbr of Superimposed Ice Layer
      integer  nsluIB (mx,my,nsx)                    ! Nbr of Slush Layer
      integer  itrdIB 

      common /srfimb/ wet_IB, wet0IB, wes_IB, wes0IB, wem_IB, wem0IB
     .              ,wesw0IB, wer_IB, wer0IB,werr0IB,wesf0IB, wei0IB
     .              ,wero0IB, mintIB, maxtIB,   ttIB,   uuIB,   vvIB
     .              ,   qqIB,  pddIB,  slqIB,  swdIB,  lwdIB,  lwuIB  
     .              ,  shfIB,  lhfIB,   ccIB,  codIB,  al1IB,  al2IB 
     .              ,   siIB,   suIB,siiceIB,slushIB,   dzIB,   g1IB 
     .              ,   g2IB,   lcIB,   roIB,   tiIB,   waIB,  zn0IB
     .              ,  mb0IB,   mbIB,  zn1IB,  zn2IB,  zn3IB,   spIB  
     .              ,   stIB,   z0IB,   r0IB,  uusIB,  utsIB,  uqsIB  
     .              ,  ussIB,  sltIB,   qwIB,   qiIB,   qsIB,   qrIB
     .              ,   zzIB, wee0IB, wee_IB
     .              ,  ttIB6H,qqIB6H,uuIB6H,vvIB6H,zzIB6H,spIB6H,stIB6H

      common /srfimt/ nsiiIB, nsluIB, itrdIB                      

     
