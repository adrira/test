#! /bin/sh

# set -x

  echo " "
  echo "COMPILATION of MAR"
  echo "=================="
  echo " "
  echo -n " RADIATIVE Transfert          (LMDZ / ECMWF): "
  read RAD
  echo " "
  echo -n " COMPILER  (f77 / pgf90 / ifc / ifort / f90): "
  read foc
  echo " "
  echo -n " INCLUDE netcdf                    ( y , n ): "
  read cdf
  echo " "


  DETAILS=`echo NO`
  VERRADI=`echo NO`
  constat=`echo Pb`


if [ ${foc} = f77 ]; then
  echo -n " Optimization                    (DB / FAST): "
  read PX
 if [ ${PX} = DB ]; then
  OPTIONS=`echo -finit-local-zero -C -g`
  constat=`echo OK`
 fi
 if [ ${PX} = FAST ]; then
  OPTIONS=`echo -finit-local-zero -O3`
  constat=`echo OK`
 fi
  LIBRARY=`echo -L/usr/local/lib -lnetcdf`
  echo " "
fi


if [ ${foc} = pgf90 ]; then
  echo -n " Optimization                    (DB / FAST): "
  read PX
 if [ ${PX} = DB ]; then
  OPTIONS=`echo -C -g`
  constat=`echo OK`
 fi
 if [ ${PX} = FAST ]; then
  OPTIONS=`echo -fast -tp=p7`
  constat=`echo OK`
 fi
  LIBRARY=`echo -L/usr/local/lib -lnetcdf -llapack`
  echo " "
fi


if [ ${foc} = ifc ]; then
  echo -n " Optimization               (DB / P3  / P4 ): "
  read PX
 if [ ${PX} = DB ]; then
    echo "CAUTION: compiler errors probable in HYDadv_VER"
    echo "                                 and LMDZ rad routines"
  HOST=`hostname`
  if [ ${HOST} = lgge-pc140.obs.ujf-grenoble.fr ]; then
  OPTIONS=`echo -C -d2`
  else
  OPTIONS=`echo -w -d2`
  fi
  constat=`echo OK`
 fi
 if [ ${PX} = P3 ]; then
  OPTIONS=`echo -O3 -mp1 -w -zero     -tpp6`
  constat=`echo OK`
 fi
 if [ ${PX} = P4 ]; then
  OPTIONS=`echo -O3 -mp1 -w -zero -xW -tpp7`
  constat=`echo OK`
 fi
 if [ -f $CDFIFC ]; then
  LIBRARY=`echo $CDFIFC`
 else
  LIBRARY=`echo -L/usr/local/lib -lnetcdfifc`
 fi
  echo " "
fi


if [ ${foc} = ifort ]; then
. /usr/local/intel/compiler9/bin/ifortvars.sh
  echo -n " Optimization     (GC / MS / DB / O4  / P4 ): "
  read PX
 if [ ${PX} = DB ]; then
    echo "CAUTION: compiler errors possible in HYDadv_VER"
    echo "                                 and LMDZ rad routines"
  HOST=`hostname`
  if [ ${HOST} = lgge-pc140.obs.ujf-grenoble.fr ]; then
  OPTIONS=`echo -zero -vec_report0 -fpe0 -fpstkchk -g -inline_debug_info`
  else
  OPTIONS=`echo -zero -vec_report0 -fpe0 -fpstkchk -g -inline_debug_info`
  fi
  constat=`echo OK`
 fi
 if [ ${PX} = GC ]; then
  OPTIONS=`echo     -O3      -w -zero -vec_report0`
  constat=`echo OK`
 fi
 if [ ${PX} = MS ]; then
  OPTIONS=`echo -xW -tpp7    -w -zero -vec_report0`
  constat=`echo OK`
 fi
 if [ ${PX} = O4 ]; then
  OPTIONS=`echo     -O4                           `
  constat=`echo OK`
 fi
 if [ ${PX} = P4 ]; then
  OPTIONS=`echo     -tpp7    -w -zero -vec_report0 -tune pn4 -arch pn4 -mp1 -O3 -prec_div -axW -xW -ip`
  constat=`echo OK`
 fi
 if [ -f $CDFIFC ]; then
  LIBRARY=`echo -L/usr/local/lib $CDFIFC     -L/usr/local/intel/mkl721/lib/32 -lmkl_lapack -lmkl_ia32 -lguide -lpthread`
 else
  LIBRARY=`echo -L/usr/local/lib -lnetcdfifc -L/usr/local/intel/mkl721/lib/32 -lmkl_lapack -lmkl_ia32 -lguide -lpthread`
 fi
  echo " "
fi


if [ ${foc} = f90 ]; then
  echo -n " Optimization                    (DB  / O4 ): "
  read PX
  echo " "
 if [ ${PX} = DB ]; then
  OPTIONS=`echo -C -g`
  constat=`echo OK`
 fi
 if [ ${PX} = O4 ]; then
  OPTIONS=`echo -O4`
  constat=`echo OK`
 fi
  LIBRARY=`echo -L/usr/local/lib -lnetcdf`
fi

  datey=`date +%y`
  datem=`date +%m`
  dated=`date +%d`
  dateH=`date +%H`
  dateM=`date +%M`
  min_0=`echo 0`
  min_1=`echo 1`
  min_2=`echo 2`
  min_5=`echo 5`
  min10=`expr $dateM / 10`

  rm -f *~


if [ ${constat} = OK ]; then
 
echo " "
echo " "
echo "COMPILATION"
echo "==========="
echo " "
echo " "
if [ ${cdf} = y ]; then
if [ -f libUN.f ]; then
  echo "libUN.f        exists"
else
  echo "libUN.f    is copied NOW"
  cp    -p -f $MARONH/libMAR/libUNs_f libUN.f
fi
 
 
if [ -f NetCDF.inc ]; then
  echo "NetCDF.inc     exists"
else
  echo "NetCDF.inc is copied NOW"
 if [ -d 3D_IDL ]; then
  cp    -p -f           3D_IDL/dirnUN/NetCDF.inc        .
 else
  cp    -p -f   $MARhom/3D_IDL/dirnUN/NetCDF.inc        .
 fi
fi
fi


if [ -f MARmai.f ]; then
if [ -f MARmai.o ]; then
  echo "MARmai.o       exists"
else
  ${foc} ${OPTIONS} -c                  MARmai.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        MARmai.f
  if [ -f MARmai.o ]; then
    touch $dateT                        MARmai.o
  fi
fi
fi


if [ -f MAR___.f ]; then
if [ -f MAR___.o ]; then
  echo "MAR___.o       exists"
else
  ${foc} ${OPTIONS} -c                  MAR___.f 
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        MAR___.f
  if [ -f MAR___.o ]; then
    touch $dateT                        MAR___.o
  fi
fi
fi


if [ -f GRDmar.f ]; then
if [ -f GRDmar.o ]; then
  echo "GRDmar.o       exists"
else
  ${foc} ${OPTIONS} -c                  GRDmar.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        GRDmar.f
  if [ -f GRDmar.o ]; then
    touch $dateT                        GRDmar.o
  fi
fi
fi


if [ -f INIgen.f ]; then
if [ -f INIgen.o ]; then
  echo "INIgen.o       exists"
else
  ${foc} ${OPTIONS} -c                  INIgen.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        INIgen.f
  if [ -f INIgen.o ]; then
    touch $dateT                        INIgen.o
  fi
fi
fi


if [ -f INIsnd.f ]; then
if [ -f INIsnd.o ]; then
  echo "INIsnd.o       exists"
else
  ${foc} ${OPTIONS} -c                  INIsnd.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        INIsnd.f
  if [ -f INIsnd.o ]; then
    touch $dateT                        INIsnd.o
  fi
fi
fi


if [ -f INIsic.f ]; then
if [ -f INIsic.o ]; then
  echo "INIsic.o       exists"
else
  ${foc} ${OPTIONS} -c                  INIsic.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        INIsic.f
  if [ -f INIsic.o ]; then
    touch $dateT                        INIsic.o
  fi
fi
fi


if [ -d radCEP.d${PX} ] && [ ${VERRADI} = y ]; then
  echo " "
  echo "radCEP.d"${PX}"/M*.inc is verified NOW: "
  cd          radCEP.d${PX}
  rm -f                                              ../MARdif.sh
  for file in M*.inc
  do
    echo ' '                                      >> ../MARdif.sh
    echo ' '                                      >> ../MARdif.sh
    echo 'echo "'${file}'"'                       >> ../MARdif.sh
    echo 'diff radCEP.d'${PX}'/'${file}' '${file} >> ../MARdif.sh
  done
  cd                                                 ../.
  chmod u+x                                             MARdif.sh
                                                      ./MARdif.sh
  TOUCH                                                 MARdif.sh
fi


if [ ${RAD} = LMDZ ]; then
if [ -f PHYrad_CEP.o ]; then
  rm -f PHYrad_CEP.o
fi
if [ -f radlsw.F90 ]; then
echo " "
echo " Compilation of the ECMWF ERA-15 radiative transfert scheme, when the code is in the current directory"
echo " -----------------------------------------------------------------------------------------------------"
echo " "
if [ -f radlsw.o ]; then
  echo "radlsw.o       exists"
else
  ${foc} ${OPTIONS} -c                  radlsw.F90
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        radlsw.F90
  if [ -f radlsw.o ]; then
    touch $dateT                        radlsw.o
  fi
fi
fi


if [ -f PHYrad_LMD.f77 ]; then
     mv PHYrad_LMD.f77 PHYrad_LMD.f
fi
if [ -f PHYrad_LMD.f ]; then
if [ -f PHYrad_LMD.o ]; then
  echo "PHYrad_LMD.o       exists"
else
echo " "
echo " Compilation of the ECMWF ERA-15 radiative transfert scheme, interface routine PHYrad_LMD.f"
echo " ------------------------------------------------------------------------------------------"
echo " "
  ${foc} ${OPTIONS} -c                  PHYrad_LMD.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        PHYrad_LMD.f
  if [ -f PHYrad_LMD.o ]; then
    touch $dateT                        PHYrad_LMD.o
  fi
fi
fi
fi


if [ ${RAD} = ECMWF ]; then
  if [ -f PHYrad_LMD.o ]; then
    rm -f PHYrad_LMD.o
  fi
  if [ -d radCEP.d${PX} ]; then
      echo " "
      echo " Use         of the ECMWF ERA-40 radiative transfert scheme, compiled, linked to the present directory"
      echo " -----------------------------------------------------------------------------------------------------"
      echo " "
  else
    if [ -f PHYrad2CEP.F90 ] && [ -f radCEP.inc]; then
      echo " "
      echo " Compilation of the ECMWF ERA-40 radiative transfert scheme, when the code is in the current directory"
      echo " -----------------------------------------------------------------------------------------------------"
      echo " "
      echo " *** CAUTION: THIS IS CPU TIME CONSUMING ***"
      echo " "
      if [ -f PHYrad2CEP.o ]; then
        echo "PHYrad2CEP.o   exists"
      else
       if [ -f parkind1.F90 ]; then
         echo "parkind1.F90   exists"
       else
         cp -p radCEP.d/module.d_0/parkind1.F90 parkind1.F90
       fi
       if [ -f parrtm1d.F90 ]; then
         echo "parrtm1d.F90   exists"
       else
         cp -p radCEP.d/parrtm1d.F90            parrtm1d.F90 
       fi
       if [ -f yomcst.F90 ]; then
         echo "yomcst.F90     exists"
       else
         cp -p radCEP.d/module.d_1/yomcst.F90   yomcst.F90  
       fi
       if [ -f yoethf.F90 ]; then
         echo "yoethf.F90     exists"
       else
         cp -p radCEP.d/module.d_1/yoethf.F90   yoethf.F90  
       fi
       if [ -f yoerad.F90 ]; then
         echo "yoerad.F90     exists"
       else
         cp -p radCEP.d/module.d_1/yoerad.F90   yoerad.F90  
       fi
       if [ -f yoerdi.F90 ]; then
         echo "yoerdi.F90     exists"
       else
         cp -p radCEP.d/module.d_1/yoerdi.F90   yoerdi.F90  
       fi
       if [ -f yoerdu.F90 ]; then
         echo "yoerdu.F90     exists"
       else
         cp -p radCEP.d/module.d_1/yoerdu.F90   yoerdu.F90  
       fi
        ${foc} ${OPTIONS} -c                    parkind1.F90 parrtm1d.F90
       if [ -f radCEP.d${PX}/parkind1.o ]; then
        mv     radCEP.d${PX}/parkind1.o        radCEP.d${PX}/parkind1.o-
        touch $dateT                           radCEP.d${PX}
       fi
       if [ -f radCEP.d${PX}/parrtm1d.o ]; then
        mv     radCEP.d${PX}/parrtm1d.o        radCEP.d${PX}/parrtm1d.o-
        touch $dateT                           radCEP.d${PX}
       fi
        ${foc} ${OPTIONS} -c                      yomcst.F90   yoethf.F90 yoerad.F90 yoerdi.F90 yoerdu.F90
        ${foc} ${OPTIONS} -c                  PHYrad2CEP.F90
       if [ -f radCEP.d${PX}/yomcst.o ]; then
        mv     radCEP.d${PX}/yomcst.o          radCEP.d${PX}/yomcst.o-
        touch $dateT                           radCEP.d${PX}
       fi
       if [ -f radCEP.d${PX}/yoethf.o ]; then
        mv     radCEP.d${PX}/yoethf.o          radCEP.d${PX}/yoethf.o-
        touch $dateT                           radCEP.d${PX}
       fi
       if [ -f radCEP.d${PX}/yoerad.o ]; then
        mv     radCEP.d${PX}/yoerad.o          radCEP.d${PX}/yoerad.o-
        touch $dateT                           radCEP.d${PX}
       fi
       if [ -f radCEP.d${PX}/yoerdi.o ]; then
        mv     radCEP.d${PX}/yoerdi.o          radCEP.d${PX}/yoerdi.o-
        touch $dateT                           radCEP.d${PX}
       fi
       if [ -f radCEP.d${PX}/yoerdu.o ]; then
        mv     radCEP.d${PX}/yoerdu.o          radCEP.d${PX}/yoerdu.o-
        touch $dateT                           radCEP.d${PX}
       fi
       if [ -f radCEP.d${PX}/PHYrad2CEP.o ]; then
        mv     radCEP.d${PX}/PHYrad2CEP.o      radCEP.d${PX}/PHYrad2CEP.o-
        touch $dateT                           radCEP.d${PX}
       fi
        dateT=`echo $datem$dated$dateH$min10$min_0$datey`
          touch $dateT                          parkind1.F90 parrtm1d.F90
          touch $dateT                            yomcst.F90   yoethf.F90 yoerad.F90 yoerdi.F90 yoerdu.F90
          touch $dateT                        PHYrad2CEP.F90
        if [ -f PHYrad2CEP.o ]; then
          touch $dateT                          parkind1.o   parrtm1d.o
         if [ -f parkind1.mod ]; then
          touch $dateT                          parkind1.mod
         else
          if [ -f PARKIND1.mod ]; then
          touch $dateT                          PARKIND1.mod
          fi
         fi
         if [ -f parrtm1d.mod ]; then
          touch $dateT                          parrtm1d.mod
         else
          if [ -f PARRTM1D.mod ]; then
          touch $dateT                          PARRTM1D.mod
          fi
         fi
         if [ -f yomcst.mod ]; then
          touch $dateT                          yomcst.mod
         else
          if [ -f YOMCST.mod ]; then
          touch $dateT                          YOMCST.mod
          fi
         fi
         if [ -f yoerad.mod ]; then
          touch $dateT                          yoerad.mod
         else
          if [ -f YOERAD.mod ]; then
          touch $dateT                          YOERAD.mod
          fi
         fi
         if [ -f yoerdi.mod ]; then
          touch $dateT                          yoerdi.mod
         else
          if [ -f YOERDI.mod ]; then
          touch $dateT                          YOERDI.mod
          fi
         fi
         if [ -f yoerdu.mod ]; then
          touch $dateT                          yoerdu.mod
         else
          if [ -f YOERDU.mod ]; then
          touch $dateT                          YOERDU.mod
          fi
         fi
         if [ -f yoethf.mod ]; then
          touch $dateT                          yoethf.mod
         else
          if [ -f YOETHF.mod ]; then
          touch $dateT                          YOETHF.mod
          fi
         fi
          touch $dateT                            yomcst.o     yoethf.o   yoerad.o   yoerdi.o   yoerdu.o
          touch $dateT                        PHYrad2CEP.o
        fi
      fi
    else
      echo " "
      echo " YOU MUST LINK  the ECMWF ERA-40 radiative transfert scheme to the present directory"
      echo " -----------------------------------------------------------------------------------"
        echo " "
        echo " Dimension od the Domain: "
        echo " ^^^^^^^^^^^^^^^^^^^^^^^^ "
          head -n9 MARdim.inc
        echo " "
      if [ ${DETAILS} = y ]; then
        echo " Chooses       directory (writes fc if you want to compile the radiative transfert scheme): "
        echo " ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ "
          ls -1d  radCEP.d/radCEP*
          read               RADD
        if [ ${RADD} != fc ]; then
            ln -s  ${RADD} radCEP.d${PX}
            cp -p          radCEP.d${PX}/radCEP.inc   .
        fi
      else
        echo " Confirms Nb of vertical levels (please use the value of parameter mz, and answers with 3 digits: ex: 033 for 33): "
        read MZ
          if [ -d radCEP.d/radCEP_${MZ}.d${foc}_${PX} ]; then
            ln -s radCEP.d/radCEP_${MZ}.d${foc}_${PX} radCEP.d${PX}
            cp -p radCEP.d${PX}/radCEP.inc            .
            RADD=`echo OK`
          else
            RADD=`echo fc`
          fi
      fi
      if [ ${RADD} = fc ]; then
            cd    radCEP.d
                ./radCEP.sh 256 ${MZ} ${foc} ${PX}
            cp -p          radCEP_${MZ}.d${foc}_${PX}/radCEP.inc ../.
            cd ../.
            ln -s radCEP.d/radCEP_${MZ}.d${foc}_${PX} radCEP.d${PX}
#           cp -p radCEP.d${PX}/radCEP.inc            .
      fi
        echo " "
    fi
  fi


  if [ -f PHYrad_CEP.f ]; then
    if [ -f PHYrad_CEP.o ]; then
      echo "PHYrad_CEP.o   exists"
    else
    echo " "
    echo " Compilation of the ECMWF ERA-40 radiative transfert scheme, interface routine PHYrad_CEP.f"
    echo " ------------------------------------------------------------------------------------------"
    echo " "
      ${foc} ${OPTIONS} -c                  PHYrad_CEP.f
      if [ -f radCEP.d${PX}/PHYrad_CEP.o ]; then
        mv     radCEP.d${PX}/PHYrad_CEP.o     radCEP.d${PX}/PHYrad_CEP.o-
        touch $dateT                          radCEP.d${PX}
      fi
        dateT=`echo $datem$dated$dateH$min10$min_0$datey`
        touch $dateT                        PHYrad_CEP.f
      if [ -f PHYrad_CEP.o ]; then
        touch $dateT                        PHYrad_CEP.o
      fi
    fi
  else
    echo " "
    echo " Compilation of the ECMWF ERA-40 radiative transfert scheme: interface routine PHYrad_CEP.f IS MISSING"
    echo " -----------------------------------------------------------------------------------------------------"
    echo " "
  fi

fi


if [ -f DYNdps.f ]; then
if [ -f DYNdps.o ]; then
  echo "DYNdps.o       exists"
else
  ${foc} ${OPTIONS} -c                  DYNdps.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        DYNdps.f
  if [ -f DYNdps.o ]; then
    touch $dateT                        DYNdps.o
  fi
fi
fi


if [ -f DYNdmp.f ]; then
if [ -f DYNdmp.o ]; then
  echo "DYNdmp.o       exists"
else
  ${foc} ${OPTIONS} -c                  DYNdmp.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        DYNdmp.f
  if [ -f DYNdmp.o ]; then
    touch $dateT                        DYNdmp.o
  fi
fi
fi


if [ -f DYNdgz.f ]; then
if [ -f DYNdgz.o ]; then
  echo "DYNdgz.o       exists"
else
  ${foc} ${OPTIONS} -c                  DYNdgz.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        DYNdgz.f
  if [ -f DYNdgz.o ]; then
    touch $dateT                        DYNdgz.o
  fi
fi
fi


if [ -f DYN_NH.f ]; then
if [ -f DYN_NH.o ]; then
  echo "DYN_NH.o       exists"
else
  ${foc} ${OPTIONS} -c                  DYN_NH.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        DYN_NH.f
  if [ -f DYN_NH.o ]; then
    touch $dateT                        DYN_NH.o
  fi
fi
fi


if [ -f DYNadv.f ]; then
if [ -f DYNadv.o ]; then
  echo "DYNadv.o       exists"
else
  ${foc} ${OPTIONS} -c                  DYNadv.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        DYNadv.f
  if [ -f DYNadv.o ]; then
    touch $dateT                        DYNadv.o
  fi
fi
fi


if [ -f DYNfil.f ]; then
if [ -f DYNfil.o ]; then
  echo "DYNfil.o       exists"
else
  ${foc} ${OPTIONS} -c                  DYNfil.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        DYNfil.f
  if [ -f DYNfil.o ]; then
    touch $dateT                        DYNfil.o
  fi
fi
fi


if [ -f DYNqqm.f ]; then
if [ -f DYNqqm.o ]; then
  echo "DYNqqm.o       exists"
else
  ${foc} ${OPTIONS} -c                  DYNqqm.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        DYNqqm.f
  if [ -f DYNqqm.o ]; then
    touch $dateT                        DYNqqm.o
  fi
fi
fi


if [ -f HYDgen.f ]; then
if [ -f HYDgen.o ]; then
  echo "HYDgen.o       exists"
else
  ${foc} ${OPTIONS} -c                  HYDgen.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        HYDgen.f
  if [ -f HYDgen.o ]; then
    touch $dateT                        HYDgen.o
  fi
fi
fi


if [ -f HYDmic.f ]; then
if [ -f HYDmic.o ]; then
  echo "HYDmic.o       exists"
else
  ${foc} ${OPTIONS} -c                  HYDmic.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        HYDmic.f
  if [ -f HYDmic.o ]; then
    touch $dateT                        HYDmic.o
  fi
fi
fi


if [ -f H2O_WB.f ]; then
if [ -f H2O_WB.o ]; then
  echo "H2O_WB.o       exists"
else
  ${foc} ${OPTIONS} -c                  H2O_WB.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        H2O_WB.f
  if [ -f H2O_WB.o ]; then
    touch $dateT                        H2O_WB.o
  fi
fi
fi


if [ -f LBCnud.f ]; then
if [ -f LBCnud.o ]; then
  echo "LBCnud.o       exists"
else
  ${foc} ${OPTIONS} -c                  LBCnud.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        LBCnud.f
  if [ -f LBCnud.o ]; then
    touch $dateT                        LBCnud.o
  fi
fi
fi


if [ -f LBC000.f ]; then
if [ -f LBC000.o ]; then
  echo "LBC000.o       exists"
else
  ${foc} ${OPTIONS} -c                  LBC000.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        LBC000.f
  if [ -f LBC000.o ]; then
    touch $dateT                        LBC000.o
  fi
fi
fi


if [ -f TURtke_gen.f ]; then
if [ -f TURtke_gen.o ]; then
  echo "TURtke_gen.o   exists"
else
  ${foc} ${OPTIONS} -c                  TURtke_gen.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        TURtke_gen.f
  if [ -f TURtke_gen.o ]; then
    touch $dateT                        TURtke_gen.o
  fi
fi
fi


if [ -f TURtke_difv.f ]; then
if [ -f TURtke_difv.o ]; then
  echo "TURtke_difv.o  exists"
else
  ${foc} ${OPTIONS} -c                  TURtke_difv.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        TURtke_difv.f
  if [ -f TURtke_difv.o ]; then
    touch $dateT                        TURtke_difv.o
  fi
fi
fi


if [ -f TURabl.f ]; then
if [ -f TURabl.o ]; then
  echo "TURabl.o       exists"
else
  ${foc} ${OPTIONS} -c                  TURabl.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        TURabl.f
  if [ -f TURabl.o ]; then
    touch $dateT                        TURabl.o
  fi
fi
fi


if [ -f SSpray.f ]; then
if [ -f SSpray.o ]; then
  echo "SSpray.o       exists"
else
  ${foc} ${OPTIONS} -c                  SSpray.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        SSpray.f
  if [ -f SSpray.o ]; then
    touch $dateT                        SSpray.o
  fi
fi
fi


if [ -f qsat3D.f ]; then
if [ -f qsat3D.o ]; then
  echo "qsat3D.o       exists"
else
  ${foc} ${OPTIONS} -c                  qsat3D.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        qsat3D.f
  if [ -f qsat3D.o ]; then
    touch $dateT                        qsat3D.o
  fi
fi
fi


if [ -f SISVAT.f ]; then
if [ -f SISVAT.o ]; then
  echo "SISVAT.o       exists"
else
  ${foc} ${OPTIONS} -c                  SISVAT.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        SISVAT.f
  if [ -f SISVAT.o ]; then
    touch $dateT                        SISVAT.o
  fi
fi
fi


if [ -f OUT_nc.f ]; then
if [ -f OUT_nc.o ]; then
  echo "OUT_nc.o       exists"
else
  ${foc} ${OPTIONS} -c                  OUT_nc.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        OUT_nc.f
  if [ -f OUT_nc.o ]; then
    touch $dateT                        OUT_nc.o
  fi
fi
fi


if [ -f Debugg_MAR.f ]; then
if [ -f Debugg_MAR.o ]; then
  echo "Debugg_MAR.o   exists"
else
  ${foc} ${OPTIONS} -c                  Debugg_MAR.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        Debugg_MAR.f
  if [ -f Debugg_MAR.o ]; then
    touch $dateT                        Debugg_MAR.o
  fi
fi
fi


if [ -f DUMP.f ]; then
if [ -f DUMP.o ]; then
  echo "DUMP.o         exists"
else
  ${foc} ${OPTIONS} -c                  DUMP.f 
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        DUMP.f
  if [ -f DUMP.o ]; then
    touch $dateT                        DUMP.o
  fi
fi
fi


if [ -f SBCnew.f ]; then
if [ -f SBCnew.o ]; then
  echo "SBCnew.o       exists"
else
    touch $dateT                        SBCnew.VER
    touch $dateT                        SBCnew.dt
  ${foc} ${OPTIONS} -c                  SBCnew.f 
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        SBCnew.f 
  if [ -f SBCnew.dat ]; then
    touch $dateT                        SBCnew.dat
  fi
  if [ -f SBCnew.o ]; then
    touch $dateT                        SBCnew.o
  fi
fi
fi


if [ ${cdf} = y ]; then
if [ -f libUN.f ]; then
if [ -f libUN.o ]; then
  echo "libUN.o        exists"
else
  ${foc} ${OPTIONS} -c                  libUN.f 
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        libUN.f
  if [ -f libUN.o ]; then
    touch $dateT                        libUN.o
  fi
fi
fi
else
    rm -f                               libUN.o
fi


if [ -f zext.f ]; then
if [ -f zext.o ]; then
  echo "zext.o         exists"
else
  ${foc} ${OPTIONS} -c                  zext.f
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        zext.f
  if [ -f zext.o ]; then
    touch $dateT                        zext.o
  fi
fi
fi


if [ ${foc} != f77 ]; then
if [ -f CVAmnh.f90 ]; then
if [ -f CVAmnh.o ]; then
  echo "CVAmnh.o       exists"
else
  ${foc} ${OPTIONS} -c                  CVAmnh.f90
  dateT=`echo $datem$dated$dateH$min10$min_0$datey`
    touch $dateT                        CVAmnh.f90
  if [ -f CVAmnh.o ]; then
    touch $dateT                        CVAmnh.o
   if [ -f MODD_CST.mod ]; then
    touch $dateT                        MODD_CST.mod MODD_CONVPAR*
   fi
   if [ -f modd_cst.mod ]; then
    touch $dateT                        modd_cst.mod modd_convpar*
   fi
  fi
    rm -f                               CVAmnh.d
fi
fi
fi


    rm -f                               work.pc*


echo " "
echo " "
echo "LINK"
echo "===="
echo " "
echo " "

if [ -d radCEP.d${PX} ]; then
  ${foc} -o MAR ${OPTIONS} *.o radCEP.d${PX}/*.o ${LIBRARY}
else
  ${foc} -o MAR ${OPTIONS} *.o                   ${LIBRARY}
fi
  dateT=`echo $datem$dated$dateH$min10$min_5$datey`
  touch $dateT                          MAR_fc.sh MAR .


else

echo "OPTIONS are NOT chosen ==> NO COMPILATION"


fi
