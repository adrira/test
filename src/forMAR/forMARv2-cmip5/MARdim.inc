   
C +  ==========
C +--MARdim.inc
C +  ==========
   
      integer   mx    ,my    ,ip11  ,jp11
      parameter(mx=301,my=  1,ip11=2,jp11=1)
      integer   mz        ,mzir1     ,mzir
      parameter(mz=033,mzir1=mz+1,mzir=mz+2)
C +...mzir1 may be chosen much larger than mz,
C +   if the model vertical domain covers a small part of the air column
   
      integer   mx1     ,mx2
      parameter(mx1=mx-1,mx2=mx-2)
      integer   my1     ,my2     ,myd2
      parameter(my1=   1,my2=   1,myd2=1+my/2)
      integer   mz1     ,mzz
      parameter(mz1=mz-1,mzz=mz+1)
      integer   i_2
      parameter(i_2=mx-mx1+1)
      integer   j_2
      parameter(j_2=my-my1+1)
      integer   mzabso      ,mzhyd
      parameter(mzabso =   6,mzhyd=mzabso+1)
   
      integer   klon,        klev
      parameter(klon=1      ,klev=mz)
C +...if        klon=1      (NO vectorization)
C +   otherwise klon=mx2*my2
   
      integer   kdlon,       kflev
      parameter(kdlon=klon  ,kflev=klev)
   
      integer   n6  ,n7
      parameter(n6=6,n7=7)
C +.. n6 et n7 determine a relaxation zone towards lateral boundaries
C +   (large scale values of the variables).
C +   This zone extends over n6-1 points.
C +   Davies (1976) propose 5 points (i.e. n6=6 and n7=7)
   
      integer   mw
      parameter(mw=2)
C +..           mw is the total number of mosaics
   
