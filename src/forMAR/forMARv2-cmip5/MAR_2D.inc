
C +  ==========
C +--MAR_2D.inc
C +  ==========

      integer         indx(klon),indy(klon)

      common /hy_grd/ indx      ,indy

      real            pkta2D(klon,klev  ),tair2D(klon,klev  ),
     .                rolv2D(klon,klev  ), pst2D(klon       ),
     .                gpmi2D(klon,klev+1),gplv2D(klon,klev+1),
     .                uair2D(klon,klev  ),vair2D(klon,klev  ),
     .                  qv2D(klon,klev  ),  pk2D(klon,klev  ),
     .                pst2Dn(klon       ),wair2D(klon,klev  )

      common /hy_dyn/ pkta2D             ,tair2D             ,
     .                rolv2D             , pst2D             ,
     .                gpmi2D             ,gplv2D             ,
     .                uair2D             ,vair2D             ,
     .                  qv2D             ,  pk2D             ,
     .                pst2Dn             ,wair2D


      real              qg2D(klon,klev  ),  qw2D(klon,klev  )
     .              ,   qr2D(klon,klev  ),  qi2D(klon,klev  )
     .              ,   qs2D(klon,klev  ),cfra2D(klon,klev  )
     .              ,  dqi2D(klon,klev  ), dqw2D(klon,klev  )
     .              , qvsw2D(klon,klev+1),qvsi2D(klon,klev+1)
     .              , ccni2D(klon,klev  ),ccnw2D(klon,klev  )
     .              , rain2D(klon       ),snow2D(klon       )
     .              , crys2D(klon       ),hlat2D(klon,klev  )
c #BS.              ,   vs2D(klon,klev  )

      common /hy_hyd/   qg2D             ,  qw2D
     .              ,   qr2D             ,  qi2D
     .              ,   qs2D             ,cfra2D
     .              ,  dqi2D             , dqw2D
     .              , qvsw2D             ,qvsi2D
     .              , ccni2D             ,ccnw2D
     .              , rain2D             ,snow2D
     .              , crys2D             ,hlat2D
c #BS.              ,   vs2D


      real            ect_2D(klon,klev),TUkv2D(klon,klev),
     .                zi__2D(klon)

      common /hy_ect/ ect_2D           ,TUkv2D           ,
     .                zi__2D      


      real            prec2D(klon),snoh2D(klon),
     .                tsrf2D(klon)

      common /hy_sfl/ prec2D      ,snoh2D      ,
     .                tsrf2D      


      real            W2xyz1(klon,klev  ),W2xyz2(klon,klev  ),
     .                W2xyz3(klon,klev  ),W2xyz4(klon,klev  ),
     .                W2xyz5(klon,klev+1),W2xyz6(klon,klev+1),
     .                W2xyz7(klon,klev+1),W2xyz8(klon,klev+1),
     .                W2xyz9(klon,klev  ),W2xyz0(klon,klev  )

      common /hy_wrk/ W2xyz1             ,W2xyz2             ,
     .                W2xyz3             ,W2xyz4             ,
     .                W2xyz5             ,W2xyz6             ,
     .                W2xyz7             ,W2xyz8             ,
     .                W2xyz9             ,W2xyz0             


      real            wat01D(klon),wat11D(klon),
     .                wat21D(klon),watf1D(klon),
     .                enr01D(klon),enr11D(klon),
     .                enr21D(klon)

      common /hy_ew1/ wat01D      ,wat11D      ,
     .                wat21D      ,watf1D      ,
     .                enr01D      ,enr11D      ,
     .                enr21D


      character*20    mphy2D(klon)

      common /hy_ew2/ mphy2D


      integer         jhlr2D(klon)

      common /hy__ge/ jhlr2D


      integer         ioutIO(5)

      common /hy__io/ ioutIO
