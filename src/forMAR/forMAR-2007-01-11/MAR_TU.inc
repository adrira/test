
C +  ==========
C +--MAR_TU.inc
C +  ==========

      real             TUkvm(mx,my,mz),TUkvh(mx,my,mz),TUmin(mz)
      common /turver/  TUkvm          ,TUkvh          ,TUmin
C +...                 TUkvm : Vertical Diffusion Coefficient (Momentum)  [m2/s]
C +                    TUkvm : Vertical Diffusion Coefficient (Heat)      [m2/s]
C +                    TUmin : Vertical Diffusion Coefficient (Minimum)   [m2/s]

      real             TUkhx(mx,my,mz),TUkhff,TUkhmx
     .                ,TUkhy(mx,my,mz)
     .                ,TUspon(mzabso)
      common /turlat/  TUkhx          ,TUkhff,TUkhmx
     .                ,TUkhy
     .                ,TUspon
C +...                 TUkhx : Horizont.Diffusion Coefficient (x-direct.) [m2/s]
C +                    TUkhy : Horizont.Diffusion Coefficient (y-direct.) [m2/s]
C +                    TUspon: Horizont.Diffusion Coefficient (Top Sponge)[m2/s]
