
! +----------------------------------------------------------------------+
      subroutine StereoSouth_inverse (lon,lat,lonE,E,N)
! |  Compute Oblique Stereographic Projection from lon,lat coordinates   |
! |  Written by Cecile Agosta                                 17-05-10   |
! |  EPSG Polar Stereographic transformation Variant B                   |
! |  (http://www.epsg.org/guides/docs/G7-2.pdf)                          |
! |  Equivalent to EPSG 3031 (WGS-84 ellipsoid)                          |
! +----------------------------------------------------------------------+
! |                                                                      |
! | INPUT : lon     : Longitude (deg)                                    |
! | ^^^^^^^ lat     : Latitude  (deg)                                    |
! |         lon0    : Longitude of X axis (90 = East, clockwise)         |
! |        [lat true = 71 S]                                             |
! |                                                                      |
! | OUTPUT : E      : Stereo coordinate on the East  (X, km)             |
! | ^^^^^^^^ N      : Stereo coordinate on the North (Y, km)             |
! |                                                                      |
! +----------------------------------------------------------------------+
      implicit none

      include 'NSTdim.inc'

! +-- General Variables
! +   -----------------
      real,intent(in ) :: lon,lat,lonE
      real,intent(out) :: E,N

! +-- Local Variables
! +   ---------------
      real  costru,ddista

! +-- Constants
! +   ---------
      real  aa,ex,pi,degrad,latF,FE,FN,tF,mF,k0,t,rho,lonrad,latrad
      real  lon0,trulat
      
      aa     = 6378.1370         ! aa (km) = demi grand axe
      ex     = 0.081819190842621 ! excentricity WGS-84 : 0.081 819 190 842 622 0.081 819 190 842 621
      trulat = -71.              ! Latitude of standard parallel, 71 S for ESPG 3031
      pi     = 4. * atan(1.)
      degrad = pi / 180.

      latF   = trulat*degrad ! Latitude of standard parallel, 71 for ESPG 3031  
      lon0   = (lonE - 90.)*degrad
      lonrad = lon *degrad
      latrad = lat *degrad      
      
      FE = 0. !False Easting
      FN = 0. !False Northing

! +
! +- Polar Stereographic Projection
! +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! +
      tF  = tan (pi/4 + latF  /2) / 
     .      ( (1 + ex*sin(latF  )) / (1 - ex*sin(latF  )) )**(ex/2)
      mF  = cos(latF) / (1 - ex**2 * sin(latF)**2)**0.5
      k0  = mF*( (1+ex)**(1+ex) * (1-ex)**(1-ex) )**0.5 / (2*tF)
      
      t   = tan (pi/4 + latrad/2) / 
     .      ( (1 + ex*sin(latrad)) / (1 - ex*sin(latrad)) )**(ex/2)
      rho = 2*aa*k0*t / ( (1+ex)**(1+ex) * (1-ex)**(1-ex) )**0.5
      
      E = FE + rho*sin (lonrad - lon0)
      N = FN + rho*cos (lonrad - lon0)

      return
      end subroutine StereoSouth_inverse
C +------------------------------------------------------------------------+

! +----------------------------------------------------------------------+
      subroutine LambertAzimuthalEqualArea_inv(lon,lat,xx,yy)
! |  Compute Oblique Stereographic Projection from lon,lat coordinates   |
! |  Written by Cecile Agosta                                 17-05-10   |
! |  EPSG Polar Stereographic transformation Variant B                   |
! |  (http://www.epsg.org/guides/docs/G7-2.pdf)                          |
! |  Equivalent to EPSG 3031 (WGS-84 ellipsoid)                          |
! +----------------------------------------------------------------------+
! |                                                                      |
! | INPUT : lon     : Longitude (deg)                                    |
! | ^^^^^^^ lat     : Latitude  (deg)                                    |
! |                                                                      |
! | OUTPUT : xx     : coordinate on the East  (X, km)                    |
! | ^^^^^^^^ yy     : coordinate on the North (Y, km)                    |
! |                                                                      |
! +----------------------------------------------------------------------+
      implicit none

      include 'NSTdim.inc'

! +-- General Variables
! +   -----------------
      real,intent(in ) :: lon, lat
      real,intent(out) :: xx, yy

      real lonrad,latrad
      real kp1,kp

! +-- Constants
! +   ---------
      real pi,degrad
      real lon0,lat0

      pi     = 4. * atan(1.)
      degrad = pi / 180.

      lon0   = 0.*degrad
      lat0   = -90.*degrad

      lonrad = lon *degrad
      latrad = lat *degrad 

! +
! +- Polar Stereographic Projection
! +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! +
      kp1 = 1 + sin(lat0)*sin(latrad) + 
     .          cos(lat0)*cos(latrad)*cos(lonrad-lon0)
      kp = sqrt(2/kp1)
      
      xx = kp*cos(latrad)*sin(lonrad-lon0)
      yy = kp*(cos(lat0)*sin(latrad) - 
     .         sin(lat0)*cos(latrad)*cos(lonrad-lon0))
      
      return
      end subroutine LambertAzimuthalEqualArea_inv
C +------------------------------------------------------------------------+
      
      subroutine areaLambertAzimuthal(xl,xr,yl,yu,GEddxx,area)
          
          implicit none
          
          real,intent(in)::xl,xr,yl,yu,GEddxx
          real,intent(out)::area
          
          real lonll,latll,lonlu,latlu
          real lonrl,latrl,lonru,latru
          real xll,yll,xlu,ylu
          real xrl,yrl,xru,yru
          real l1,l2,l3
          real dp
          
          call StereoSouth(xl,yl,GEddxx,lonll,latll)
          call StereoSouth(xl,yu,GEddxx,lonlu,latlu)
          call StereoSouth(xr,yl,GEddxx,lonrl,latrl)
          call StereoSouth(xr,yu,GEddxx,lonru,latru)
          
          call LambertAzimuthalEqualArea_inv(lonll,latll,xll,yll)
          call LambertAzimuthalEqualArea_inv(lonlu,latlu,xlu,ylu)
          call LambertAzimuthalEqualArea_inv(lonrl,latrl,xrl,yrl)
          call LambertAzimuthalEqualArea_inv(lonru,latru,xru,yru)
          
          l1 = sqrt((xll-xlu)**2+(yll-ylu)**2)
          l2 = sqrt((xll-xrl)**2+(yll-yrl)**2)
          l3 = sqrt((xlu-xrl)**2+(ylu-yrl)**2)
          dp = (l1+l2+l3)/2.
          area = sqrt(dp*(dp-l1)*(dp-l2)*(dp-l3))
          l1 = sqrt((xru-xlu)**2+(yru-ylu)**2)
          l2 = sqrt((xru-xrl)**2+(yru-yrl)**2)
          dp = (l1+l2+l3)/2.
          area = area + sqrt(dp*(dp-l1)*(dp-l2)*(dp-l3))
      end subroutine areaLambertAzimuthal
