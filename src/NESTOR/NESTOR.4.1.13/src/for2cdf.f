      subroutine for2cdf(iprint,nprint,ID__nc
     .                  ,iyrrGE,mmarGE,jdarGE,jhurGE,minuGE,jsecGE
     .                  ,x_unit,y_unit,z_unit,w_unit
     .                  ,lxyz_0,lxyz_1,lxyz_2,lxyz_3,lxyz_4
     .                  ,lxyz_5,lxyz_6,lxyz_7,lxyz_8,lxyz_9
     .                  ,lxyw_0,lxyw_1,lxyw_2,lxyw_3,lxyw_4
     .                  ,lxyw_5,lxyw_6,lxyw_7,lxyw_8,lxyw_9
     .                  ,filnam,title ,fildat)

C +------------------------------------------------------------------------+
C | MAR OUTPUT                                             03-02-2005  MAR |
C |   SubRoutine for2nc is used to write x-D OUTPUTS                       |
C |                                      on  a NetCDF file                 |
C |                                                                        |
C +------------------------------------------------------------------------+
C |                                                                        |
C |   INPUT: iprint: Current time step    number                           |
C |   ^^^^^^         (starting from iprint=1, which => new file creation)  |
C |          nprint: Total  'time slices' number (max value of iprint)     |
C |          iyrrGE: Year                                                  |
C |          mmarGE: Month                                                 |
C |          jdarGE: Day                                                   |
C |          jhurGE: Hour  [UT]                                            |
C |          minuGE: Minute                                                |
C |          jsecGE: Second                                                |
C |          x_unit,y_unit,z_unit,w_unit       : x, y, z, w axes unities   |
C |          lxyz_0,lxyz_1,lxyz_2,lxyz_3,lxyz_4: variables attributes      |
C |          lxyz_5,lxyz_6,lxyz_7,lxyz_8,lxyz_9: variables attributes      |
C |          lxyw_0,lxyw_1,lxyw_2,lxyw_3,lxyw_4: variables attributes      |
C |          lxyw_5,lxyw_6,lxyw_7,lxyw_8,lxyw_9: variables attributes      |
C |          filnam: 1st Label          of the OUTPUT File Name            |
C |          title : Title              of the OUTPUT File                 |
C |          fildat: Table of Variables of the OUTPUT File                 |
C |                                                                        |
C |   INPUT(via for2nc.inc): OUTPUT dimensions                             |
C |   ^^^^^^                 OUTPUT variables                              |
C |                                                                        |
C |   OUTPUT: NetCDF File adapted to IDL Graphic Software                  |
C |   ^^^^^^                                                               |
C |                                                                        |
C |   CAUTION: 1) This Routine requires the usual NetCDF library,          |
C |   ^^^^^^^^    and the complementary access library  'libUN.a'          |
C |                                                                        |
C +------------------------------------------------------------------------+


      IMPLICIT NONE


C +--General Variables
C +  =================

      include     'for2cdf.inc'

      integer      iprint,nprint,ID__nc
      integer      iyrrGE,mmarGE,jdarGE,jhurGE,minuGE,jsecGE

      character*20 filnam
      character*90 title
      character*31 x_unit,y_unit,z_unit,w_unit
      character*10 fildat

      character*7  lxyz_0,lxyz_1,lxyz_2,lxyz_3,lxyz_4
      character*7  lxyz_5,lxyz_6,lxyz_7,lxyz_8,lxyz_9
      character*7  lxyw_0,lxyw_1,lxyw_2,lxyw_3,lxyw_4
      character*7  lxyw_5,lxyw_6,lxyw_7,lxyw_8,lxyw_9


C +--Local   Variables
C +  =================

      integer    nzz,i,j,k
      PARAMETER (nzz  = nz+1)

      integer    njmoGE(0:12),njmbGE(0:12)
      integer    njyrGE(0:12),njybGE(0:12)

      integer    Lfnam,     Ltit,     Luni,     Lnam,     Llnam
      PARAMETER (Lfnam= 40, Ltit= 90, Luni= 31, Lnam= 13, Llnam=50)
C +...Length of char strings 

      CHARACTER*(Lfnam)  fnamNC
      common/for2nc_loc/ fnamNC
C +...                   fnamNC: To retain file name.

      integer    NdimNC
      PARAMETER (NdimNC = 5)
C +...Number of defined spatial dimensions (exact)

      integer    MXdim
      PARAMETER (MXdim = 6000)
C +...Maximum Number of all dims: recorded Time Steps
C +   and also maximum of spatial grid points for each direction. 

      integer    MX_var
      PARAMETER (MX_var = 80)
C +...Maximum Number of Variables 

      integer    NattNC
      PARAMETER (NattNC = 2)
C +...Number of REAL attributes given to all variables

      INTEGER           RCODE

      integer           jourNC(MXdim)
      integer           moisNC(MXdim)
      real              yearNC(MXdim)
      real              dateNC(MXdim)
      real              timeNC(MXdim)
      common/OUT2nc_r/  yearNC,dateNC
      real              VALdim(MXdim,0:NdimNC)
      integer           nDFdim(      0:NdimNC)
      common/OUT2nc_d/  nDFdim
      integer           NvatNC(NattNC)
      CHARACTER*(Lnam)  NAMdim(      0:NdimNC)
      CHARACTER*(Luni)  UNIdim(      0:NdimNC)
      CHARACTER*(Lnam)  SdimNC(4,MX_var)       
      CHARACTER*(Luni)  unitNC(MX_var)
      CHARACTER*(Lnam)  nameNC(MX_var)
      CHARACTER*(Llnam) lnamNC(MX_var)
      CHARACTER*(Ltit ) tit_NC
      CHARACTER*(Lnam)  NAMrat(NattNC)
c #TC CHARACTER*9   labelc
      CHARACTER*120 tmpINP

      integer   n1000 ,n100a ,n100  ,n10_a ,n10   ,n1    
      integer   m10   ,       jd10  ,jd1
      integer   MMXstp,it    ,mois  ,mill  ,iu
      integer   itotNC,NtotNC
      real      starta(1)

      data  njmoGE                                          ! Nb of Days
     .     /0,31,28,31,30, 31, 30, 31, 31, 30, 31, 30, 31/  ! in each Month
      data  njmbGE                                          ! Leap Year
     .     /0, 0, 1, 0, 0,  0,  0,  0,  0,  0, 0,   0,  0/  ! Correction
      data  njyrGE                                          ! Nb of Days
     .     /0, 0,31,59,90,120,151,181,212,243,273,304,334/  ! since 1st Jan
      data  njybGE                                          ! Leap Year
     .     /0, 0, 0, 1, 1,  1,  1,  1,  1,  1,  1,  1,  1/  ! Correction


C +--NetCDF File Initialization
C +  ==========================


      IF (iprint.eq.1) THEN


C +--Output File Label
C +  -----------------

        fnamNC = filnam      


C +--Output Title
C +  ------------

        tit_NC = title


C +--Create File / Write Constants
C +  -----------------------------
        MMXstp = MXdim
C +...  To check array bounds... silently

C +--Time Variable (hour)
C +  ~~~~~~~~~~~~~~~~~~~~

C +...  To define a NetCDF dimension (size, name, unit):
c _UL   nDFdim(0)= nprint
        nDFdim(0)= 0
        NAMdim(0)= 'time'
        UNIdim(0)= 'HOURS since 1901-01-15 00:00:00'

C +...  Check temporary arrays: large enough ?
        IF (nprint.gt.MMXstp)
     &  STOP '*** for_2D - ERROR : MXdim to low ***'


C +--Define horizontal spatial dimensions :    
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

C +...  Check temporary arrays: large enough ?
        IF (    nx .gt.MMXstp.or.ny.gt.MMXstp
     &      .or.nzz.gt.MMXstp.or.nw.gt.MMXstp)
     &    STOP '*** for_2D - ERROR : MXdim to low ***'

C +...To define NetCDF dimensions (size, name, unit):

        DO i = 1, nx
          VALdim(i,1) =  x_axis(i)
        END DO
          nDFdim(1)   =  nx
          NAMdim(1)   = 'x'
          UNIdim(1)   =  x_unit

        DO j = 1, ny
          VALdim(j,2) =  y_axis(j)
        END DO
          nDFdim(2)   =  ny
          NAMdim(2)   = 'y'
          UNIdim(2)   =  y_unit

        do k = 1, nz
          VALdim(1,3) =  z_axis(k)
        enddo
          nDFdim(3)   =  nz
          NAMdim(3)   = 'level'
          UNIdim(3)   =  z_unit
C +...    For levels k

        do k = 1, nz
          VALdim(k,4) =  z_axis(k)
        enddo
          nDFdim(4)   =  nz
          NAMdim(4)   = 'level2'
          UNIdim(4)   =  z_unit  
C +...    For levels k+1/2

        do k = 1, nw
          VALdim(k,5) =  w_axis(k)
        enddo
          nDFdim(5)   =  nw
          NAMdim(5)   = 'sector'
          UNIdim(5)   =  w_unit
C +...    For Surface Sectors 

C +--Variable's Choice (Table xxxxxx.dat)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        OPEN(unit=10,status='unknown',file=fildat)

        itotNC = 0
 980    CONTINUE
          READ (10,'(A120)',end=990) tmpINP
          IF (tmpINP(1:4).eq.'    ')                                THEN
            itotNC = itotNC + 1
            READ (tmpINP,'(4x,5A9,A12,A50)')
     &          nameNC(itotNC)  ,SdimNC(1,itotNC),SdimNC(2,itotNC),
     &          SdimNC(3,itotNC),SdimNC(4,itotNC),
     &          unitNC(itotNC)  ,lnamNC(itotNC)
C +...          nameNC: Name
C +             SdimNC: Names of Selected Dimensions (max.4/variable) 
C +             unitNC: Units
C +             lnamNC: Long_name, a description of the variable

          ENDIF
        GOTO 980
 990    CONTINUE

        CLOSE(unit=10)

        NtotNC = itotNC 
C +...  NtotNC : Total number of variables writen in NetCDF file.

C +--List of NetCDF attributes given to all variables:
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
C +...  The "actual_range" is the (min,max)
C +     of all data for each variable:
        NAMrat(1) = 'actual_range'
        NvatNC(1) = 2

C +...  The "[var]_range" is NOT of attribute type,
C +     it is a true variable containing the (min,max) for
C +     each level, for 4D (space+time) variables only
C +     (automatic handling by UN library;
C +      must be the LAST attribute)
        NAMrat(NattNC) = '[var]_range'
        NvatNC(NattNC) = 2

C +--Automatic Generation of the NetCDF File Structure
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

C +     **************
        CALL UNscreate (fnamNC, tit_NC,
     &                  NdimNC, nDFdim, MXdim , NAMdim, UNIdim, VALdim,
     &                  MX_var, NtotNC, nameNC, SdimNC, unitNC, lnamNC,
     &                  NattNC, NAMrat, NvatNC,
     &                  ID__nc) 
C +     **************


C +--Write Time - Constants
C +  ~~~~~~~~~~~~~~~~~~~~~~

C +       ************
          CALL UNwrite (ID__nc, 'LON   ', 1  , nx    , ny, 1 , Longit)
          CALL UNwrite (ID__nc, 'LAT   ', 1  , nx    , ny, 1 , Latitu)
          CALL UNwrite (ID__nc, 'ANT1km', 1  , nx    , ny, 1 , OroOBS)
c #2D     CALL UNwrite (ID__nc, 'sh_MAR', 1  , nx    , ny, 1 , OroSIM)
          CALL UNwrite (ID__nc, 'SOL   ', 1  , nx    , ny, 1 , SolTyp)
C +       ************


C +--Re-Open file if already created.
C +  ================================


      ELSE

C +     ************
        CALL UNwopen (fnamNC,ID__nc)
C +     ************

      END IF


C +--Write Time-dependent variables:
C +  ===============================


C +--UNLIMITED Time Dimension
C +  ------------------------

      IF (nDFdim(0).eq.0)                         THEN !
              starta = (351+(iyrrGE  -1902) *365       ! Nb Days before iyrrGE
     .                     +(iyrrGE  -1901) /  4       ! Nb Leap Years
     .                     + njyrGE(mmarGE)            ! Nb Days before mmarGE
     .                     + njybGE(mmarGE)            ! (including Leap Day)
     .                 *max(0,1-mod(iyrrGE,4))         !
     .                     + jdarGE     -1      )*  24 !
     .                 +jhurGE                         !
     .               + (minuGE *60 +jsecGE      )/3600.!

C +     ************
        CALL UNwrite (ID__nc, 'time   ',iprint, 1, 1, 1, starta)
C +     ************

      END IF


C +     ************
c #3D   CALL UNwrite (ID__nc,lxyz_0,iprint,nx,ny,nz , fxyz_0)
c #3D   CALL UNwrite (ID__nc,lxyz_1,iprint,nx,ny,nz , fxyz_1)
c #3D   CALL UNwrite (ID__nc,lxyz_2,iprint,nx,ny,nz , fxyz_2)
c #3D   CALL UNwrite (ID__nc,lxyz_3,iprint,nx,ny,nz , fxyz_3)
c #3D   CALL UNwrite (ID__nc,lxyz_4,iprint,nx,ny,nz , fxyz_4)
c #3D   CALL UNwrite (ID__nc,lxyz_5,iprint,nx,ny,nz , fxyz_5)
c #3D   CALL UNwrite (ID__nc,lxyz_6,iprint,nx,ny,nz , fxyz_6)
c #3D   CALL UNwrite (ID__nc,lxyz_7,iprint,nx,ny,nz , fxyz_7)
c #3D   CALL UNwrite (ID__nc,lxyz_8,iprint,nx,ny,nz , fxyz_8)
c #3D   CALL UNwrite (ID__nc,lxyz_9,iprint,nx,ny,nz , fxyz_9)
c #3D   CALL UNwrite (ID__nc,lxyw_0,iprint,nx,ny,nw , fxyw_0)
c #3D   CALL UNwrite (ID__nc,lxyw_1,iprint,nx,ny,nw , fxyw_1)
c #3D   CALL UNwrite (ID__nc,lxyw_2,iprint,nx,ny,nw , fxyw_2)
c #3D   CALL UNwrite (ID__nc,lxyw_3,iprint,nx,ny,nw , fxyw_3)
c #3D   CALL UNwrite (ID__nc,lxyw_4,iprint,nx,ny,nw , fxyw_4)
c #3D   CALL UNwrite (ID__nc,lxyw_5,iprint,nx,ny,nw , fxyw_5)
c #3D   CALL UNwrite (ID__nc,lxyw_6,iprint,nx,ny,nw , fxyw_6)
c #3D   CALL UNwrite (ID__nc,lxyw_7,iprint,nx,ny,nw , fxyw_7)
c #3D   CALL UNwrite (ID__nc,lxyw_8,iprint,nx,ny,nw , fxyw_8)
c #3D   CALL UNwrite (ID__nc,lxyw_9,iprint,nx,ny,nw , fxyw_9)
C +     ************


C +--That 's all, folks: NetCDF File Closure
C +  =======================================

C +   ***********
      CALL NCCLOS (ID__nc,RCODE)
C +   ***********


      return
      end
