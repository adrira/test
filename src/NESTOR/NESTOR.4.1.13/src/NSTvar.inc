
!-Nested model
! ------------

      CHARACTER*3 NSTmod

! ....NSTmod : acronym of the nested model


!-Horizontal and vertical grid
! ----------------------------

      REAL NST__x(mx,my),NST__y(mx,my),                                 &
     &     NSTgdx(mx)   ,NSTgdy(my)   ,NSTgdz(mz)      ,                &
     &     NST_dx,NSTrcl     

      LOGICAL NSTinc(mx,my,nbdom)

! ....NST__x : X-coordinates (longitude)
! ....NST__y : Y-coordinates (latitude)
! ....NSTgdx : simple X-grid (specific to each model)
! ....NSTgdy : simple Y-grid (specific to each model)
! ....NSTgdz : simple Z-grid (specific to each model)
! ....NST_dx : horizontal resolution for regular grid
! ....NSTinc : Check the location (continent) of a grid point


!-2-D surface variables
! ---------------------

      REAL NST_st(mx,my),NSTdst(mx,my),NST_sp(mx,my),                   &
     &     NST_sh(mx,my),NST_pr(mx,my),NST_sn(mx,my),                   &
     &     NST_z0(mx,my),NST_r0(mx,my),NST_d1(mx,my),                   &
     &     NSTalb(mx,my),NSTeps(mx,my),NSTres(mx,my),                   &
     &     NSTch0(mx,my),NSTIpr(mx,my),NSTewc(mx,my),                   &
     &     NSTsst(mx,my),NSTuts(mx,my),NSTpr1(mx,my),                   &
     &     NSTpr2(mx,my),NSTpr3(mx,my),NSTsic(mx,my),                   &
     &     NSTzor(mx,my)

      INTEGER NSTveg(mx,my,nvx),NSTsvt(mx,my,nvx),                      &
     &        NSTiwf(mx,my    ),NSTsol(mx,my    ),                      &
     &        NSTtex(mx,my    )

      REAL    NSTlai(mx,my,nvx),NSTglf(mx,my,nvx),                      &
     &        NSTvfr(mx,my,nvx),NSTsfr(mx,my,nvx),                      &
     &        NSTdsa(mx,my    ),NSTlmx(mx,my,nvx),                      &
     &        NSTfrc(mx,my    ),NSTdv1(mx,my    ),                      &
     &        NSTdv2(mx,my    ),NSTndv(mx,my    ),                      &
     &        NSTice(mx,my    ),NSTgrd(mx,my    ),                      &
     &        NSTrck(mx,my    ),NSTarea(mx,my)

! ....NST_st : soil or sea surface temperature
! ....NSTsst : sea surface temperature (Reynolds)
! ....NSTdst : deep soil temperature
! ....NST_sp : surface pressure
! ....NST_sh : surface elevation
! ....NSTpr1 : desaggregated precipitation (without conservation)
! ....NSTpr2 : desaggregated precipitation (with global conservation)
! ....NSTpr3 : desaggregated precipitation (with local and global conservation)
! ....NSTsic : Sea-Ice   Fraction
! ....NSTIpr : rain precipitation (non desaggregated)
! ....NSTewc : equivalent water content (water vapor)
! ....NST_sn : snow precipitation
! ....NSTsol : soil types (water,ice,snow,land,...)
! ....NST_z0 : roughness length for momentum
! ....NST_r0 : roughness length for heat
! ....NSTtex : soil texture
! ....NST_d1 : surface heat capacity (Deardorff, 1978)
! ....NSTalb : surface albedo
! ....NSTeps : surface IR emissivity
! ....NSTres : aerodynamic resistance
! ....NSTch0 : bulk aerodynamic coefficient air/surface
! ....         humidity flux
! ....NSTveg : vegetation type (IGBP classification)
! ....NSTvfr : fraction of vegetation grid cells (IGBP)
! ....NSTsvt : vegetation type (SVAT classification)
! ....NSTsfr : fraction of vegetation grid cells (SVAT)
! ....NSTfrc : fraction of vegetation cover (from NDVI)
! ....NSTndv : NDVI index
! ....NSTdv1 : minimum NDVI index
! ....NSTdv2 : maximum NDVI index
! ....NSTlai : leaf area index
! ....NSTglf : green leaf fraction
! ....NSTdsa : dry soil albedo
! ....NSTiwf : 0=no water flux, 1=free drainage
! ....NSTuts : surface heat flux
! ....NSTzor : roughness length for momentum / orography contribution
! ....NSTice : ice(land-ice+ice-sheves)/sea(ocean+sea-ice) fraction
! ....NSTgrd : fraction of land ice that is grounded
! ....NSTrck : fraction of cell containing rock
! ....NSTarea: cell area


!-2.5-D surface variables
! -----------------------

      REAL NST_ts(mx,my,nvx,nsl),NST_sw(mx,my,nvx,nsl)

! ....NST_ts : soil temperature
! ....NST_sw : soil moisture content


!-3-D atmospheric variables
! -------------------------
      REAL NST_hp(mx,my,mz)
! ....NST_hp : Local vertic coordinate for interpolation

      REAL NST__u(mx,my,mz),NST__v(mx,my,mz),NST__w(mx,my,mz),          &
     &     NST_pt(mx,my,mz),NST__t(mx,my,mz),NST_qv(mx,my,mz),          &
     &     NST_zz(mx,my,mz),NST__p(mx,my,mz),NSTtke(mx,my,mz),          &
     &     NST_qt(mx,my,mz),NSTtmp(mx,my,mz+1),NST_rh(mx,my,mz)

! ....NST__u : U-wind
! ....NST__v : V-wind
! ....NST__w : W-wind
! ....NST_pt : potential temperature
! ....NST__t : real temperature
! ....NST_qv : specific humidity
! ....NST_rh : relative humidity
! ....NST_zz : geopotential height
! ....NST__p : pressure at each level
! ....NSTtke : turbulent kinetic energy
! ....NST_qt : total cloud water content
! ....NSTtmp : temporary array


      COMMON/NSTvar_c/NSTmod
      
      COMMON/NSTvar_i/NSTveg,NSTsvt,NSTiwf,NSTsol,                      &
     &                NSTtex
      
      COMMON/NSTvar_l/NSTinc
      
      COMMON/NSTvar_r/NST__x,NST__y,NSTgdx,NSTgdy,NSTgdz,NST_dx,        &
     &                NST_st,NSTdst,NST_sp,NST_sh,NST_pr,NST_sn,        &
     &                NST_z0,NST_r0,NST_d1,NSTalb,NSTeps,NSTres,        &
     &                NSTch0,NSTIpr,NSTewc,NSTsst,NSTuts,NSTpr1,        &
     &                NSTpr2,NSTpr3,NSTlai,NSTglf,NSTdsa,NSTlmx,        &
     &                NSTfrc,NSTdv1,NSTdv2,NSTndv,NST_ts,NST_sw,        &
     &                NST_hp,NST__u,NST__v,NST__w,NST_pt,NST__t,        &
     &                NST_qv,NST_zz,NST__p,NSTtke,NST_qt,NSTtmp,        &
     &                NSTsic,NST_rh,NSTvfr,NSTsfr,NSTice,NSTzor,        &
     &                NSTrcl, NSTgrd,NSTrck,NSTarea
 
