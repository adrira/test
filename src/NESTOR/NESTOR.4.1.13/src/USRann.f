C   +-------------------------------------------------------------------+
C   |  Subroutine USRann                               June 03  NESTING |
C   +-------------------------------------------------------------------+
C   | USRant adapt NESTOR to Antarctic region                           |
C   |                                                                   |
C   | Input : - subnam : Name of the subroutine                         |
C   | ^^^^^^^            where USRann is called                         |
C   |                                                                   |
C   | Maintainer : Hubert Gallee                                        |
C   | ^^^^^^^^^^^^                                                      |
C   |                                                                   |
C   +-------------------------------------------------------------------+

      SUBROUTINE USRann (subnam)


      IMPLICIT NONE

C +---General variables
C +   -----------------

      INCLUDE 'NSTdim.inc'
      INCLUDE 'NSTvar.inc'
      INCLUDE 'NESTOR.inc'
      INCLUDE 'LSCvar.inc'

C +---local variables
C +   ---------------

      CHARACTER*6 subnam
  
C +   +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

C +---Topography for Antarctic    
C +   ========================

      IF (subnam.eq.'NSIDC ')                                       THEN

C +           ******
!        CALL GTOP30
C +           ******

C +           ******
         CALL A_TOPO 
C +           ******

      END IF

          
      END SUBROUTINE

C +   +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
      
C   +-------------------------------------------------------------------+
C   |  Subroutine A_TOPO                          19 June 2009  NESTING |
C   |             ANTARCTIC TOPOGRAPHY specific Assimilation            |
C   +-------------------------------------------------------------------+
C   |                                                                   |
C   | Input : NST__x : longitude (degree) of the NST grid               |
C   | ^^^^^^^ NST__y : latitude  (degree) of the NST grid               |
C   |                                                                   |
C   | Output: NST_sh: surface elevation                                 |
C   | ^^^^^^^ NSTsol: land (4) / sea (1) mask                           |
C   |                                                                   |
C   | Method: Divide each nested Grid Cell in elementary Meshes         |
C   | ^^^^^^^ much small than either the nested or the DTM Mesh         |
C   |         Compute geographic Coordinates of each elementary Mesh    |
C   |         Compute Distance of this Mesh to 4 closest DTM Meshes     |
C   |         Compute Height of this Mesh by kriging                    |
C   |         Nested Grid Cell Height is the Average                    |
C   |                                 of the elementary Meshes Heights  |
C   |                                                                   |
C   | DATA Source: Radarsat Antarctic Mapping Project Digital           |
C   | ^^^^^^^^^^^^ Digital  Elevation Model   Version 2                 |
C   |              ftp site : sidads.colorado.edu                       |
C   |              directory: /pub/DATASETS/RAMP/DEM_V2/1KM/ASCII       |
C   |              file     : ramp1kmdem_wgsosu_v2.txt.gz               |
C   |                                                                   |
C   | Reference:   Liu, H., K. Jezek, B. Li, and Z. Zhao. 2001.         |
C   | ^^^^^^^^^^   Radarsat Antarctic Mapping Project                   |
C   |              Digital Elevation Model Version 2.                   |
C   |              Boulder, CO: National Snow and Ice Data Center.      |
C   |              Digital media.                                       |
C   |                                                                   |
C   | Modifications:                                                    |
C   | ^^^^^^^^^^^^^^                                                    |
C   |         nico: 19/07/2005                                          |
C   |                little change in the mask calculation (NSTsol) to  |
C   |                allow the coupling with the ocean model OPA.       |
C   |         hub:  19/06/2009                                          |
C   |                interpolation made on the stereographic plane      |
C   |                               not on the sphere                   |
C   |                                                                   |
C   +-------------------------------------------------------------------+


      SUBROUTINE A_TOPO


      IMPLICIT NONE


C +   General and local variables
C +   ---------------------------

      INCLUDE 'NSTdim.inc'
      INCLUDE 'NSTvar.inc'
      INCLUDE 'NESTOR.inc'
      INCLUDE 'MARvar.inc'
      INCLUDE 'LOCfil.inc'
      INCLUDE 'NetCDF.inc'
      INCLUDE 'for2nc.inc'
      INCLUDE 'for2cdf.inc'

      LOGICAL   OUTrmp,ROTdom

      character*20 OROcdf
      character*31 x0unit,y0unit,z0unit,w0unit
      character*90 Title0

      integer      ID_cdf,ID__nc,idx

      REAL      aux_z0(mx,my)

      INTEGER   nxdata,nydata ,nwdata,nndata
      PARAMETER(nxdata=13670204) 
      INTEGER   ii           ,jj           ,i,j,nn
      REAL      x_RAMP(mx,my),y_RAMP(mx,my)

      REAL*4    ANT_sh
      REAL*4    ANTlat
      REAL*4    ANTlon
      REAL      xdista       ,ydista       ,ddista
      REAL      xdisto       ,ydisto       ,TruRCL
      REAL      xx_min       ,yy_min
      REAL      xx_max       ,yy_max

      REAL      pi           ,degrad       ,t_grad       ,t__rad
      REAL      pidemi       ,earthr
      INTEGER   inx          ,jny
      INTEGER   in           ,jn
      INTEGER   ir           ,jr
      INTEGER   i__rot       ,j__rot

      INTEGER   imxx    ,jmyy
      PARAMETER(imxx=100,jmyy=100)
      REAL      ddxxi        ,ddxxj
      REAL      xx(      imxx,      jmyy),yy(      imxx,      jmyy)
      REAL      xxii                     ,yyii
      REAL      x0(-imxx:imxx,-jmyy:jmyy),y0(-imxx:imxx,-jmyy:jmyy)
      REAL      x1(-imxx:imxx,-jmyy:jmyy),y1(-imxx:imxx,-jmyy:jmyy)
      REAL      hh(imxx,jmyy)
      REAL      x__A         ,y__A
      REAL      sinA         ,cosA         ,hypA


      DATA      OUTrmp/.true.    /        ! RAMP OUTPUT switch
      DATA      ROTdom/.false.   /        ! Rotation    switch
      DATA      earthr/6396.990e3/        ! Earth Radius
      DATA      t_grad/ 360.     /        ! 
c #DDUDATA      i__rot/ 210      /        ! DDU closest MAR grid pt
c #DDUDATA      j__rot/  40      /        ! DDU closest MAR grid pt
      DATA      i__rot/ 217      /        ! PrE closest MAR grid pt
      DATA      j__rot/ 233      /        ! PrE closest MAR grid pt


C +   INITIALIZATION
C +   ==============

      pi     =  acos( -1.0d0)
      pidemi =  pi *   0.5d0
      degrad =  pi / 180.0d0
      t__rad =  pi *   2.0d0
      nndata =         0

      DO j=1,ny
      DO i=1,nx
        SolTyp(i,j) = 1.
      ENDDO
      ENDDO


C +   TOPOGRAPHY INPUT (already interpolated)
C +   ================

      IF (TOP30.eq.'nsid')                                          THEN
        write(6,*) 'A_TOPO: INPUT File rampxkmdem_wgsosu_v2.dat OPENING'
        open(unit=1,status='unknown',file='rampxkmdem_wgsosu_v2.dat')
        rewind    1
        DO  j=1,my
        DO  i=1,mx
          read(1,100)   NST_sh(i,j),NSTsol(i,j),NSTzor(i,j)
 100      format(e15.6,i15,e15.6)
        END DO
        END DO
          read(1,101)   TruRCL
 101      format(e15.6)
        IF (TruRCL.NE.NSTrcl)                                       THEN
          write(6,102)TruRCL,NSTrcl
 102      format(' The relative colatitude',f6.1,
     .        ' of this TOPO is NOT that (',f6.1,') what is requested')
          STOP '#�@#&!! Relative Colatitude is inconsistent'
        END IF
        write(6,*)  'A_TOPO: INPUT File rampxkmdem_wgsosu_v2.dat IN'
        close(unit=1)
      ELSE


C +   TOPOGRAPHY INPUT (to be   interpolated, )
C +   ================

        write(6,*) 'A_TOPO: INPUT File ramp1kmdem_wgsosu_v2.bin OPENING'
        write(6,*) '                 '
        write(6,*) '        NST_dx = ',NST_dx
        write(6,*) '                 '
        open(unit=1,status='old',file='ramp1kmdem_wgsosu_v2.bin',
     .       form='unformatted')
        rewind     1

           ii= 0
           xx_min = 1.e9
           xx_max =-1.e9
           yy_min = 1.e9
           yy_max =-1.e9
 1001   CONTINUE

c #WR   IF (mod(ii,10).EQ.0) write(6,6000)
 6000   format(12x,8x,'Lat',12x,'Lon',14x,'z',
     .         14x,'d',14x,'x',13x,'dx',14x,'y',13x,'dy')

           ii=1+ii
c #DO   DO ii=1,nxdata
        read(1,end=1000) ANTlat,ANTlon,ANT_sh


C +   Cartesian coordinates of the INPUT TOPOGRAPHY (polar stereographic
C +   ---------------------------------------------  reference plane: 71�S)

        ddista = earthr *(sin( 71.d0              *degrad) + 1.d0)
     .                  * tan((45.d0+ANTlat*0.5d0)*degrad)
        xdista = ddista * cos((90.d0-ANTlon)      *degrad)
        ydista = ddista * sin((90.d0-ANTlon)      *degrad)

c #WR   IF (ii.GT.1)
c #WR.  write(6,6001) ii,ANTlat,ANTlon,ANT_sh,1.e-3*ddista
c #WR.                  ,1.e-3*xdista,1.e-3*(xdista-xdisto)
c #WR.                  ,1.e-3*ydista,1.e-3*(ydista-ydisto)
 6001   format(' TEST:',i6,3f15.9,5f15.3)

c #WR   xdisto = xdista
c #WR   ydisto = ydista

c #WR   IF (ii.EQ.50) STOP "Arr�t TEST"

        xx_min = min(xdista,xx_min)
        yy_min = min(ydista,yy_min)
        xx_max = max(xdista,xx_max)
        yy_max = max(ydista,yy_max)


C +   Index     Coordinates
C +   ---------------------

        i          = xdista *1.e-3 + 2750
        j          = ydista *1.e-3 + 2250
        Latitu(i,j)= ANTlat
        Longit(i,j)= ANTlon
        OroOBS(i,j)= ANT_sh
        SolTyp(i,j)= 3.


        GO TO 1001
 1000   CONTINUE

c #DO   END DO
        nwdata = ii
        close(unit=1)


C +   OUTPUT
C +   ------

        IF (OUTrmp)                                                 THEN
          x0unit        = 'km'
          y0unit        = 'km'
          z0unit        = 'm'
          w0unit        = '-'
          OROcdf        = 'RAntMP_1_km_DEM.cdf'
          Title0( 1:40) = 'Radarsat Antarctic Mapping Project 1 km '
          Title0(41:80) = 'DEM, version 2, NSIDC, Liu et al., 2001 '
C +                        1234567890123456789012345678901234567890

          DO i=1,nx
            x_axis(i) = i - 2750.
          ENDDO

          DO j=1,ny
            y_axis(j) = j - 2250.
          ENDDO

C +            *******
          call for2cdf(1,1,ID_cdf,2001,12,31,0,0,0
     .                , x0unit  , y0unit  , z0unit  , w0unit
     .                ,'       ','       ','       ','       ','       '
     .                ,'       ','       ','       ','       ','       '
     .                ,'       ','       ','       ','       ','       '
     .                ,'       ','       ','       ','       ','       '
     .                , OROcdf  ,Title0,'ANToro.dat')
C +            *******

        END IF

        write(6,6011) xx_min*1.e-3
 6011   format(     ' xx_min = ',f15.3)
        write(6,6012) yy_min*1.e-3
 6012   format(     ' yy_min = ',f15.3)
        write(6,6021) xx_max*1.e-3
 6021   format(     ' xx_max = ',f15.3)
        write(6,6022) yy_max*1.e-3
 6022   format(     ' yy_max = ',f15.3)
        write(6,*)  'A_TOPO: INPUT File ramp1kmdem_wgsosu_v2.bin IN'
        write(6,*)  '        Nb DATA: ',nwdata
        write(6,*)  '                 '


C +   Antarctic Topography in MAR
C +   ===========================

        idx   = NST_dx*0.5e-3           ! 1/2 maille, passage m --> km 
        inx   =    idx*2+1
        jny   =    idx*2+1
        ddxxi =        1.d0
        ddxxj =        1.d0

C +     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        DO j=1,my                             ! Loop on NST grid points
        DO i=1,mx

C +     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


C +   Cartesian coordinates of MAR/ANTAR TOPOGRAPHY (polar stereographic
C +   ---------------------------------------------  reference plane: 71�S)

        IF (NST__y(i,j).GE.0.)                                      THEN
          write(6,6003) NST__x(i,j),NST__y(i,j)
 6003     format(/,' *** LON, LAT =',2f12.4,' ***',/,1x)
          STOP 'Latitude: empty data'
        END IF

        ddista = earthr *(sin( 71.d0                   *degrad) + 1.d0)
     .                  * tan((45.d0+NST__y(i,j)*0.5d0)*degrad)
        xdista = ddista * cos((90.d0-NST__x(i,j))      *degrad)
        ydista = ddista * sin((90.d0-NST__x(i,j))      *degrad)

        x_RAMP(i,j)= xdista
        y_RAMP(i,j)= ydista

C +     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        ENDDO                                 ! Loop on NST grid points
        ENDDO

        DO j=1,my                             ! Loop on NST grid points
        DO i=1,mx

C +     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        IF (NST__y(i,j).LT.-60.00)       THEN ! South of Drake Passage


C +   Interpolation/Average
C +   ---------------------

            ii     = x_RAMP(i,j) *1.e-3 + 2750
            jj     = y_RAMP(i,j) *1.e-3 + 2250

            NST_sh(i,j) = 0.
            nydata      = 0

C +   Rotation
C +   ~~~~~~~~
        IF   (ROTdom)                                               THEN
          IF (i.LT.mx)                                              THEN
            y__A =  y_RAMP(i+1,j)-y_RAMP(i  ,j)
            x__A =  x_RAMP(i+1,j)-x_RAMP(i  ,j)
          ELSE
            y__A =  y_RAMP(i  ,j)-y_RAMP(i-1,j)
            x__A =  x_RAMP(i  ,j)-x_RAMP(i-1,j)
          END IF

            hypA =  sqrt(x__A*x__A + y__A*y__A)
            cosA =       x__A/hypA
            sinA =       y__A/hypA

          DO jr=-idx,idx
          DO ir=-idx,idx
            x0(ir,jr) = ir
            y0(ir,jr) = jr
          ENDDO
          ENDDO

          DO jr=-idx,idx
          DO ir=-idx,idx
            x1(ir,jr) = cosA * x0(ir,jr) - sinA * y0(ir,jr)
            y1(ir,jr) = sinA * x0(ir,jr) + cosA * y0(ir,jr)
          ENDDO
          ENDDO

C +   OUTPUT for VERIFICATION
C +   ~~~~~~~~~~~~~~~~~~~~~~~
          IF (i.EQ.i__rot .AND. j.EQ.j__rot)                        THEN
            OROcdf        = 'MARdom_1_km_VER.JNL'
            write(OROcdf(7:9),'(i3)') idx *2
            IF   (OROcdf(7:7).EQ.' ') OROcdf(7:7) = '_'
            IF   (OROcdf(8:8).EQ.' ') OROcdf(8:8) = '_'

            open(unit=2,status='unknown',file=OROcdf)
            rewind    2
          END IF

          DO jr=-idx,idx
          DO ir=-idx,idx
            in                          = x1(ir,jr) + ii
            jn                          = y1(ir,jr) + jj
            xx(ir   +idx+1,jr   +idx+1) = in
            yy(ir   +idx+1,jr   +idx+1) = jn
          IF  (in.GT.0.AND.in.LE.nx .AND. jn.GT.0.AND.jn.LE.ny)     THEN
            hh(ir   +idx+1,jr   +idx+1) = OroOBS(in,jn)
          ELSE
            hh(ir   +idx+1,jr   +idx+1) = 0.
          END IF

            NST_sh(i,j) = NST_sh(i,j)   + hh(ir   +idx+1,jr   +idx+1)
            nydata      = nydata        + 1

C +   OUTPUT for VERIFICATION
C +   ~~~~~~~~~~~~~~~~~~~~~~~
          IF (i.EQ.i__rot .AND. j.EQ.j__rot)                        THEN
            write(2,21) x_RAMP(i,j)*1.e-3+x1(ir,jr)
     .                 ,y_RAMP(i,j)*1.e-3+y1(ir,jr)
 21         format('LABEL ',2(f8.0,','),' 0,0,.08 @P5+')
          END IF
          ENDDO
          ENDDO
          IF (i.EQ.i__rot .AND. j.EQ.j__rot)                        THEN
            write(2,22) x_RAMP(i  ,j  ) *1.e-3,y_RAMP(i  ,j  ) *1.e-3
     .                 ,x_RAMP(i-1,j  ) *1.e-3,y_RAMP(i-1,j  ) *1.e-3
     .                 ,x_RAMP(i  ,j-1) *1.e-3,y_RAMP(i  ,j-1) *1.e-3
     .                 ,x_RAMP(i-1,j-1) *1.e-3,y_RAMP(i-1,j-1) *1.e-3
 22         format('LABEL ',2(f8.0,','),' 0,0,.15 @P7x'
     .        ,  /,'LABEL ',2(f8.0,','),' 0,0,.12 @P7x'
     .        ,2(/,'LABEL ',2(f8.0,','),' 0,0,.10 @P7x'))

            close(unit=2)
          END IF

C +   No Rotation
C +   ~~~~~~~~~~~
        ELSE
          DO jn=jj-idx,jj+idx
          DO in=ii-idx,ii+idx
              xx(in-ii+idx+1,jn-jj+idx+1) = in
              yy(in-ii+idx+1,jn-jj+idx+1) = jn
            IF  (in.GT.0.AND.in.LE.nx .AND. jn.GT.0.AND.jn.LE.ny)   THEN
              hh(in-ii+idx+1,jn-jj+idx+1) = OroOBS(in,jn)
            ELSE
              hh(in-ii+idx+1,jn-jj+idx+1) = 0.
            END IF

              NST_sh(i,j) = NST_sh(i,j)   + hh(in-ii+idx+1,jn-jj+idx+1)
              nydata      = nydata        + 1
          ENDDO
          ENDDO
        END IF

C +   Nested Grid Cell Average
C +   ~~~~~~~~~~~~~~~~~~~~~~~~
            NST_sh(i,j) = NST_sh(i,j)   / nydata
c #WR   write(6,6004) NST__x(i,j),NST__y(i,j),NST_sh(i,j),ii,jj
 6004   format(3f15.3,2i6)

C +   No Topography below Sea Level...
C +   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        IF (NST_sh(i,j).lt.0.0)                                     THEN
            NST_sh(i,j) =  0.0
        ENDIF

C +   Orography Roughness
C +   ~~~~~~~~~~~~~~~~~~~

C +              ******
        call     z_orog(inx        ,jny        ,xx,yy,ddxxi,hh,
     .           NST_dx,NST_sh(i,j),NSTsol(i,j),NSTzor(i,j))
C +              ******

c #WR   IF  (NSTzor(i,j).GT.0.) 
c #WR.         write(6,*) 'Before smooth ',i,j,NSTzor(i,j)

C +   Distinction between land and sea (further refined)
C +   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        IF (NST_sh(i,j).lt.0.01)                                    THEN
            NSTsol(i,j) =  1
        ELSE
            NSTsol(i,j) =  3
        ENDIF


        ENDIF                                 ! South of Drake Passage


C +     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        ENDDO                                 ! Loop on NST grid points
        ENDDO

C +     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


C +   OUTPUT (ASCII)
C +   ------

        write(6,*)'A_TOPO: OUTPUT File rampxkmdem_wgsosu_v2.dat OPENING'
        open(unit=2,status='new',file='rampxkmdem_wgsosu_v2.dat')
        rewind    2

C + nico: Fill in of the bays (sea mesh with 3 neighbouring land mesh  )
C +   (the ocean model OPA needs this to allow exchanges between meshes)
        
C ---------------------------------------------------------------------
        DO nn=1,2                     !the fill-in is processed 2 times
        DO  j=2,my-1
        DO  i=2,mx-1
          IF((NSTsol(i,j)                 .ne.3)  .AND.
     .      ((NSTsol(i-1,j)+NSTsol(i+1,j)
     .       +NSTsol(i,j-1)+NSTsol(i,j+1)).ge.9)) THEN
              NSTsol(i,j)=3  !the detected bay is filled in
              write(6,*) i,j,':this bay is filled in-----------------'
          ENDIF
        ENDDO
        ENDDO
        ENDDO
C ---------------------------------------------------------------------
        

        DO  j=2,my-1
        DO  i=2,mx-1
          aux_z0(i,j) = 0.0625
     .    *(   NSTzor(i+1,j-1) + 2.*NSTzor(i+1,j) +    NSTzor(i+1,j+1)
     .    + 2.*NSTzor(i  ,j-1) + 4.*NSTzor(i  ,j) + 2.*NSTzor(i  ,j+1)
     .    +    NSTzor(i-1,j-1) + 2.*NSTzor(i-1,j) +    NSTzor(i-1,j+1))
c #WR   IF  (aux_z0(i,j).GT.0.) 
c #WR.         write(6,*) 'After  smooth ',i,j,aux_z0(i,j)
        ENDDO
        ENDDO

        DO  j=1,my
        DO  i=1,mx
          NSTzor(i,j) =                        aux_z0(i,j)
          write(2,100) NST_sh(i,j),NSTsol(i,j),NSTzor(i,j)
c #WR   IF  (NSTzor(i,j).GT.0.) 
c #WR.         write(6,*) 'After  smooth ',i,j,NSTzor(i,j)

        END DO
        END DO
          write(2,201) NSTrcl
 201      format(e15.6,' is the relative colatitude',
     .                 ' where the distances on the projection plane',
     .                 ' are true')

        close(unit=2)
        write(6,*)'A_TOPO: OUTPUT File rampxkmdem_wgsosu_v2.dat OUT'
        write(6,*)' '
        write(6,*)' '


C +   OUTPUT (netcdf)
C +   ------

        x0unit        = 'km'
        y0unit        = 'km'
        z0unit        = 'm'
        w0unit        = '-'
        OROcdf        = 'MARdom_1_km_DEM.cdf'
        write(OROcdf(8:9),'(i2)') idx *2
        IF   (OROcdf(8:8).EQ.' ') OROcdf(8:8) = '_'
        Title0( 1:40) = 'MAR Antarctic Topography from RAMP 1 km '
        Title0(41:80) = 'DEM, version 2, NSIDC, Liu et al., 2001 '
C +                      1234567890123456789012345678901234567890

        DO i=1,mx
          x__MAR(i)   = NSTgdx(i)
        ENDDO

        DO j=1,my
          y__MAR(j)   = NSTgdy(j)
        ENDDO

        DO j=1,my
        DO i=1,mx
          LonMAR(i,j) = NST__x(i,j)
          LatMAR(i,j) = NST__y(i,j)
          OroMAR(i,j) = NST_sh(i,j)
          Oro_z0(i,j) = NSTzor(i,j)
          SolMAR(i,j) = NSTsol(i,j)
        ENDDO
        ENDDO

C +          ******
        call for2nc(1,1,ID__nc,2001,12,31,0,0,0
     .             , x0unit  , y0unit  , z0unit  , w0unit
     .             ,'       ','       ','       ','       ','       '
     .             ,'       ','       ','       ','       ','       '
     .             ,'       ','       ','       ','       ','       '
     .             ,'       ','       ','       ','       ','       '
     .             , OROcdf  ,Title0,'MARoro.dat')
C +          ******

C +   +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


C +   AWS MAR-Coordinates
C +   ===================

C +          ******
        call AWSgeo(x_RAMP,y_RAMP)
C +          ******

c #WR   STOP "Arret TEST"

      END IF

      RETURN
      END
