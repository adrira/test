 
!-NST domain dimensions
! ---------------------
 
      INTEGER mx,my,mz,mzabso,mw,nvx,nsl,nsno,nbdom

      PARAMETER (mx    = 120)   ! X-grid
      PARAMETER (my    = 110)   ! Y-grid
      PARAMETER (mz    =  24)   ! Z-grid
      PARAMETER (mzabso=   4)   ! Z-grid

      PARAMETER (nvx   =   3)   ! Sub-division of grid cell (SVAT)
      PARAMETER (mw    = nvx)   ! Sub-division of grid cell (Deardorff)

      PARAMETER (nsl   =   7)   ! Soil layers               (SVAT)
      PARAMETER (nsno  =  15)   ! Snow layers               (Snow model)
      PARAMETER (nbdom =   2)   ! Number of continents      ("GLOveg.f")

 
 
 
!-Selector for vectorization of the MAR code
! ------------------------------------------
 
      LOGICAL    vector
 
      PARAMETER (vector = .false.)
 
!     "vector" is true only if the MAR code is run on vectorial computer
 
 
 
!-LSC domain dimensions
! ---------------------
 
      INTEGER ni,nj,njv,nk,bi,bj,isLMz
 
!     A sub-region of the external large-scale domain is defined in order to 
!     reduced the CPU cost and the memory requirement for the interpolation.
 
!-1. SIZE of the SUB-REGION (in grid points)
 
      PARAMETER (isLMz = 0)

      PARAMETER (ni = 360)
      PARAMETER (nj =  50)
      PARAMETER (njv=  nj-isLMz)
      PARAMETER (nk =  42)
  
!     Warning:
!     For LMDz, you may use the scalar grid size, nj= size(lat_s)
!     but in that case, you must set   isLMz=1 (njv = nj-1)
!     For all other models, please set isLMz=0 (njv = nj  )
 
!-2. BEGINNING INDEX of the SUB-REGION
 
      PARAMETER (bi =  1)
      PARAMETER (bj =  1)
 
 
!-Dimensions of the RELAXATION ZONE towards LATERAL BOUNDARIES
! ------------------------------------------------------------
 
      INTEGER n6,n7,n8,n9,n10 
      PARAMETER(n7 = 7)
!     ......... ^ number of grid points of the relaxation zone
 
      PARAMETER(n10= 2)
!     ......... ^ number of grid points of constant topo. zone
 
      PARAMETER(n8 = 3)
!     ......... ^ number of grid points of the topography
!                 transition zone (valid if using LS constant
!                 topography at boundaries).
 
      PARAMETER(n6 =n7 -1)
      PARAMETER(n9 =n10-1)
 
 
!     Explanation of boundary structure :
!     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
!     1. TOPOGRAPHY
!     -------------
!
!        |   Constant  | Transition |  Computation  | Transition |  Constant  |
!        |  topography |    zone    |    domain     |    zone    | topography |
!        |     zone    | (LS -> MAR)|               | (LS -> MAR)|    zone    |
!        ^             ^            ^               ^            ^            ^
!        1    ...     n10  ...  n10+n8+1  ...  mx-n9-n8-1 ...  mx-n9   ...   mx
!
!     2. RELAXATION LSC --> NST
!     -------------------------
!
!        |      Relaxation     |      Computation      |      Relaxation      |
!        |         zone        |        domain         |         zone         |
!        ^                     ^                       ^                      ^
!        1         ...        n7         ...         mx-n6        ...        mx
