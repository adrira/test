C   +-------------------------------------------------------------------+
C   |  Subroutine GLOcov                              Jan 2016  NESTING |
C   +-------------------------------------------------------------------+

      SUBROUTINE GLOcov

      IMPLICIT none


C +---General variables
C +   -----------------

      INCLUDE 'NSTdim.inc'
      INCLUDE 'NSTvar.inc'
      INCLUDE 'LOCfil.inc'
      INCLUDE 'NetCDF.inc'
      INCLUDE 'NESTOR.inc'

      real   ,parameter :: reso=0.00833333
      integer,parameter :: cx  = 43200
      integer,parameter :: cy  = 21600


      integer  in,jn,i,j,k,l,kk,ll
      integer  NET_ID,NETcid,Rcode,start(2),count(2)
      integer  ilc(mw+1),lcmax
      integer  cov,NET_ID2,NETcid2,icemask

      real     dx1,dx2,dy1,dy2,previous_dx1,previous_dx2
      real     lc(-1:13),nbr_lc,diff,sum1,sum2

      NETcid = NCOPN("input/VEGE/glcesa3a.nc",NCNOWRIT,Rcode)
      NET_ID = NCVID(NETcid,'Band1',Rcode)

      write(6,*) 'GlobCover V.2.2 Land Cover'
      write(6,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~'
      write(6,*) ' '

      IF (region.eq."GRD".or.region.eq."ANT") THEN
       NETcid2= NCOPN("input/ICEmask/ICEmask_full.nc",NCNOWRIT,Rcode)
       NET_ID2= NCVID(NETcid2,'MASK',Rcode)

      write(6,*) 'Ice mask ESA CCI Land Cover User Tool (v.3.10)'
      write(6,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
      write(6,*) ' '

      ENDIF

      previous_dx1=5
      previous_dx2=5

      DO j=1,my
      WRITE(6,'(i4,$)') j
      DO i=1,mx

!      do i=93,93 ; do j=120,120

C +   *****
      IF(NSTsol(i,j)>=3)THEN
C +   *****
 
       NSTsol(i,j)=max(4,NSTsol(i,j))

       dx1=abs(NST__x(i,j)-
     .      NST__x(max(1,min(mx,i-1)),max(1,min(my,j))))
       dx2=abs(NST__x(i,j)-
     .      NST__x(max(1,min(mx,i+1)),max(1,min(my,j))))

       dy1=abs(NST__y(i,j)-
     .      NST__y(max(1,min(mx,i  )),max(1,min(my,j-1))))
       dy2=abs(NST__y(i,j)-
     .      NST__y(max(1,min(mx,i  )),max(1,min(my,j+1))))

       if(dx1<50.and.dx2<50) then 
        dx1=dx1/(2.*reso)
        dx2=dx2/(2.*reso)
       else
        dx1=previous_dx1
        dx2=previous_dx2     
       endif

       dy1=dy1/(2.*reso)
       dy2=dy2/(2.*reso)

       in=nint((NST__x(i,j)+180.)/reso)
       jn=nint((NST__y(i,j)+ 90.)/reso)    

       nbr_lc=0
 
       do while(nbr_lc==0)

        lc=0.

        do k=in-nint(dx1),in+nint(dx2) ; do l=jn-nint(dy1),jn+nint(dy2) 

 
                   kk=k 
                   ll=l
         if(kk<1)  kk=cx+kk
         if(ll<1)  ll=cy+ll
         if(kk>cx) kk=kk-cx
         if(ll>cy) ll=ll-cy

         kk=max(1,min(cx,kk))
         ll=max(1,min(cy,ll)) 

         start(1)=kk
         start(2)=ll
         count(1)=1
         count(2)=1
         icemask =0 

         Rcode = nf_get_vara_int(NETcid,NET_ID,start,count,cov)

         IF (region.eq."GRD".or.region.eq."ANT") THEN
         Rcode = nf_get_vara_int(NETcid2,NET_ID2,start,count,icemask)
         ENDIF

         if(cov<0) cov=cov+256

         IF ((region.eq."GRD".or.region.eq."ANT").and.icemask>0) THEN 
                      lc(-1)=lc(-1)+1.! Permanent snow and ice
         ELSE

         if(cov==11)  lc(0)=lc(0)+1.  ! NO     VEGETATION
         if(cov==14)  lc(1)=lc(1)+1.  ! Rainfed croplands    
         if(cov==20)  lc(2)=lc(2)+1.  ! Mosaic cropland (50-70%) / vegetation (grassland/shrubland/forest) (20-50%)
         if(cov==30)  lc(3)=lc(3)+1.  ! Mosaic vegetation (grassland/shrubland/forest) (50-70%) / cropland (20-50%)
         if(cov==40)  lc(9)=lc(9)+1.  ! Closed to open (>15%) broadleaved evergreen or semi-deciduous forest (>5m)
         if(cov==50)  lc(9)=lc(9)+1.  ! Closed (>40%) broadleaved deciduous forest (>5m)
         if(cov==60)  lc(8)=lc(8)+1.  ! Open (15-40%) broadleaved deciduous forest/woodland (>5m)
         if(cov==70)  lc(12)=lc(12)+1.! Closed (>40%) needleleaved evergreen forest (>5m)
         if(cov==90)  lc(11)=lc(11)+1.! Open (15-40%) needleleaved deciduous or evergreen forest (>5m)
         if(cov==100) lc(7)=lc(7)+1.  ! Closed to open (>15%) mixed broadleaved and needleleaved forest (>5m)
         if(cov==110) lc(6)=lc(6)+1.  ! Mosaic forest or shrubland (50-70%) / grassland (20-50%) 
         if(cov==120) lc(5)=lc(5)+1.  ! Mosaic grassland (50-70%) / forest or shrubland (20-50%) 
         if(cov==130) lc(10)=lc(10)+1.! Closed to open (>15%) (broadleaved or needleleaved, evergreen or deciduous) shrubland (<5m)
         if(cov==140) lc(5)=lc(5)+1.  ! Closed to open (>15%) herbaceous vegetation (grassland, savannas or lichens/mosses)  
         if(cov==150) lc(4)=lc(4)+1.  ! Sparse (<15%) vegetation
         if(cov==160) lc(7)=lc(7)+1.  ! Closed to open (>15%) broadleaved forest regularly flooded (semi-permanently or temporarily)
         if(cov==170) lc(9)=lc(9)+1.  ! Closed (>40%) broadleaved forest or shrubland permanently flooded - Saline or brackish water
         if(cov==180) lc(5)=lc(5)+1.  ! Closed to open (>15%) grassland or woody vegetation on regularly flooded or waterlogged soil
         if(cov==190) lc(13)=lc(13)+1.! Artificial surfaces and associated areas (Urban areas >50%)  
         if(cov==200) lc(0)=lc(0)+1.  ! Bare areas  
         if(cov==220) lc(0)=lc(0)+1   ! Permanent snow and ice => Bare areas

         ENDIF

        enddo ; enddo


        nbr_lc=0

        do l=-1,13
         nbr_lc=nbr_lc+lc(l)
        enddo

        dx1=dx1*1.5
        dx2=dx2*1.5
        dy1=dy1*1.5
        dy2=dy2*1.5

       enddo

      ilc=-1
 
      do l=1,mw-1

       lcmax=0

       do k=0,13

        if(k/=1) then

        if(l==1.and.lc(k)>=lcmax) then
         lcmax=lc(k)
         ilc(l)=k
        endif

        if(l==2.and.k/=ilc(1).and.lc(k)>=lcmax) then
         lcmax=lc(k)
         ilc(l)=k
        endif

        if(l==3.and.k/=ilc(1).and.k/=ilc(2).and.lc(k)>=lcmax) then
         lcmax=lc(k)
         ilc(l)=k
        endif

        if(l==4.and.k/=ilc(1).and.k/=ilc(2).and.
     .     k/=ilc(3).and.lc(k)>=lcmax) then
         lcmax=lc(k)
         ilc(l)=k
        endif

        if(l==5) then 
         print *,"mw>5!!" ; stop
        endif

        endif

       enddo
   
       enddo
 
       nbr_lc=0

       do l=-1,13
        nbr_lc=nbr_lc+lc(l)
       enddo 

       IF (region.eq."GRD".or.region.eq."ANT")THEN
       if(lc(-1)>0.1*nbr_lc.and.ilc(1)/=-1) then ! 10%
        do k=mw+1,2,-1
         ilc(k)=ilc(k-1)
        enddo
        ilc(1)=-1
       endif
       ENDIF

       do k=1,mw-1

        if(lc(ilc(k))>=0)then
        NSTsvt(i,j,k)=ilc(k)
        NSTsfr(i,j,k)=lc(ilc(k))/nbr_lc * 100.
        else
        NSTsvt(i,j,k)=0
        NSTsfr(i,j,k)=0
        endif

       enddo

       if(NSTsvt(i,j,1)==-1.and.NSTsfr(i,j,1)>0) then
         NSTice(i,j)   = NSTsfr(i,j,1)
       else
         NSTice(i,j)   = 0.
       endif
       

       NSTsvt(i,j,mw)=1.
       NSTsfr(i,j,mw)=0.

       do l=1,mw-1 
        NSTsfr(i,j,l) = min(100.,max(0.,NSTsfr(i,j,l)))
        NSTsfr(i,j,mw)= NSTsfr(i,j,mw) +NSTsfr(i,j,l)
       enddo

       if(NSTsfr(i,j,mw)>100.0001) then
        print *,"ERROR: NSTsrf>100",i,j
        do l=1,mw-1 
         print *,l,NSTsvt(i,j,l),NSTsfr(i,j,l)
        enddo
        stop
       endif

       NSTsfr(i,j,mw) = min(100.,max(0.,100. - NSTsfr(i,j,mw)))

       sum1=0
       do l=1,mw 
        sum1=sum1+NSTsfr(i,j,l)
       enddo

       do l=1,mw 
        NSTsfr(i,j,l) = NSTsfr(i,j,l)/sum1*100.
        NSTveg(i,j,k) = NSTsvt(i,j,k)
        NSTvfr(i,j,k) = NSTsfr(i,j,k)
       enddo

       DO l=1,nvx
        IF (NSTsvt(i,j,l).le. 0) NSTlmx(i,j,l) = 0.0
        IF (NSTsvt(i,j,l).eq. 1) NSTlmx(i,j,l) = 0.6
        IF (NSTsvt(i,j,l).eq. 2) NSTlmx(i,j,l) = 0.9
        IF (NSTsvt(i,j,l).eq. 3) NSTlmx(i,j,l) = 1.2
        IF (NSTsvt(i,j,l).eq. 4) NSTlmx(i,j,l) = 0.7
        IF (NSTsvt(i,j,l).eq. 5) NSTlmx(i,j,l) = 1.4
        IF (NSTsvt(i,j,l).eq. 6) NSTlmx(i,j,l) = 2.0
        IF (NSTsvt(i,j,l).eq. 7.or.NSTsvt(i,j,l).eq.10)
     .    NSTlmx(i,j,l) = 3.0
        IF (NSTsvt(i,j,l).eq. 8.or.NSTsvt(i,j,l).eq.11)
     .    NSTlmx(i,j,l) = 4.5
        IF (NSTsvt(i,j,l).eq. 9.or.NSTsvt(i,j,l).eq.12)
     .    NSTlmx(i,j,l) = 6.0

        NSTlai(i,j,l) = NSTlmx(i,j,l)
        NSTglf(i,j,l) = 1.0

       ENDDO

C +   ****
      ELSE   ! Ocean, ice, snow
C +   ****

       NSTsvt(i,j,nvx)=  0
       NSTsfr(i,j,nvx)=100
       NSTveg(i,j,nvx)= -1
       NSTvfr(i,j,nvx)=100
       DO l=1,nvx
        NSTlai(i,j,l) = 0.0
        NSTglf(i,j,l) = 0.0
       ENDDO

       previous_dx1=dx1
       previous_dx2=dx2

C +   *****
      ENDIF  ! Continental areas
C +   *****

      ENDDO
      ENDDO

      IF (region.eq."GRD") call USRgrd ("GLOcov")

      END SUBROUTINE
