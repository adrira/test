C   +-------------------------------------------------------------------+
C   |  Subroutine HWSDsl                              Oct 2016  NESTING |
C   +-------------------------------------------------------------------+

      SUBROUTINE HWSDsl

      IMPLICIT none


C +---General variables
C +   -----------------

      INCLUDE 'NSTdim.inc'
      INCLUDE 'NSTvar.inc'
      INCLUDE 'LOCfil.inc'
      INCLUDE 'NetCDF.inc'
      INCLUDE 'NESTOR.inc'

      real   ,parameter :: reso=0.00833333
      integer,parameter :: cx  = 43200
      integer,parameter :: cy  = 17760


      integer  in,jn,i,j,k,l,kk,ll
      integer  NET_ID,NETcid,Rcode,start(2),count(2)
      integer  ilc(mw+1),lcmax
      integer  cov

      real     dx1,dx2,dy1,dy2,previous_dx1,previous_dx2
      real     lc(0:13),nbr_lc,diff,sum1,sum2

      NETcid = NCOPN("input/SOIL/HWSDglob.nc",NCNOWRIT,Rcode)
      NET_ID = NCVID(NETcid,'HWS',Rcode)

      write(6,*) ' '
      write(6,*) 'HSWD SOIL Cover'
      write(6,*) '~~~~~~~~~~~~~~~'
      write(6,*) ' '

      previous_dx1=5
      previous_dx2=5

      DO j=1,my
      WRITE(6,'(i4,$)') j
      DO i=1,mx

!      do i=93,93 ; do j=120,120

C +   *****

      IF(NSTsol(i,j)>=3.)THEN

C+    +---No data areas !CKittel 07/10/16
C+    if HWSDsl.f is very slow, please verify 
C+    http://webarchive.iiasa.ac.at/Research/LUC/External-World-soil-database/HTML/
C+    and if no data over your area add it below

      IF (NST__y(i,j)<-58                          .or.  !Antarctica
     .   (NST__y(i,j)<-40   .and. NST__y(i,j)>-55        !Kerguelen     Island
     .                      .and. NST__x(i,j)> 60 
     .                      .and. NST__X(i,j)< 75) .or.
     .   (NST__y(i,j)<-46.5 .and. NST__y(i,j)>-47.25     !Prince Edward Islands
     .                      .and. NST__x(i,j)> 37 
     .                      .and. NST__X(i,j)< 38) .or.
     .   (NST__y(i,j)<-50   .and. NST__y(i,j)>-55        !South Georgia and the South Sandwich Islands
     .                      .and. NST__x(i,j)>-42 
     .                      .and. NST__X(i,j)<-30)   
     .                                              ) then 
      GOTO 2222
      endif
C +   *****

       dx1=abs(NST__x(i,j)-
     .         NST__x(max(1,min(mx,i-1)),max(1,min(my,j))))
       dx2=abs(NST__x(i,j)-
     .         NST__x(max(1,min(mx,i+1)),max(1,min(my,j))))

       dy1=abs(NST__y(i,j)-
     .         NST__y(max(1,min(mx,i  )),max(1,min(my,j-1))))
       dy2=abs(NST__y(i,j)-
     .         NST__y(max(1,min(mx,i  )),max(1,min(my,j+1))))



       if(dx1<50.and.dx2<50) then 
        dx1=dx1/(2.*reso)
        dx2=dx2/(2.*reso)
       else
        dx1=previous_dx1
        dx2=previous_dx2     
       endif

       dy1=dy1/(2.*reso)
       dy2=dy2/(2.*reso)

       in=nint((NST__x(i,j)+180.)/reso)
       jn=nint((NST__y(i,j)+ 58.)/reso)  
  

       nbr_lc=0
 
       do while(nbr_lc==0)
        
        lc=0.

        do k=in-nint(dx1),in+nint(dx2) ; do l=jn-nint(dy1),jn+nint(dy2) 

 
                   kk=k 
                   ll=l
         if(kk<1)  kk=cx+kk
         if(ll<1)  ll=cy+ll
         if(kk>cx) kk=kk-cx
         if(ll>cy) ll=ll-cy

         kk=max(1,min(cx,kk))
         ll=max(1,min(cy,ll)) 

         start(1)=kk
         start(2)=ll
         count(1)=1
         count(2)=1
 
         Rcode = nf_get_vara_int(NETcid,NET_ID,start,count,cov)

         if(cov==1)   lc(11)= lc(11)+1
         if(cov==2)   lc(10)= lc(10)+1
         if(cov==3)   lc(11)= lc(11)+1
         if(cov==4)   lc(7) = lc(7) +1
         if(cov==5)   lc(8) = lc(8) +1 
         if(cov==6)   lc(4) = lc(4) +1
         if(cov==7)   lc(4) = lc(4) +1
         if(cov==8)   lc(9) = lc(9) +1
         if(cov==9)   lc(5) = lc(5) +1
         if(cov==10)  lc(6) = lc(6) +1
         if(cov==11)  lc(3) = lc(3) +1
         if(cov==12)  lc(2) = lc(2) +1
         if(cov==13)  lc(1) = lc(1) +1

         if(cov==-1)  lc(0) = lc(0)+1

        enddo ; enddo


        nbr_lc=0

        do l=0,12
         nbr_lc=nbr_lc+lc(l)
        enddo

        dx1=dx1*1.5
        dx2=dx2*1.5
        dy1=dy1*1.5
        dy2=dy2*1.5
 

       enddo

      ilc=-1
 
      lcmax=0 ; l=1

      do k=0,12

       if(l==1.and.lc(k)>=lcmax) then
        lcmax=lc(k)
        ilc(l)=k
       endif
   
      enddo
 
      if(ilc(l)>0) then
      NSTtex(i,j) = ilc(l)
      NSTtex(i,j) = max(1   ,min(12 ,NSTtex(i,j)))
      endif
     
C +   *****
2222  continue
      ENDIF  ! Continental area
C +   *****

      ENDDO
      ENDDO

      END SUBROUTINE HWSDsl
