C   +-------------------------------------------------------------------+
C   |  Subroutine MARoutp                       September 2016  NESTING |
C   +-------------------------------------------------------------------+
C   |                                                                   |
C   | Input : Interpolated LSC (large-scale) fields                     |
C   | ^^^^^^^              or sounding                                  |
C   |                                                                   |
C   | Output: Creation of MARdyn.DAT (initialization)                   |
C   | ^^^^^^^             MARsol.DAT (       "      )                   |
C   |                     MARlbc.DAT (bound. forcing)                   |
C   |                     MARubc.DAT (bound. forcing)                   |
C   |                     MARsic.DAT (bound. forcing / Sea-Ice)         |
C   |                     MARdom.dat (surf. characteristics)            |
C   |         Note that *.DAT file can be written according to ASCII    |
C   |         or Binary format, depending on ASCfor variable.           |
C   |                                                                   |
C   +-------------------------------------------------------------------+

      SUBROUTINE MARoutp


      IMPLICIT NONE


C +---General variables
C +   -----------------

      INCLUDE 'NSTdim.inc'
      INCLUDE 'NESTOR.inc'
      INCLUDE 'NSTvar.inc'
      INCLUDE 'MARvar.inc'
      INCLUDE 'SNDvar.inc'
      INCLUDE 'CTRvar.inc'


C +---Local variables
C +   ---------------

      INTEGER i,j,k,l,n,ifh,nbchar,losnd,jjmez,
     .        veg_1D(mx),iwf_1D(mx),svt_1D(mx,nvx),sfr_1D(mx,nvx),
     .        isol1D(mx),ii1,ii2,jj1,jj2,tmpveg,mmx,mmy,m1,m2,
     .        ic1,ic2,jc1,jc2,ip11,jp11,mx1,mx2,my1,my2


      REAL    pcap,WK2_1D(mz),zesnd,TMP_dd(0:mzm),compt,compt1
      
      REAL    sst1D(mx),dsa_1D(mx),lai_1D(mx,nvx),SH_1D(mx),
     .        glf_1D(mx,nvx),d1__1D(mx),ts__1D(mx,nvx,nsl),
     .        sw__1D(mx,nvx,nsl),compt2,z0__1D(mx,mw),
     .        r0__1D(mx,mw),ch0_1D(mx),rsur1D(mx),alb01D(mx),
     .        eps01D(mx)

      REAL    uairUB(mx,my,mzabso),vairUB(mx,my,mzabso)
      REAL    pktaUB(mx,my,mzabso)
      REAL     Zorro(mx,my,mw)


! For marp (parallel version of MAR) and MARdynp.DAT, MARsvtp.DAT, MARsicp.DAT, MARglfp.DAT
! ----------------------------------     -----------  -----------  -----------  -----------
 
      LOGICAL NSTmap
      INTEGER jt_000
      REAL    rt_000
      REAL    pi
      REAL    NSTloh(mx,my)
      REAL    NSTlar(mx,my)
      REAL    NST_mz(mx,my,mz)
      REAL    NSTmzz(mx,my,mzz)
      REAL    sigmzz(0:mzz)

      INTEGER iwk_xy(mx,my)
      INTEGER iwkxyn(mx,my,mw)
      REAL    wk1_xy(mx,my)
      REAL    wk2_xy(mx,my)
      REAL    wk_xyn(mx,my,mw)


      CHARACTER*7  cklon
      CHARACTER*10 NSTinfo

      LOGICAL NSTini,NSTfor,NSTend,Vfalse


C +---Thermodynamical Constants (Atmosphere)
C +   --------------------------------------

      DATA pcap  /   3.730037070d0/
C +...     pcap  = 100 ** (R / Cp)


C +---Data
C +   ----

      DATA Vfalse  / .false. /
      DATA NSTmap  / .true.  /


      jt_000     =       0
      rt_000     =       0.
      pi         = acos(-1.)


C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


C +---Dates
C +   -----

      NSTini=.false.
      NSTend=.false.
      NSTfor=.true. 

      IF (DATtim.eq.DATini) NSTini=.true.
      IF (DATtim.eq.DATfin) NSTend=.true.


C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


C +---READING OF GRID PARAMETERS IN MARgrd.ctr
C +   ========================================

      OPEN (unit=51,status='old',file='MARgrd.ctr')

       read (51,*) !- - - - - - - - - - - - - - - - - -
       read (51,*) !- - - - - - - - - - - - - - - - - -
       read (51,*) !- - - - - - - - - - - - - - - - - -
       read (51,*) !- - - - - - - - - - - - - - - - - -
       read (51,*) maptyp
       read (51,*) !- - - - - - - - - - - - - - - - - -
       read (51,*) GElon0
       read (51,*) imez
       read (51,*) GElat0
       read (51,*) jmez
       read (51,*) !- - - - - - - - - - - - - - - - - -
       read (51,*) dx
       read (51,*) !- - - - - - - - - - - - - - - - - -
       read (51,*) GEddxx
       read (51,*) !- - - - - - - - - - - - - - - - - -
       read (51,*) ptopDY
       read (51,*) !- - - - - - - - - - - - - - - - - -
       read (51,*) zmin
       read (51,*) aavu
       read (51,*) bbvu
       read (51,*) ccvu
       read (51,*) !- - - - - - - - - - - - - - - - - -
       read (51,'(l4)') vertic
       read (51,*) !- - - - - - - - - - - - - - - - - -
       read (51,*) sst_SL
       read (51,*) !- - - - - - - - - - - - - - - - - -

      CLOSE(unit=51)


C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


C +---DATE
C +   ====


      itexpe = 0

C +        ******
      CALL DATcnv (RUNiyr,mmaDYN,jdaDYN,jhuDYN,DATtim,Vfalse)
C +        ******

      iyrDYN=RUNiyr

      IF (DATtim.eq.DATfin) THEN
       jdh_LB=0
      ELSE
       jdh_LB=DAT_dt
      ENDIF


C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


C +---PREPARATION OF VARIABLES TO BE WRITTEN
C +   ======================================


C +---Surface characteristics
C +   -----------------------

      DO j=1,my
      DO i=1,mx
       d1_SL (i,j)  =NST_d1(i,j)
       alb0SL(i,j)  =NSTalb(i,j)
       eps0SL(i,j)  =NSTeps(i,j)
       DO n=1,mw
c #ZO   IF (region.eq."ANT")                                        THEN
        Zorro (i,j,n)=NSTzor(i,j)
c #ZO   ELSE
c #ZO   Zorro (i,j,n)=NST_z0(i,j)
c #ZO   ENDIF
        SL_z0 (i,j,n)=NST_z0(i,j)
        SL_r0 (i,j,n)=NST_r0(i,j)
       ENDDO
       rsurSL(i,j)  =NSTres(i,j)
       ch0SL (i,j)  =NSTch0(i,j)
       ro_SL (i,j)  =0.0
      ENDDO
      ENDDO


C +---Surface layer variables
C +   -----------------------

      DO j=1,my
      DO i=1,mx
       tairSL(i,j)  =NST_st(i,j)
       t2_SL (i,j)  =NSTdst(i,j)
       DO n=1,mw
        tsrfSL(i,j,n)=NST_st(i,j)   ! Bloc Temporaire, a modif 
        SLsrfl(i,j,n)=0.
        SLuusl(i,j,n)=0.
        SLutsl(i,j,n)=0.
       ENDDO
       nSLsrf(i,j)  =1
       SLsrfl(i,j,1)=1.
C +
       qvapSL(i,j)  =1.e-5
       w2_SL (i,j)  =0.15 
       wg_SL (i,j)  =0.15 
       roseSL(i,j)  =0.
       hmelSL(i,j)  =0.
       hsnoSL(i,j)  =0.
       SaltSL(i,j)  =0.
      ENDDO
      ENDDO


C +---Prognostic variables
C +   --------------------

      DO j=1,my
      DO i=1,mx
       DO k=1,mz
        NST_qv(i,j,k)=MAX(1.e-5,NST_qv(i,j,k))
        NSTtmp(i,j,k)=NST_pt(i,j,k)/pcap
       ENDDO
       NSTtmp(i,j,mz+1)=NST_pt(i,j,mz)/pcap
       pstDY (i,j)     =NST_sp(i,j)-ptopDY
      ENDDO
      ENDDO

C +...uairDY <-- NST__u
C +...vairDY <-- NST__v
C +...qvDY   <-- NST_qv
C +...pktaDY <-- NSTtmp


C +---Boundary variables
C +   ------------------

      IF (NSTmod.ne.'CPL') THEN

       DO k=1,mzabso
        DO j=1,my
         DO i=1,mx
           uairUB(i,j,k) = NST__u (i,j,k)
           vairUB(i,j,k) = NST__v (i,j,k)
           pktaUB(i,j,k) = NSTtmp (i,j,k)
         ENDDO
        ENDDO
       ENDDO
       DO k=1,mz
        DO j=1,my
         DO i=1,n7
          vaxgLB(i,j,k,1) = NST__u (i,j,k)
          vaxgLB(i,j,k,2) = NST__v (i,j,k)
          vaxgLB(i,j,k,3) = NST_qv (i,j,k)
          vaxgLB(i,j,k,4) = NSTtmp (i,j,k)
          vaxgLB(i,j,1,5) = pstDY  (i,j)
          vaxgLB(i,j,mz,5)= tsrfSL (i,j,1)
         ENDDO
         DO i=mx-n6,mx
          vaxdLB(i,j,k,1) = NST__u (i,j,k)
          vaxdLB(i,j,k,2) = NST__v (i,j,k)
          vaxdLB(i,j,k,3) = NST_qv (i,j,k)
          vaxdLB(i,j,k,4) = NSTtmp (i,j,k)
          vaxdLB(i,j,1,5) = pstDY  (i,j)
          vaxdLB(i,j,mz,5)= tsrfSL (i,j,1)
         ENDDO
        ENDDO
        DO i=1,mx
         DO j=1,n7
          vayiLB(i,j,k,1) = NST__u (i,j,k)
          vayiLB(i,j,k,2) = NST__v (i,j,k)
          vayiLB(i,j,k,3) = NST_qv (i,j,k)
          vayiLB(i,j,k,4) = NSTtmp (i,j,k)
          vayiLB(i,j,1,5) = pstDY  (i,j)
          vayiLB(i,j,mz,5)= tsrfSL (i,j,1)
         ENDDO
         DO j=my-n6,my
          vaysLB(i,j,k,1) = NST__u (i,j,k)
          vaysLB(i,j,k,2) = NST__v (i,j,k)
          vaysLB(i,j,k,3) = NST_qv (i,j,k)
          vaysLB(i,j,k,4) = NSTtmp (i,j,k)
          vaysLB(i,j,1,5) = pstDY  (i,j)
          vaysLB(i,j,mz,5)= tsrfSL (i,j,1)
         ENDDO
        ENDDO
       ENDDO

      ENDIF


C +---Sounding variables
C +   ------------------

      DO k=0,mzm
       TMP_dd(k)=90.-SND_dd(k)
       IF (TMP_dd(k).lt.  0.) TMP_dd(k)=TMP_dd(k)+360.
       IF (TMP_dd(k).gt.360.) TMP_dd(k)=TMP_dd(k)-360.
      ENDDO


C +---Soil variables
C +   --------------

      DO j=1,my
      DO i=1,mx
       
                                 isolSL(i,j)=NSTsol(i,j)
!      IF (region.eq."AFW".OR.region.eq."WAF".OR.region.eq."ALP")   THEN
                                 isolTV(i,j)=NSTtex(i,j)
!      ELSE                                      ! FAO DATA not yet used
!          IF (NSTtex(i,j).eq.1) isolTV(i,j)=2   ! loamy sand
!          IF (NSTtex(i,j).eq.2) isolTV(i,j)=5   ! sand
!          IF (NSTtex(i,j).eq.3) isolTV(i,j)=11  ! clay
!      ENDIF
        
       IF (region.eq."GRD".or.region.eq."ANT")                      THEN
           IF (NSTsol(i,j).le.2) isolTV(i,j)=0
           IF (NSTsol(i,j).eq.4) isolTV(i,j)=11
           IF (NSTsol(i,j).eq.3) isolTV(i,j)=12
C +...     Transform to SVAT (De Ridder) classification

       ENDIF
       
      ENDDO
      ENDDO


C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


C +---Some constants specific to MAR
C +   ==============================


C +---Deardorff Soil Model Parameters
C +   -------------------------------

      cs2SL  = 86400.0
      w20SL  = 0.15
      wg0SL  = 0.10
      wk0SL  = 0.15
      wx0SL  = 0.20


C +---Typical Roughness Lengths (m) for land, sea, snow
C +   -------------------------------------------------

      zl_SL  = 1.00e-1
      zs_SL  = 1.00e-3
      zn_SL  = 1.00e-4


C +---Inversion surface temperature
C +   -----------------------------

      dtagSL = 0.


C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


C +---Parameters of the vertical grid
C +   ===============================


C +        ******
      CALL SETsig (mz,zmin,aavu,bbvu,ccvu,ptopDY)
C +        ******

C +        ******
      CALL GRDsig(mz,zmin,aavu,bbvu,ccvu,vertic,
     .               sst_SL,TUkhmx,sigma,WK2_1D)
C +        ******



C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


C +---Specifications for horizontal grid and time step
C +   ================================================

      dx = 1000. *dx
      dy = dx
      dt = 4.e-3 *dx

      IF (NSTmod.eq.'M2D') THEN
       mmx   = mx
       mmy   = 1
       ii1   = 1
       ii2   = mx
       jj1   = jmez
       jj2   = jmez
       jjmez = 1
      ELSE
       IF (NSTmod.eq.'CPL') THEN
        mmx   = 1
        mmy   = 1
        ii1   = 2
        ii2   = 2
        jj1   = 2
        jj2   = 2
        jjmez = 1
       ELSE
        mmx   = mx
        mmy   = my
        ii1   = 1
        ii2   = mx
        jj1   = 1
        jj2   = my
        jjmez = jmez
       ENDIF
      ENDIF

      IF (SNDing) THEN
       imez = 1
       jmez = 1
       iyrDYN = SNDyar
       mmaDYN = SNDmma
       jdaDYN = SNDjda
       jhuDYN = SNDjhu
      ENDIF


C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


C +---1-D Topography
C +   ==============

      IF (NSTmod.eq.'M2D'.and.mmx.gt.1) THEN

       ic1 = MIN(2,mx)
       ic2 = MAX(1,mx-1)

       jc1 = MIN(2,my)
       jc2 = MAX(1,my-1)

       DO i=1,mx
 
        compt1    = 0.
        compt2    = 0.
        SH_1D(i)  = 0.
        isol1D(i) = 1
 
        DO j=jc1,jc2
         compt1   = compt1   + 1.
         SH_1D(i) = SH_1D(i) + NST_sh(i,j)
         IF (NSTsol(i,j).ge.3) THEN
          compt2  = compt2   + 1.
         ENDIF
        ENDDO
 
        IF (compt1.ge.1.) THEN
         SH_1D(i) = SH_1D(i) / compt1
        ENDIF

        IF (compt2.ge.(my/2)) isol1D(i) = 4
 
        IF (isol1D(i).le.2) THEN
         SH_1D(i) = 0.
        ENDIF

       ENDDO


C +....Topography filtering
C +    --------------------

       IF (TOPfilt) THEN

C +...  First filtering
        DO i=ic1,ic2
         IF (isol1D(i).ge.3) THEN
          SH_1D(i) = (SH_1D(i-2)+SH_1D(i-1)+2.*SH_1D(i)
     .               +SH_1D(i+1)+SH_1D(i+2)) / 6.0
         ENDIF
        ENDDO

C +...  Second filtering
        DO i=ic2,ic1,-1
         IF (isol1D(i).ge.3) THEN
          SH_1D(i) = (SH_1D(i-2)+SH_1D(i-1)+2.*SH_1D(i)
     .               +SH_1D(i+1)+SH_1D(i+2)) / 6.0
         ENDIF
        ENDDO

C +...  Third filtering
        DO i=ic1,ic2
         IF (isol1D(i).ge.3) THEN
          SH_1D(i) = (SH_1D(i-2)+SH_1D(i-1)+2.*SH_1D(i)
     .               +SH_1D(i+1)+SH_1D(i+2)) / 6.0
         ENDIF
        ENDDO

C +...  Fourth filtering
        DO i=ic2,ic1,-1
         IF (isol1D(i).ge.3) THEN
          SH_1D(i) = (SH_1D(i-2)+SH_1D(i-1)+2.*SH_1D(i)
     .               +SH_1D(i+1)+SH_1D(i+2)) / 6.0
         ENDIF
        ENDDO

C +...  Fifth filtering
        DO i=ic1,ic2
         IF (isol1D(i).ge.3) THEN
          SH_1D(i) = (SH_1D(i-1)+2.*SH_1D(i)+SH_1D(i+1)) / 4.0
         ENDIF
        ENDDO

C +...  Sixth filtering
        DO i=ic2,ic1,-1
         IF (isol1D(i).ge.3) THEN
          SH_1D(i) = (SH_1D(i-1)+2.*SH_1D(i)+SH_1D(i+1)) / 4.0
         ENDIF
        ENDDO

       ENDIF


       m1 = MIN(mx,n10)
       DO i=1,m1-1
        SH_1D(i)=SH_1D(m1)
       ENDDO

       m2 = MAX(1,mx-n10+1)
       DO i=m2+1,mx
        SH_1D(i)=SH_1D(m2)
       ENDDO


      ENDIF


C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


C +---1-D SST
C +   =======

      IF (NSTmod.eq.'M2D') THEN

       DO i=1,mx

        compt    = 0.
        sst1D(i) = 0.
 
        DO j=1,my
         IF (NSTsol(i,j).le.2) THEN
          IF (NSTsst(i,j).lt.1.) THEN
           compt    = compt    + 1.
           sst1D(i) = sst1D(i) + NST_st(i,j)
          ELSE
           compt    = compt    + 1.
           sst1D(i) = sst1D(i) + NSTsst(i,j)
          ENDIF
         ENDIF
        ENDDO
 
        IF (compt.ge.1.) THEN
         sst1D(i) = sst1D(i) / compt
        ENDIF
 
        IF (isol1D(i).ge.3) THEN
         sst1D(i) = 0.
        ENDIF
 
       ENDDO
 
      ENDIF
 

C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


C +---1-D Surface
C +   ===========

      IF (NSTini.and.NSTmod.eq.'M2D'.and.
     .    LoutDA.and.SELECT.eq.1        ) THEN


       DO i=1,mx

        ch0_1D(i) = 0.
        rsur1D(i) = 0.
        alb01D(i) = 0.
        eps01D(i) = 0.
        d1__1D(i) = 0.
        DO k=1,mw
         z0__1D(i,k) = 0.
         r0__1D(i,k) = 0.
        ENDDO

        DO j=1,my
         ch0_1D(i) = ch0_1D(i) + NSTch0(i,j)
         rsur1D(i) = rsur1D(i) + NSTres(i,j)
         alb01D(i) = alb01D(i) + NSTalb(i,j)
         eps01D(i) = eps01D(i) + NSTeps(i,j)
         d1__1D(i) = d1__1D(i) + NST_d1(i,j)
         DO k=1,mw
          z0__1D(i,k) = z0__1D(i,k) + NST_z0(i,j)
          r0__1D(i,k) = r0__1D(i,k) + NST_r0(i,j)
         ENDDO
        ENDDO

        ch0_1D(i) = ch0_1D(i) / REAL(my)
        rsur1D(i) = rsur1D(i) / REAL(my)
        alb01D(i) = alb01D(i) / REAL(my)
        eps01D(i) = eps01D(i) / REAL(my)
        d1__1D(i) = d1__1D(i) / REAL(my)
        DO k=1,mw
         z0__1D(i,k) = z0__1D(i,k) / REAL(my)
         r0__1D(i,k) = r0__1D(i,k) / REAL(my)
        ENDDO

       ENDDO


      ENDIF


C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


C +---1-D SISVAT variables
C +   ====================

      IF (NSTini.and.SVTmod.and.
     .    LoutDA.and.SELECT.eq.1.and.
     .    NSTmod.eq.'M2D'           ) THEN


       DO i=1,mx

        compt1    = 0.

        veg_1D(i) = 0
        tmpveg    = 0
        iwf_1D(i) = 0
        dsa_1D(i) = 0.
        DO k=1,nvx
         svt_1D(i,k) = 0
         sfr_1D(i,k) = 0
         lai_1D(i,k) = 0.
         DO l=1,nsl
          ts__1D(i,k,l) = 0.
          sw__1D(i,k,l) = 0.
         ENDDO
        ENDDO

        DO j=1,my
         IF (NSTsol(i,j).ge.3) THEN
          compt1    = compt1    + 1.
          tmpveg    = tmpveg    + NSTtex(i,j)
          iwf_1D(i) = iwf_1D(i) + NSTiwf(i,j)
          dsa_1D(i) = dsa_1D(i) + NSTdsa(i,j)
          DO k=1,nvx
           svt_1D(i,k) = svt_1D(i,k) + NSTsvt(i,j,k)
           sfr_1D(i,k) = sfr_1D(i,k) + NSTsfr(i,j,k)
           lai_1D(i,k) = lai_1D(i,k) + NSTlai(i,j,k)
           DO l=1,nsl
            ts__1D(i,k,l) = ts__1D(i,k,l) + NST_ts(i,j,k,l)
            sw__1D(i,k,l) = sw__1D(i,k,l) + NST_sw(i,j,k,l)
           ENDDO
          ENDDO
         ENDIF
        ENDDO
 
        IF (compt1.ge.1.) THEN
         tmpveg    = NINT (REAL(tmpveg)    / compt1)
         iwf_1D(i) = NINT (REAL(iwf_1D(i)) / compt1)
         dsa_1D(i) = dsa_1D(i) / compt1
         veg_1D(i) = NINT (REAL(veg_1D(i)) / compt1)
         IF (tmpveg.eq.1) veg_1D(i)=2   ! loamy sand
         IF (tmpveg.eq.2) veg_1D(i)=5   ! sand
         IF (tmpveg.eq.3) veg_1D(i)=11  ! clay
         DO k=1,nvx
          svt_1D(i,k) = NINT (REAL(svt_1D(i,k)) / compt1)
          sfr_1D(i,k) = NINT (REAL(sfr_1D(i,k)) / compt1)
          lai_1D(i,k) = lai_1D(i,k) / compt1
         ENDDO
         DO l=1,nsl
         DO k=1,nvx
          ts__1D(i,k,l) = ts__1D(i,k,l) / compt1
          sw__1D(i,k,l) = sw__1D(i,k,l) / compt1
         ENDDO
         ENDDO
        ENDIF

       ENDDO

      ENDIF


      IF (NSTfor.and.SVTmod.and.
     .    LoutDA.and.SELECT.eq.1.and.
     .    NSTmod.eq.'M2D'           ) THEN

       DO i=1,mx

        compt2    = 0.
        DO k=1,nvx
         glf_1D(i,k) = 0.
        ENDDO

        DO j=1,my
         IF (NSTsol(i,j).ge.4) THEN
          DO k=1,nvx
           compt2      = compt2      + 1.
           glf_1D(i,k) = glf_1D(i,k) + NSTlai(i,j,k)*NSTglf(i,j,k)
          ENDDO
         ENDIF
        ENDDO

        IF (compt2.ge.1.) THEN
         DO k=1,nvx
          IF (lai_1D(i,k).gt.0.) THEN
           glf_1D(i,k) = glf_1D(i,k) / compt2 / lai_1D(i,k)
           glf_1D(i,k) = MIN(1.0,glf_1D(i,k))
          ELSE
           glf_1D(i,k) = 0.0
          ENDIF
         ENDDO
        ENDIF

       ENDDO

      ENDIF


C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


C +---Filter parameters
C +   -----------------

      CALL MARfil(my,dx,dt,FIslot,FIslou,FIslop,
     .                     FIkhmn,TUkhff,TUkhmx)

C     Note (PhM): we give the opportunity to change FIslo* here
C       because standard value is high in comparison to
C       recomendations in 
C       Raymond and Garder, MWR 116, Jan 1988, p209 
C       (suggests 0.0075, while default is 0.05 in MARfil)
C       Note that we do not change  FIlkhm, which
C       is computed in MARfil and used in MAR:TURhor_dyn.f
C       (i.e.: I don't know the reason for changing it
C       with the filter; of course it also smooth horizontal
C       fields, but may be physically based (?) in contrast to
C       the filter, which should only eliminates 2dx)

      IF (NSTfis.GE.0.0001) THEN
         FIslop= NSTfis
         FIslou= FIslop      
         FIslot= FIslop
       ENDIF

      write(6,*) 'Write  files :'
      write(6,*) '~~~~~~~~~~~~~~'


C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


C +---Output directory
C +   ================

      nbchar=1

      DO i=1,60
       IF (NSTdir(i:i).ne.' ') nbchar=i
      ENDDO


C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

! Output file for parallel dynamics : MARdynp.DAT
! ===============================================

! ASCII OUTPUT for marp 
! ---------------------

      IF (NSTmap)                       THEN
      IF (NSTini.and.(.not.SNDing).and.
     .    LoutDA.and.SELECT.eq.1  .and.
     .    NSTmod.ne.'M2D'         .and.
     .    NSTmod.ne.'CPL'             ) THEN


          sigmzz(0)     = 0.0000
        DO k=1,mz
          sigmzz(k)     = sigma(k)
        DO j=1,my
        DO i=1,mx
          NST_mz(i,j,k) = 0.0000
          NSTmzz(i,j,k) = 0.0000
        ENDDO
        ENDDO
        ENDDO
           k=  mzz
          sigmzz(k)     = 1.0000
        DO j=1,my
        DO i=1,mx
          NSTmzz(i,j,k) = 0.0000
          NSTloh(i,j)   = NST__x(i,j)      /  15.
          NSTlar(i,j)   = NST__y(i,j) * pi / 180.
        ENDDO
        ENDDO
         
! ASCII format is more portable than binary format
! ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        write(6,*) ' '
        write(6,*) ' Ecriture MARdynp.DAT: Temps: ',iyrDYN,mmaDYN,jdaDYN,jhuDYN
        write(6,*) ' '

        open (unit=11,status='unknown'
     .       ,file=NSTdir(1:nbchar)//'MARtimp.DAT')
          write(11,114) jt_000            !  Iteration de debut d'experience: 0
          write(11,114) jt_000,jt_000     !  time_index_cdf                 : 0
 114      format(8I16)
          write(11,118) rt_000            !  Temps     de debut d'experience: 0.
 118      format(8E16.9)
        close(unit=11)

        open (unit=11,status='unknown'
     .       ,file=NSTdir(1:nbchar)//'MARdynp.DAT')
          write(11,114) mx-2
          write(11,114) my-2
          write(11,114) mz  
          write(11,114) mzabso
          write(11,114) mw  
          write(11,114) nsno
          write(11,114) nsl-1

          write(11,114) iyrDYN
          write(11,114) mmaDYN
          write(11,114) jdaDYN
          write(11,114) jhuDYN
          write(11,114) jt_000
          write(11,118) rt_000

          write(11,118) GElon0
          write(11,118) GElat0
          write(11,118) GEddxx
          write(11,118) NSTrcl
          write(11,114) imez
          write(11,114) jjmez

          write(11,114) isolSL            !
          write(11,118) NST_sh            !
       
          write(11,118) NSTloh            !  Longitude   [hours]
          write(11,118) NSTlar            !   Latitude [radians]

          NSTloh = dx
          NSTlar = dx
          write(11,118) NSTloh            !         dx  (x-axis)
          write(11,118) NSTlar            !         dx  (y-axis)

          write(11,118) TUkhmx            !  Upper Sponge, 0.1 * dx * dx / dt
          write(11,118) ptopDY            !
          write(11,118) sigmzz            !

          write(11,118) pstDY             !  ps_dyn
          write(11,118) NSTtmp            !  PT_dyn

        DO k=1,mz
        DO j=1,my
        DO i=1,mx
          NSTmzz(i,j,k) = NST__u(i,j,k)
        ENDDO
        ENDDO
        ENDDO
          write(11,118)   NSTmzz          !  ua_dyn, ua_dyn(mzz) assumed = 0

        DO k=1,mz
        DO j=1,my
        DO i=1,mx
          NSTmzz(i,j,k) = NST__v(i,j,k)
        ENDDO
        ENDDO
        ENDDO
          write(11,118)   NSTmzz          !  va_dyn, va_dyn(mzz) assumed = 0

        DO k=1,mz
        DO j=1,my
        DO i=1,mx
          NSTmzz(i,j,k) = NST_qv(i,j,k)
        ENDDO
        ENDDO
        ENDDO
           k=  mzz
        DO j=1,my
        DO i=1,mx
          NSTmzz(i,j,k) = NST_qv(i,j,mz)
        ENDDO
        ENDDO
          write(11,118)   NSTmzz          !  qv_H2O, qv_H2O(mzz) assumed = qv_H2O(mz)

        close(unit=11)

        write(6,*) 'Initialization  file MARdynp.DAT created'

      END IF
      END IF



! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

! Output for SISVAT in marp-phyp: MARsvtp.DAT
! ===========================================

! ASCII OUTPUT for phyp 
! ---------------------

      IF (NSTmap)                                                   THEN
      IF (NSTini.and.(.not.SNDing).and.
     .    LoutDA.and.SELECT.eq.1  .and.
     .    NSTmod.ne.'M2D'         .and.
     .    NSTmod.ne.'CPL'              )                            THEN

! ASCII format is more portable than binary format
! ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        write(6,*) ' '
        write(6,*) ' Ecriture MARsvtp.DAT: Temps: ',iyrDYN,mmaDYN,jdaDYN,jhuDYN
        write(6,*) ' '

        open (unit=11,status='unknown'
     .       ,file=NSTdir(1:nbchar)//'MARsvtp.DAT')
        rewind     11

          write(11,114)  iyrDYN
          write(11,114)  mmaDYN
          write(11,114)  jdaDYN
          write(11,114)  jhuDYN
          write(11,114)  jt_000
          write(11,118)  rt_000

          write(11,118)  GEddxx


! Land (1) - Sea (0) Mask AND Topography
! ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          DO j=1,my
          DO i=1,mx
            IF (isolSL(i,j) .GT. 2)                                 THEN
                iwk_xy(i,j)   =  1
              DO k=1,mw
                iwkxyn(i,j,k) =  1
              ENDDO
            ELSE
                iwk_xy(i,j)   =  0
              DO k=1,mw
                iwkxyn(i,j,k) =  0
              ENDDO
            END IF
          ENDDO
          ENDDO

          write(6,1231)     (i    ,i=1,9)
 1231     format(4x,9i10)
          write(6,1230) (mod(i,10),i=1,90)
 1230     format(4x,90i1)
          DO j=my,1,-1
          write(6,1232)j,(iwk_xy(i,j),i=1,mx)
 1232     format(i2,1x,90i1)
          ENDDO

          write(11,114)  iwk_xy
          write(11,118)  NST_sh
          write(11,114)  iwkxyn

! Vegetation Type
! ^^^^^^^^^^^^^^^
          write(11,114)  NSTsvt

! Soil       Type
! ^^^^^^^^^^^^^^^
          DO k=1,mw
          DO j=1,my
          DO i=1,mx
          iwkxyn(i,j,k)= isolTV(i,j)
          ENDDO
          ENDDO
          ENDDO
          write(11,114)  iwkxyn

! Soil Drainage Switch =0 ==> no Water Flux
! ^^^^^^^^^^^^^^^^^^^^ =1 ==> free Drainage
          DO k=1,mw
          DO j=1,my
          DO i=1,mx
          iwkxyn(i,j,k)= NSTiwf(i,j)
          ENDDO
          ENDDO
          ENDDO
          write(11,114)  iwkxyn   
          
! Slope
! ^^^^^
          DO k=1,mw
          DO j=1,my
          DO i=1,mx
                wk_xyn(i,j,k) = 0.
          ENDDO
          ENDDO
          ENDDO

          DO j=2,my-1
          DO i=2,mx-1
                wk1_xy(i,j)   = 0.5 *(NST_sh(i+1,j) -NST_sh(i-1,j)) /dx
                wk2_xy(i,j)   = 0.5 *(NST_sh(i,j+1) -NST_sh(i,j-1)) /dx
          ENDDO
          ENDDO
         
          DO k=1,mw
          DO j=2,my-1
          DO i=2,mx-1
                wk_xyn(i,j,k) = sqrt(wk1_xy(i,j)*wk1_xy(i,j)
     &                              +wk2_xy(i,j)*wk2_xy(i,j))
          ENDDO
          ENDDO
          ENDDO
          write(11,118)  wk_xyn

! Mosaic Fraction
! ^^^^^^^^^^^^^^^
          write(11,118)  SLsrfl

!         write(6,6000)  (       i                   ,i=1,10)
!6000     format(/,'SLsrfl(2) - NSTsic',/,6x,10i12)
          DO j=my,1,-1
!         write(6,6001)j,(SLsrfl(i,j,2) - NSTsic(i,j),i=1,11)
!6001     format(i4,2x,11e11.3)
          DO i=mx,1,-1
            IF (isolSL(i,j).LE.3)                                   THEN
                SLsrfl(i,j,2) = NSTsic(i,j)
                SLsrfl(i,j,1) =-NSTsic(i,j) + 1.
             IF(mw.GT.2)                                            THEN
               DO n=3,mw
                SLsrfl(i,j,n) = 0.0000
               ENDDO
             END IF
            END IF
          ENDDO
          ENDDO
!         write(6,*) ' '

! Roughness Length (Momentum / Scalars)
! ^^^^^^^^^^^^^^^^
          write(11,118)  SL_z0 
          write(11,118)  SL_r0 

! Soil     Albedo
! ^^^^^^^^^^^^^^^
          DO k=1,mw
          DO j=1,my
          DO i=1,mx
          wk_xyn(i,j,k)= alb0SL(i,j)
          ENDDO
          ENDDO
          ENDDO
          write(11,118)  wk_xyn

! LAI (Leaf Area Index)
! ^^^^^^^^^^^^^^^^^^^^^
          write(11,118)  NSTlai

! GLF (Green Leaf Fraction)
! ^^^^^^^^^^^^^^^^^^^^^^^^^
          write(11,118)  NSTglf

! SOIL Temperature 
! ^^^^^^^^^^^^^^^^
          write(11,118)  NST_ts

          write(6,6002)  (       i                   ,i=1,10)
 6002     format(/,'NSTsst(2) - NST_st',/,6x,10i12)
          DO j=my,1,-1
          write(6,6003)j,(NSTsst(i,j)   - NST_st(i,j),i=1,11)
 6003     format(i4,2x,11e11.3)
          ENDDO

! SOIL Humidity
! ^^^^^^^^^^^^^
          DO j=1,my
          DO i=1,mx

          GO TO (101,102,103,104) isolSL(i,j)

 101      CONTINUE
            DO k=1,nvx
            DO l=1,nsl
              NST_sw(i,j,k,l) = 1.000000
            ENDDO
            ENDDO
          GO TO 105

 102      CONTINUE
            DO k=1,nvx
            DO l=1,nsl
              NST_sw(i,j,k,l) = 1.000000
            ENDDO
            ENDDO
          GO TO 105

 103      CONTINUE
            DO k=1,nvx
            DO l=1,nsl
              NST_sw(i,j,k,l) = 0.000000
            ENDDO
            ENDDO
          GO TO 105

 104      CONTINUE
!           DO k=1,nvx
!           DO l=1,nsl
!             NST_sw(i,j,k,l) = 1.000000
!           ENDDO
!           ENDDO
          GO TO 105

 105      CONTINUE

          ENDDO
          ENDDO

          write(11,118)  NST_sw

        close(unit=11)

        write(6,*) 'Initialization  file MARsvtp.DAT created'

      ENDIF
      ENDIF


! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

! Output for boundary forcing : MARlbc.DAT and MARlbcp.DAT
! ========================================================

! OPEN     MARlbc(p).DAT
! ----     -------------

      IF (NSTini.and.(.not.SNDing).and.
     .    LoutDA.and.SELECT.eq.1  .and.
     .    NSTmod.ne.'M2D'         .and.
     .    NSTmod.ne.'CPL'              ) THEN


       IF (NSTmap)                                                  THEN
        open (unit=22,status='unknown'
     .       ,file=NSTdir(1:nbchar)//'MARlbcp.DAT')
        rewind     22

        write(6,*) 'Boundary forcing file MARlbcp.DAT created'

       END IF

      ENDIF


! WRITE on MARlbc(p).DAT
! ----------------------

      IF (NSTfor.and.(.not.SNDing).and.
     .    LoutDA.and.SELECT.eq.1  .and.
     .    NSTmod.ne.'M2D'         .and.
     .    NSTmod.ne.'CPL'              )                            THEN

       IF (NSTmap)                                                  THEN
        write     (22,114) iyrDYN
        write     (22,114) mmaDYN
        write     (22,114) jdaDYN
        write     (22,114) jhuDYN
        write     (22,114) jt_000
        write     (22,118) rt_000
        write     (22,118) vaxgLB
        write     (22,118) vaxdLB
        write     (22,118) vayiLB
        write     (22,118) vaysLB
        write     (22,118) NST_st  !  here for the SBC of the atmosphere

        write(6,*) 'Boundary forcing file MARlbcp.DAT appended'
       END IF

      ENDIF


! CLOSE    MARlbc(p).DAT
! -----    -------------

      IF (NSTend.and.(.not.SNDing).and.
     .    LoutDA.and.SELECT.eq.1  .and.
     .    NSTmod.ne.'M2D'         .and.
     .    NSTmod.ne.'CPL'              )                            THEN
    
       IF (NSTmap)                                                  THEN
         CLOSE(unit=22)      
       ENDIF

      ENDIF



! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

! Output for upper boundary forcing : MARubc.DAT (version MAR > 20/02/04)
! ==============================================

! OPEN     MARubc(p).DAT
! ----     -------------

      IF (NSTini.and.(.not.SNDing).and.
     .    LoutDA.and.SELECT.eq.1  .and.
     .    NSTmod.ne.'M2D'         .and.
     .    NSTmod.ne.'CPL'              )                            THEN


       IF (NSTmap)                                                  THEN
        open (unit=27,status='unknown'
     .       ,file=NSTdir(1:nbchar)//'MARubcp.DAT')
        rewind     27

       write(6,*) 'Boundary forcing file MARubcp.DAT created'
       END IF

      ENDIF


! WRITE on MARubc(p).DAT
! ----------------------

      IF (NSTfor.and.(.not.SNDing).and.
     .    LoutDA.and.SELECT.eq.1  .and.
     .    NSTmod.ne.'M2D'         .and.
     .    NSTmod.ne.'CPL'              ) THEN

       IF (NSTmap)                                                  THEN
        write     (27,114) iyrDYN
        write     (27,114) mmaDYN
        write     (27,114) jdaDYN
        write     (27,114) jhuDYN
        write     (27,114) jt_000
        write     (27,118) rt_000
        write     (27,118) uairUB
        write     (27,118) vairUB
        write     (27,118) pktaUB

        write(6,*) 'Boundary forcing file MARubcp.DAT appended'

       END IF

      ENDIF


! CLOSE    MARubc(p).DAT
! -----    -------------

      IF (NSTend.and.(.not.SNDing).and.
     .    LoutDA.and.SELECT.eq.1  .and.
     .    NSTmod.ne.'M2D'         .and.
     .    NSTmod.ne.'CPL'              )                            THEN
    
       IF (NSTmap)                                                  THEN
         CLOSE(unit=27)      
       ENDIF

      ENDIF



! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

! Output for boundary forcing : MARsic(p).DAT (version MAR > 20/02/04)
! ===========================================

! OPEN     MARsic(p).DAT
! ----     -------------

      IF (NSTini                  .and.
     .    LoutDA.and.SELECT.eq.1  .and.
     .    NSTmod.ne.'M2D'         .and.
     .    NSTmod.ne.'CPL'              ) THEN

       IF (NSTmap)                                                  THEN
        open (unit=26,status='unknown'
     .       ,file=NSTdir(1:nbchar)//'MARsicp.DAT')
        rewind     26

        write(6,*) 'Boundary forcing file MARsicp.DAT created'
       END IF

      ENDIF


! WRITE on MARsic(p).DAT
! ----------------------

      IF (NSTfor.and.(.not.SNDing).and.
     .    LoutDA.and.SELECT.eq.1  .and.
     .    NSTmod.ne.'M2D'         .and.
     .    NSTmod.ne.'CPL'              ) THEN

       IF (NSTmap)                                                  THEN
        write     (26,114) iyrDYN
        write     (26,114) mmaDYN
        write     (26,114) jdaDYN
        write     (26,114) jhuDYN
        write     (26,114) jt_000
        write     (26,118) rt_000
        write     (26,118) NSTsic
        write     (26,118) NSTsst

        write(6,*) 'Boundary forcing file MARsicp.DAT appended'

       END IF

      ENDIF


! CLOSE    MARsic(p).DAT
! -----    -------------

      IF (NSTend.and.(.not.SNDing).and.
     .    LoutDA.and.SELECT.eq.1  .and.
     .    NSTmod.ne.'M2D'         .and.
     .    NSTmod.ne.'CPL'              ) THEN
     
       IF (NSTmap)                                                  THEN
         CLOSE(unit=26)      
       ENDIF
      ENDIF

! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

! Output for boundary forcing : MARglf(p).DAT (marp-phyp > 24/04/17)
! ===========================================

! OPEN     MARglf(p).DAT
! ----     -------------

      IF (NSTini                  .and.
     .    LoutDA.and.SELECT.eq.1  .and.
     .    NSTmod.ne.'M2D'         .and.
     .    NSTmod.ne.'CPL'              ) THEN

       IF (NSTmap)                                                  THEN
        open (unit=28,status='unknown'
     .       ,file=NSTdir(1:nbchar)//'MARglfp.DAT')
        rewind     28

        write(6,*) 'Boundary forcing file MARglfp.DAT created'
       END IF

      ENDIF


! WRITE on MARglf(p).DAT
! ----------------------

      IF (NSTfor.and.(.not.SNDing).and.
     .    LoutDA.and.SELECT.eq.1  .and.
     .    NSTmod.ne.'M2D'         .and.
     .    NSTmod.ne.'CPL'              ) THEN

       IF (NSTmap)                                                  THEN
        write     (28,114) iyrDYN
        write     (28,114) mmaDYN
        write     (28,114) jdaDYN
        write     (28,114) jhuDYN
        write     (28,114) jt_000
        write     (28,118) rt_000
        write     (28,118) NSTlai
        write     (28,118) NSTglf

        write(6,*) 'Boundary forcing file MARglfp.DAT appended'

       END IF

      ENDIF


! CLOSE    MARglf(p).DAT
! -----    -------------

      IF (NSTend.and.(.not.SNDing).and.
     .    LoutDA.and.SELECT.eq.1  .and.
     .    NSTmod.ne.'M2D'         .and.
     .    NSTmod.ne.'CPL'              ) THEN
     
       IF (NSTmap)                                                  THEN
         CLOSE(unit=28)      
       ENDIF
      ENDIF

C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


      RETURN
      END
