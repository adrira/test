      subroutine AWSgeo(x__MAR,y__MAR)

C +----------------------------------------------------------------------------+
C |                                                                            |
C | MAR OUTPUT   Generic Routine                               10-06-2008  MAR |
C |          condition on next closest MAR Grid Point modified 29-04-2010      |
C +----------------------------------------------------------------------------+

      IMPLICIT                 NONE

      INCLUDE                 'NSTdim.inc'
      INCLUDE                 'NSTvar.inc'
      INCLUDE                 'for2cdf.inc'

      REAL                     x__MAR(mx,my),y__MAR(mx,my) ! MAR coordinate 
                                                           ! on  RAMP DEM
      REAL                     earthr
      REAL                     pi           ,degrad
      REAL                     x__AWS       ,y__AWS
      REAL                     ddista       ,dd_min
      REAL                     d__lon       ,d__lat
      REAL                     dlomin       ,dlamin
      REAL                     dlosgn       ,dlasgn
      INTEGER                  i            ,j     ,idx
      INTEGER                  i__min       ,j__min
      INTEGER                  ilomin       ,jlomin
      INTEGER                  ilamin       ,jlamin

      INTEGER                  i0,j0,i1,j1,i2,j2,i3,j3
      REAL                     INT_la_1     ,INT_la_2
      REAL                     AWSgdx_1     ,AWSgdx_2
      REAL                     AWSgdy_1     ,AWSgdy_2

      integer                  n_AWS
      parameter               (n_AWS=279)
      integer                  nnAWS,  n
      integer            AWSio(n_AWS),AWS_i(n_AWS),AWS_j(n_AWS)
      REAL               AWSla(n_AWS),AWSlo(n_AWS),AWS_z(n_AWS)
      REAL               AWS_x(n_AWS),AWS_y(n_AWS)
      character*6        AWS_0(n_AWS)
      character*6        AWS_0_n
      character*8                     AWS_1(n_AWS),AWS_2(n_AWS)
      character*19             filtxt

      common /AWS_nc_INT/AWSio       ,AWS_i       ,AWS_j
     .                  ,nnAWS
      common /AWS_nc_REA/AWSla       ,AWSlo       ,AWS_z
      common /AWS_nc_CH6/AWS_0
      common /AWS_nc_CH8/             AWS_1       ,AWS_2


C +   INITIALIZATION
C +   ==============

      DATA      earthr/6396.990e3/        ! Earth Radius

      pi     =  acos( -1.0d0)
      degrad =  pi / 180.0d0
      idx    =  NST_dx*1.e-3


C +   OUTPUT FILE
C +   ===========

      filtxt = 'RAntMP_1_km_DEM.JNL'
      open (unit=1,status='unknown',file=filtxt)
                                    !     1234567890123456789
      rewind     1

      filtxt = 'MARdom_20km_DEM.JNL'
      write(filtxt(8:9),'(i2)') idx
      IF   (filtxt(8:8).EQ.' ')          filtxt( 8: 8) = '_'
      open (unit=2,status='unknown',file=filtxt)
      rewind     2
                                         filtxt(17:19) = 'txt'
      open (unit=3,status='unknown',file=filtxt)
      rewind     3
           write(3,30)
 30        format('AWS    Station         | Latit. | Longit.|'
     .     ,                     ' x [km] | y [km] | Altit.||'
     .     ,           ' Grid pt.| x [km] | y [km] |'
     .     ,                     ' Latit. | Longit.| Altit.||'
     .     ,             ' D(AWS)|')
           write(3,32)
 32        format('-----------------------+--------+--------+'
     .     ,                     '--------+--------+-------++'
     .     ,           '---------+--------+--------+'
     .     ,                     '--------+--------+-------++'
     .     ,             '-------+')

      write(6,61)
 61   format(/,' AWSgeo: OUTPUT File RAntMP/MARdom*km_DEM.JNL IN')

      DO n=1,n_AWS
      IF (AWSio(n).GE.1)                                            THEN


C +   Search AWS coordinates in MAR Grid
C +   ==================================

C +   RAMP/DEM coordinates of the AWS
C +   -------------------------------

        ddista = earthr *(sin( 71.d0                *degrad) + 1.d0)
     .                  * tan((45.d0+AWSla(n)*0.5d0)*degrad)
        x__AWS = ddista * cos((90.d0-AWSlo(n))      *degrad)
        y__AWS = ddista * sin((90.d0-AWSlo(n))      *degrad)


C +   Indices of the      closest MAR Grid Point
C +   ------------------------------------------

            dd_min =  1.e6
            i__min =  0
            j__min =  0
        DO j=1,my
        DO i=1,mx
            ddista =sqrt((x__AWS-x__MAR(i,j))*(x__AWS-x__MAR(i,j))
     .                  +(y__AWS-y__MAR(i,j))*(y__AWS-y__MAR(i,j)))
        IF (ddista.LT.dd_min)                                       THEN
            dd_min =  ddista
            i__min = i
            j__min = j
        ENDIF
        ENDDO
        ENDDO


C +   Indices of the next closest MAR Grid Point (Longitude)
C +   ------------------------------------------------------

        IF  ((i__min.GT. 1.AND.j__min.GT. 1)
     .  .AND.(i__min.LT.mx.AND.j__min.LT.my))                       THEN
        IF            (AWSlo(n)-NST__x(i__min,j__min) .GT. 0.0)     THEN
              dlosgn =    1.0
        ELSE
              dlosgn =   -1.0
        END IF

              dlomin =    1.e6
        DO j=j__min-1,j__min+1
        DO i=i__min-1,i__min+1
              d__lon = AWSlo(n)-NST__x(i     ,j     )
          IF (d__lon * dlosgn                         .LT. 0.0  .AND.
     .    abs(d__lon)                                 .LT. dlomin)  THEN
              dlomin = abs(d__lon)
              ilomin =  i -i__min
              jlomin =  j -j__min
          ENDIF
        ENDDO
        ENDDO


C +   Indices of the next closest MAR Grid Point  (Latitude)
C +   ------------------------------------------------------

        IF            (AWSla(n)-NST__y(i__min,j__min) .GT. 0.0)     THEN
              dlasgn =    1.0
        ELSE
              dlasgn =   -1.0
        END IF

              dlamin =    1.e6
        DO j=j__min-1,j__min+1
        DO i=i__min-1,i__min+1
              d__lat = AWSla(n)-NST__y(i     ,j     )
          IF (d__lat * dlasgn                         .LT. 0.0  .AND.
     .    abs(d__lat)                                 .LT. dlamin)  THEN
              dlamin = abs(d__lat)
              ilamin =  i -i__min
              jlamin =  j -j__min
          ENDIF
        ENDDO
        ENDDO

C +   Bilinear Interpolation
C +   ----------------------

        i0       = i__min
        j0       = j__min
        i1       = i__min+ilomin
        j1       = j__min+jlomin
        i2       = i__min       +ilamin
        j2       = j__min       +jlamin
        i3       = i__min+ilomin+ilamin
        j3       = j__min+jlomin+jlamin

C +   1st Line of Longitudes: AWSlo between NST__x(i__min       ,j__min       ) &
C +   ~~~~~~~~~~~~~~~~~~~~~~                NST__x(i__min+ilomin,j__min+jlomin)
                            !           ==> NST_la_1 
                            !             & NSTgdx_1
                            !             & NSTgdy_1
        
        INT_la_1 = NST__y(i0,j0) +( AWSlo(n)    -NST__x(i0,j0))
     .                           *(NST__y(i1,j1)-NST__y(i0,j0))
     .                           /(NST__x(i1,j1)-NST__x(i0,j0))

        AWSgdx_1 = NSTgdx(i0   ) +( AWSlo(n)    -NST__x(i0,j0))
     .                           *(NSTgdx(i1   )-NSTgdx(i0   ))
     .                           /(NST__x(i1,j1)-NST__x(i0,j0))

        AWSgdy_1 = NSTgdy(   j0) +( AWSlo(n)    -NST__x(i0,j0))
     .                           *(NSTgdy(   j1)-NSTgdy(   j0))
     .                           /(NST__x(i1,j1)-NST__x(i0,j0))


C +   2nd Line of Longitudes: AWSlo between NST__x(i__min+ilamin,j__min+jlamin) &
C +   ~~~~~~~~~~~~~~~~~~~~~~                NST__x(i__min+ilomin,j__min+jlomin)
                            !                            +ilamin       +jlamin
                            !           ==> NST_la_2
                            !             & NSTgdx_2
                            !             & NSTgdy_2

        INT_la_2 = NST__y(i2,j2) +( AWSlo(n)    -NST__x(i2,j2))
     .                           *(NST__y(i3,j3)-NST__y(i2,j2))
     .                           /(NST__x(i3,j3)-NST__x(i2,j2))

        AWSgdx_2 = NSTgdx(i2   ) +( AWSlo(n)    -NST__x(i2,j2))
     .                           *(NSTgdx(i3   )-NSTgdx(i2   ))
     .                           /(NST__x(i3,j3)-NST__x(i2,j2))

        AWSgdy_2 = NSTgdy(   j2) +( AWSlo(n)    -NST__x(i2,j2))
     .                           *(NSTgdy(   j3)-NSTgdy(   j2))
     .                           /(NST__x(i3,j3)-NST__x(i2,j2))

C +       Line of  Latitudes: AWSla between NST_la_1 & NST_la_2 ==> NSTgdx,NSTgdy
C +       ~~~~~~~~~~~~~~~~~~
        AWS_x(n) = AWSgdx_1      +( AWSla(n)    -INT_la_1)
     .                           *(AWSgdx_2     -AWSgdx_1)
     .                           /(INT_la_2     -INT_la_1)

        AWS_y(n) = AWSgdy_1      +( AWSla(n)    -INT_la_1)
     .                           *(AWSgdy_2     -AWSgdy_1)
     .                           /(INT_la_2     -INT_la_1)

C +   OUTPUT
C +   ------

        IF (AWS_x(n) .GE. NSTgdx(1) .AND. AWS_x(n) .LE. NSTgdx(mx) .AND.
     .      AWS_y(n) .GE. NSTgdy(1) .AND. AWS_y(n) .LE. NSTgdy(my)) THEN
         IF(AWSio(n) .EQ. 2)
     .    write(6,66) AWS_0(n)
     .               ,AWSlo(n),       i0       ,j0
     .                        ,NST__x(i0       ,j0       )
     .                               ,i0,ilomin,j0,jlomin 
     .                        ,NST__x(i1       ,j1       ),AWS_x(n)
     .               ,AWSla(n),       i0       ,j0
     .                        ,NST__y(i0       ,j0       )
     .                               ,i0,ilamin,j0,jlamin 
     .                        ,NST__y(i2       ,j2       ),AWS_y(n)
 66       format(a6,' Lon(AWS) =',f9.4,3x,'Lon(', i3,',', i3,') =',f9.4
     .                                ,3x,'Lon(',2i3,',',2i3,') =',f9.4
     .                                ,3x,'  x ='                ,f10.4
     .         /,6x,' Lat(AWS) =',f9.4,3x,'Lat(', i3,',', i3,') =',f9.4
     .                                ,3x,'Lat(',2i3,',',2i3,') =',f9.4
     .                                ,3x,'  y ='                ,f10.4)

             AWS_0_n= AWS_0(n)
         IF (AWS_0_n(1:3).EQ.'B__')                                 THEN
          write(1,20)     x__AWS *1.e-3  
     .                  , y__AWS *1.e-3  
         ELSE
          write(1,21)     x__AWS *1.e-3  
     .                  , y__AWS *1.e-3  
     .                  , AWS_0(n)
     .                  , x__AWS *1.e-3  
     .                  , y__AWS *1.e-3  
     .                  , NSTgdx(i0   )
     .                  , NSTgdy(   j0)
         END IF

         IF (AWS_0_n(1:3).EQ.'B__')                                 THEN
          write(2,20)     AWS_x(n)
     .                  , AWS_y(n)
 20       format('LABEL ',2(f8.2,','),' 0,0,.07 @P6.'   )
         ELSE
          write(2,21)     AWS_x(n)
     .                  , AWS_y(n)
     .                  , AWS_0(n)
     .                  , AWS_x(n)
     .                  , AWS_y(n)
     .                  , NSTgdx(i0   )
     .                  , NSTgdy(   j0)
 21       format('LABEL ',2(f8.2,','),'-1,0,.07 \\',1x,a6
     .        ,/,'LABEL ',2(f8.2,','),' 0,0,.12 @P7x'
     .        ,/,'LABEL ',2(f8.2,','),' 0,0,.15 @P5.'
     .                                                  )
         END IF

           write(3,31)    AWS_0(n)
     .                   ,AWS_1(n)       ,AWS_2(n)
     .                   ,AWSla(n)       ,AWSlo(n)
     .                   ,AWS_x(n)       ,AWS_y(n)
     .                   ,AWS_z(n)
     .                   ,       i__min,j__min
     .                   ,NSTgdx(i__min       )
     .                   ,NSTgdy(       j__min)
     .                   ,NST__y(i__min,j__min)
     .                   ,NST__x(i__min,j__min)
     .                   ,NST_sh(i__min,j__min),    1.e-3*dd_min
 31        format(a6,1x,2a8, '|',2(f7.2,' |'),2(f7.1,' |'),f6.0,' ||',
     .                  2i4,' |',2(f7.1,' |'),
     .                           2(f7.2,' |'),  f6.0,' ||',f6.1,' |')
        END IF
        END IF
      END IF
      ENDDO

           write(3,32)

      close(unit=1)
      close(unit=2)
      close(unit=3)

      write(6,60)
 60   format(  ' AWSgeo: OUTPUT File RAntMP/MARdom*km_DEM.JNL OUT')

      return
      end

      block data AWS_nc_DATA

C +----------------------------------------------------------------------------+
C |                                                                            |
C | MAR OUTPUT   Generic Routine                               17-09-2007  MAR |
C |   Manned and Automatic Weather Stations (AWS) Geographic Coordinates       |
C |                                                                            |
C +----------------------------------------------------------------------------+


C +--General Variables
C +  =================

      integer                  n_AWS,  n
      parameter               (n_AWS=279)
      integer            AWSio(n_AWS),AWS_i(n_AWS),AWS_j(n_AWS)
      integer            nnAWS
      REAL               AWSla(n_AWS),AWSlo(n_AWS),AWS_z(n_AWS)
      character*6        AWS_0(n_AWS)
      character*8                     AWS_1(n_AWS),AWS_2(n_AWS)

      common /AWS_nc_INT/AWSio       ,AWS_i       ,AWS_j
     .                  ,nnAWS
      common /AWS_nc_REA/AWSla       ,AWSlo       ,AWS_z
      common /AWS_nc_CH6/AWS_0
      common /AWS_nc_CH8/             AWS_1       ,AWS_2


C +--DATA
C +  ====

C +--ANT
C +  ---

      data (AWS_0(n),AWS_1(n),AWS_2(n)
     .     ,AWSla(n),AWSlo(n),AWS_z(n),AWSio(n),n=001,094)
C +...      LABel      AWS LABELS            Latit. Longit. Alti. PR
C +                                                                0 => No IO
C +                                                                1 => OUTone
C +                                                                2 => OUTone
C +                                                                     ASCII
     .  /  'CapSpe' ,'Cape Spe','ncer    ', -77.97, 167.55,   30., 1,  !   1
     .     'HerbA1' ,'Herbie A','lley    ', -78.10, 166.67,   30., 1,  !   2
     .     'Harry_' ,'Harry   ','        ', -83.00,-121.39,  945., 1,  !   3
     .     'CapBir' ,'Cape Bir','d       ', -77.22, 166.44,  100., 1,  !   4
     .     'Butler' ,'Butler I','sland   ', -72.21, 299.84,   91., 1,  !   5
     .     'Byrd__' ,'Byrd    ','        ', -80.01,-119.40, 1530., 1,  !   6
     .     'Dome-F' ,'Dome F  ','        ', -77.31,  39.70, 3810., 1,  !   7
     .     'Manuel' ,'Manuela ','        ', -74.95, 163.69,   78., 1,  !   8
     .     'Marble' ,'Marble P','oint    ', -77.44, 163.75,  120., 1,  !   9
     .     'Whitlo' ,'Whitlock','        ', -76.14, 168.39,  275., 1,  !  10
     .     'Lettau' ,'Lettau  ','        ', -82.52,-174.45,   30., 1,  !  11
     .     'PortMa' ,'Port Mar','tin     ', -66.82, 141.40,   39., 1,  !  12
     .     'PengPt' ,'Penguin ','Point   ', -67.62, 146.18,   30., 1,  !  13
     .     'Gill_1' ,'Gill    ','        ', -79.99,-178.61,   25., 1,  !  14
     .     'Schwer' ,'Schwerdt','feger   ', -79.90, 169.97,   60., 1,  !  15
     .     'D10___' ,'D-10    ','        ', -66.71, 139.83,  243., 1,  !  16
     .     'Elaine' ,'Elaine  ','        ', -83.13, 174.17,   60., 1,  !  17
     .     'Ski_Hi' ,'Ski Hi  ','        ', -74.79, 289.51, 1395., 1,  !  18
     .     'Relay_' ,'Relay St','        ', -74.02,  43.06, 3353., 1,  !  19
     .     'Linda_' ,'Linda   ','        ', -78.46, 168.38,   50., 1,  !  20
     .     'Uranus' ,'Uranus G','lacier  ', -71.43, 291.07,  780., 1,  !  21
     .     'MADISO' ,'MADISON ','        ',  43.08, -89.38,    0., 1,  !  22
     .     'Doug__' ,'Doug    ','        ', -82.32,-113.24, 1430., 1,  !  23
     .     'BonaPt' ,'Bonapart','e Point ', -64.78, 295.93,    8., 1,  !  24
     .     'Nico__' ,'Nico    ','        ', -89.00,  89.67, 2935., 1,  !  25
     .     'Limbrt' ,'Limbert ','        ', -75.42, 300.15,   40., 1,  !  26
     .     'Larsen' ,'Larsen I','ce      ', -66.95, 299.10,   17., 1,  !  27
     .     'Wndlss' ,'Wndlss B','t       ', -77.73, 167.70,   60., 1,  !  28
     .     'Ferrel' ,'Ferrell ','        ', -77.91, 170.82,   45., 1,  !  29
     .     'Kirkwd' ,'Kirkwood','        ', -68.34, 290.99,   30., 1,  !  30
     .     'Dismal' ,'Dismal I','s       ', -68.09, 291.18,   10., 1,  !  31
     .     'Marily' ,'Marilyn ','        ', -79.95, 165.13,   75., 1,  !  32
     .     'MinnaB' ,'Minna Bl','uff     ', -78.55, 166.66,  920., 1,  !  33
     .     'PegasS' ,'Pegasus ','South   ', -77.99, 166.58,   10., 1,  !  34
     .     'SipleD' ,'Siple Do','me      ', -81.66,-148.77,  620., 1,  !  35
     .     'Sutton' ,'Sutton  ','        ', -67.10, 141.40,  871., 1,  !  36
     .     'RacerR' ,'Racer Ro','ck      ', -64.07, 298.39,   17., 1,  !  37
     .     'YoungI' ,'Young Is','        ', -66.20, 162.30,   30., 1,  !  38
     .     'MtSipl' ,'Mount Si','ple     ', -73.20,-127.05,  230., 1,  !  39
     .     'PossIs' ,'Poss Is ','        ', -71.89, 171.21,   30., 1,  !  40
     .     'Henry_' ,'Henry   ','        ', -89.01,  -1.02, 2755., 1,  !  41
     .     'D47___' ,'D-47    ','        ', -67.40, 138.73, 1560., 2,  !  42
     .     'D57___' ,'D-57    ','        ', -68.30, 137.87, 2105., 2,  !  43
     .     'CapDen' ,'Cape Den','ison    ', -67.01, 142.66,   31., 1,  !  44
     .     'DomeC2' ,'Dome C2 ','        ', -75.12, 123.37, 3250., 1,  !  45
     .     'SwiBnk' ,'Swithinb','ank     ', -81.20,-126.17,  945., 1,  !  46
     .     'PegasN' ,'Pegasus ','North   ', -77.95, 166.50,    8., 1,  !  47
     .     'Theres' ,'Theresa ','        ', -84.60,-115.81, 1463., 1,  !  48
     .     'Mizuho' ,'Mizuho  ','        ', -70.70,  44.29, 2260., 1,  !  49
     .     'LaurII' ,'Laurie I','I       ', -77.55, 170.81,   30., 1,  !  50
     .     'Elizab' ,'Elizabet','h       ', -82.61,-137.08,  519., 1,  !  51
     .     'Briana' ,'Brianna ','        ', -83.89,-134.15,  526., 1,  !  52
     .     'Erin__' ,'Erin    ','        ', -84.90, 231.17,  990., 1,  !  53
     .     'WillF1' ,'Willie F','ield    ', -77.87, 167.02,   20., 1,  !  54
     .     'Young2' ,'Young Is','land    ', -62.23, 162.28,   30., 1,  !  55
     .     'CleanA' ,'Clean Ai','r       ', -90.00,   0.00, 2835., 1,  !  56
     .     'OdellG' ,'Odell Gl','acier   ', -76.63, 160.05, 1637., 1,  !  57
     .     'HerbA2' ,'Herbie A','lley    ', -77.97, 167.54,   24., 1,  !  58
     .     'SkyBlu' ,'Sky Blu ','        ', -74.79, 288.51, 1395., 1,  !  59
     .     'A028-A' ,'A028-A  ','        ', -67.59, 112.22, 1622., 1,  !  60
     .     'A028__' ,'A028    ','        ', -67.59, 112.22, 1622., 1,  !  61
     .     'GC41__' ,'GC41    ','        ', -70.40, 111.26, 2791., 1,  !  62
     .     'GC46__' ,'GC46    ','        ', -70.86, 109.84, 3096., 1,  !  63
     .     'GF08__' ,'GF08    ','        ', -67.51, 102.18, 2123., 1,  !  64
     .     'LawDom' ,'Law Dome','        ', -65.27, 112.74, 1376., 1,  !  65
     .     'DDU___' ,'DDU     ','        ', -66.67, 140.02,   42., 2,  !  66
     .     'Mawson' ,'Mawson  ','        ', -67.60,  62.87,   10., 2,  !  67
     .     'Casey_' ,'Casey   ','        ', -66.28, 110.52,   40., 2,  !  68
     .     'McMurd' ,'McMurdo ','(Fogle) ', -77.82, 166.75,  202., 2,  !  69
     .     'Mirny_' ,'Mirny   ','        ', -66.33,  93.01,   30., 2,  !  70
     .     'Vostok' ,'Vostok  ','        ', -78.45, 106.84, 3471., 2,  !  71
     .     'Alison' ,'Allison ','        ', -89.88, 300.00, 2835., 0,  !  72
     .     'Bowers' ,'Bowers  ','        ', -85.20, 163.40, 2090., 0,  !  73
     .     'D80___' ,'D80     ','        ', -70.02, 134.72, 2500., 0,  !  74
     .     'Dollem' ,'Dolleman',' Island ', -70.58, 299.08,  396., 0,  !  75
c #C1.     'DomeC1' ,'Dome  C ','        ', -74.65, 124.40, 3232., 0,  !  76
     .     'Dome-A' ,'Dome  A ','China   ', -81.00,  77.00, 4100., 0,  !  76
     .     'DomeCA' ,'Dome  C ','AMRC    ', -74.50, 123.00, 3280., 0,  !  77
     .     'DomeCE' ,'Dome  C ','EPICA   ', -75.11, 123.32, 3232., 2,  !  78
     .     'Eneid_' ,'Eneid   ','(TNB)   ', -74.41, 164.06,   88., 0,  !  79
     .     'Gill_2' ,'Gill    ','        ', -80.00, 181.00,   55., 0,  !  80
     .     'Maning' ,'Manning ','        ', -78.80, 166.80,   65., 0,  !  81
     .     'Martha' ,'Martha  ','        ', -78.31,-172.50,   42., 0,  !  82
     .     'Patrik' ,'Patrick ','        ', -89.88,  45.00, 2835., 0,  !  83
     .     'RidgeB' ,'Ridge B ','        ', -77.08,  94.92, 3400., 1,  !  84
     .     'Tiffan' ,'Tiffany ','        ', -77.95, 168.17,   25., 0,  !  85
     .     'Whitlo' ,'Whitlok ','        ', -76.24, 168.66,  274., 0,  !  86
     .     'WindlB' ,'Windless',' Bight  ', -77.70, 167.70,   40., 0,  !  87
     .     'GPS2__' ,'GPS2    ','        ', -74.61, 157.38, 1804., 0,  !  88
     .     '31Dpt_' ,'31Dpt   ','        ', -74.06, 155.93, 2041., 0,  !  89
     .     'M2____' ,'M2      ','        ', -74.80, 151.10, 2272., 0,  !  90
     .     'MidPt2' ,'MdPt2   ','        ', -75.53, 145.92, 2454., 0,  !  91
     .     'D2____' ,'D2      ','        ', -75.65, 140.48, 2482., 0,  !  92
     .     'D4____' ,'D4      ','        ', -75.60, 135.83, 2793., 0,  !  93
     .     'D6____' ,'D6      ','        ', -75.44, 129.63, 3038., 0/  !  94


      data (AWS_0(n),AWS_1(n),AWS_2(n)
     .     ,AWSla(n),AWSlo(n),AWS_z(n),AWSio(n),n= 95,100)
     .  /  'Rother' ,'Rothera ','        ', -67.50, 291.90,   16., 1,  !  95
     .     'Primav' ,'Primaver','a       ', -64.20, 259.00,   50., 1,  !  96
     .     'O_Higg' ,'O Higgin','s       ', -63.30, 302.10,   10., 1,  !  97
     .     'Bellin' ,'Bellings','hausen  ', -62.20, 301.10,   16., 1,  !  98
     .     'Adelai' ,'Adelaide','        ', -67.80, 302.10,   26., 1,  !  99
     .     'SanMar' ,'San Mart','in      ', -68.10, 292.90,    4., 1/  ! 100


C +--AFW
C +  ---

      data (AWS_0(n),AWS_1(n),AWS_2(n)
     .     ,AWSla(n),AWSlo(n),AWS_z(n),AWSio(n),n=101,175)
C +...      LABel      AWS LABELS            Latit. Longit. Alti. PR
C +                                                                0 => No IO
C +                                                                1 => OUTone
C +                                                                2 => OUTone
C +                                                                     ASCII
     .  /  'Bamako' ,' Bamako ','    Mali',  12.32,  -7.57,  381., 2,  ! 101
     .     'Tombou' ,' Tombouc','tou Mali',  16.43,  -3.00,  264., 2,  ! 102
     .     'NiameA' ,' Niamey-','Aero  NI',  13.48,   2.16,  223., 2,  ! 103
     .     'Abidjn' ,' Abidjan',' Cote Iv',   5.15,  -3.56,    8., 2,  ! 104
     .     'Tamanr' ,' Tamanra','sset  AL',  22.78,   5.51, 1377., 2,  ! 105
     .     'Dakar_' ,' Dakar  ',' Senegal',  14.44, -17.30,   27., 2,  ! 106
     .     'Adiake' ,' Adiake ',' Cote Iv',    5.3,   -3.3,    7., 0,  ! 107
     .     'Bondok' ,' Bondouk',' Cote Iv',  08.03,  -2.47,  370., 0,  ! 108
     .     'Bouake' ,' Bouake ',' Cote Iv',  07.44,  -5.04,  376., 0,  ! 109
     .     'MAN___' ,' Man    ',' Cote Iv',  07.23,  -7.31,  340., 0,  ! 110
     .     'Korhog' ,' Korhogo',' Cote Iv',  09.25,  -5.37,  381., 0,  ! 111
     .     'Sassan' ,' Sassand',' Cote Iv',  04.57,  -6.05,   66., 0,  ! 112
     .     'Tabou_' ,' Tabou  ',' Cote Iv',  04.25,  -7.22,   21., 0,  ! 113
     .     'Dimbok' ,' Dimbokr',' Cote Iv',  06.39,  -4.42,   92., 0,  ! 114
     .     'Odienn' ,' Odienn�',' Cote Iv',  09.30,  -7.34,  421., 0,  ! 115
     .     'Bobodi' ,' Bobodio',' Burkfas',  11.10,  -4.19,  460., 0,  ! 116
     .     'Ouaga_' ,' Ouaga  ',' Burkfas',  12.21,  -1.31,  306., 0,  ! 117
     .     'Ouahig' ,' Ouahigo',' Burkfas',  13.34,  -2.25,  336., 0,  ! 118
     .     'Fadago' ,' Fadagou',' Burkfas',  12.02,   0.22,  309., 0,  ! 119
     .     'Conakr' ,' Conakry',' Guinee ',  09.34, -13.37,   26., 2,  ! 120
     .     'Labe__' ,' Labe   ',' Guinee ',  11.19, -12.18, 1026., 0,  ! 121
     .     'Nzere_' ,' Nzere  ',' Guinee ',   7.44,  -8.50,  470., 0,  ! 122
     .     'Siguir' ,' Siguiri',' Guinee ',  11.26,  -9.10,  366., 0,  ! 123
     .     'Bissau' ,' Bissau ',' Gbissau',  11.53, -15.39,   26., 2,  ! 124
     .     'StLoui' ,' Stlouis',' Senegal',  16.03, -16.26,   04., 0,  ! 125
     .     'Matam_' ,' Matam  ',' Senegal',  15.39, -13.15,   17., 0,  ! 126
     .     'Tambac' ,' Tambaco',' Senegal',  13.46, -13.41,   50., 0,  ! 127
     .     'Kolda_' ,' Kolda  ',' Senegal',  12.53, -14.58,   10., 0,  ! 128
     .     'Ziguin' ,' Ziguinc',' Senegal',  12.33, -16.16,   23., 0,  ! 129
     .     'Diourb' ,' Diourbe',' Senegal',  14.39, -16.14,    9., 0,  ! 130
     .     'Kedouk' ,' Kedouko',' Senegal',  12.34, -12.13,  167., 0,  ! 131
     .     'Lungi_' ,' Lungi  ',' Sierral',   8.37, -13.12,   27., 0,  ! 132
     .     'Robert' ,' Robertf',' Liberia',   6.15, -10.21,   18., 0,  ! 133
     .     'Nouakc' ,' Nouakch',' Maurita',  18.06, -15.57,    3., 0,  ! 134
     .     'Nouadi' ,' Nouadib',' Maurita',  20.56, -17.02,    3., 0,  ! 135
     .     'Zouera' ,' Zouerat',' Maurita',  22.45, -12.29,  343., 0,  ! 136
     .     'Nema__' ,' Nema   ',' Maurita',  16.36,  -7.16,  269., 0,  ! 137
     .     'Atar__' ,' Atar   ',' Maurita',  20.31, -13.04,  224., 0,  ! 138
     .     'Kayes_' ,' Kayes  ',' Mali   ',  14.26, -11.26,   47., 0,  ! 139
     .     'Kidal_' ,' Kidal  ',' Mali   ',  18.26,   1.21,  459., 0,  ! 140
     .     'Mopti_' ,' Mopti  ',' Mali   ',  14.31,  -4.06,  272., 0,  ! 141
     .     'Sikaso' ,' Sikasso',' Mali   ',  11.21,  -5.41,  375., 0,  ! 142
     .     'Accra_' ,' Accra  ',' Ghana  ',   5.36,  -0.10,   69., 0,  ! 143
     .     'Kumasi' ,' Kumassi',' Ghana  ',   6.43,  -1.36,  293., 2,  ! 144
     .     'Tamal_' ,' Tamal  ',' Ghana  ',   9.30,  -0.51,  173., 0,  ! 145
     .     'WAG___' ,' WA     ',' Ghana  ',  10.03,  -0.30,  323., 0,  ! 146
     .     'Wenchi' ,' Wenchi ',' Ghana  ',   7.43,  -2.06,  340., 0,  ! 147
     .     'Ada___' ,' Ada    ',' Ghana  ',   5.47,  -0.38,    7., 0,  ! 148
     .     'Abuja_' ,' Abuja  ',' Nigeria',   9.15,   7.00,  344., 0,  ! 149
     .     'Lagos_' ,' Lagos  ',' Nigeria',   6.35,   3.20,   38., 2,  ! 150
     .     'Maidug' ,' Maidugu',' Nigeria',  11.51,  13.05,  354., 0,  ! 151
     .     'Cotonu' ,' Cotonou',' Benin  ',   2.21,   2.23,    9., 2,  ! 152
     .     'Paraku' ,' Parakou',' Benin  ',   9.21,   2.37,  393., 2,  ! 153
     .     'Kandi_' ,' Kandi  ',' Benin  ',  11.08,   2.56,  292., 0,  ! 154
     .     'Natiti' ,' Natitin',' Benin  ',  10.19,   1.23,  461., 0,  ! 155
     .     'Lome__' ,' Lome   ','  Togo  ',   6.10,   1.15,   25., 0,  ! 156
     .     'Atakpa' ,' Atakpam','  Togo  ',   7.35,   1.07,  402., 0,  ! 157
     .     'Sokode' ,' Sokode ','  Togo  ',   8.59,   1.09,  387., 0,  ! 158
     .     'Dapaon' ,' Dapaon ','  Togo  ',  10.52,   0.15,  330., 0,  ! 159
     .     'Faya__' ,' Faya   ','  Tchad ',  18.00,  19.10,  234., 0,  ! 160
     .     'Moundu' ,' Moundou','  Tchad ',   8.37,  16.10,  422., 0,  ! 161
     .     'Ndjame' ,' Ndjamen','a Tchad ',  12.08,  15.02,  295., 2,  ! 162
     .     'Sarh__' ,' Sarh   ','  Tchad ',   9.09,  18.23,  365., 0,  ! 163
     .     'Douala' ,' Douala ',' Camerou',   4.00,   9.44,    9., 2,  ! 164
     .     'Garoua' ,' Garoua ',' Camerou',   9.20,  13.23,  244., 0,  ! 165
     .     'Malabo' ,' Malabo ',' Camerou',   3.49,   8.46,   56., 0,  ! 166
     .     'Tindou' ,' Tindouf',' Algerie',  27.40,  -8.08,  431., 0,  ! 167
     .     'Agadir' ,' Agadir ',' Maroc  ',  30.20,  -9.24,   74., 0,  ! 168
     .     'Villac' ,'Villacis',' Sah Occ',  23.42, -15.52,   10., 0,  ! 169
     .     'Daloa_' ,'Daloa   ',' Cote Iv',   6.52,  -6.28,  276., 2,  ! 170 
     .     'Gagnoa' ,'Gagnoa  ',' Cote Iv',   6.08,  -5.57,  205., 2,  ! 171
     .     'SanPed' ,'San Pedr',' Cote Iv',   4.45,  -6.39,   31., 2,  ! 172
     .     'Tabou_' ,'Tabou   ',' Cote Iv',   4.25,  -7.22,   20., 2,  ! 173
     .     'Yamous' ,'Yamousso',' Cote Iv',   6.54,  -5.21,  196., 2,  ! 174
     .     'Sal___' ,'Cap Vert',' Cote Iv',  16.44, -22.57,   54., 2/  ! 175


C +--ANT (again)
C +  ---

      data (AWS_0(n),AWS_1(n),AWS_2(n)
     .     ,AWSla(n),AWSlo(n),AWS_z(n),AWSio(n),n=176,188)
C +...      LABel      AWS LABELS            Latit. Longit. Alti. PR
C +                                                                0 => No IO
C +                                                                1 => OUTone
C +                                                                2 => OUTone
C +                                                                     ASCII
     .  /  'AGO-A8' ,'AGO-A8  ','        ', -84.36, -23.86, 2103., 1,  ! 176
     .     'Davis_' ,'Davis   ','        ', -68.35,  77.59,   18., 2,  ! 177
     .     'Kenton' ,'Kenton  ','        ', -72.28, -38.82, 3185., 1,  ! 178
     .     'SantCI' ,'Santa Cl','aus Isla', -64.96, -65.67,   25., 1,  ! 179
     .     'ScottI' ,'Scott Is','land    ', -67.37,-179.97,   30., 1,  ! 180
     .     'ScottB' ,'Scott Ba','se      ', -77.51, 166.45,   94., 2,  ! 181
     .     'Ski-Hi' ,'Ski-Hi  ','        ', -74.98, -70.77, 1395., 1,  ! 182
     .     'TNB___' ,'Terra No','va Bay  ', -74.42, 164.06,   80., 1,  ! 183
     .     'Theres' ,'Theresa ','        ', -84.60,-115.81, 1463., 1,  ! 184
     .     'WhiteO' ,'White Ou','t       ', -77.87, 168.16,   30., 1,  ! 185
     .     'WhiteI' ,'White Is','land    ', -78.09, 168.01,   30., 1,  ! 186
     .     'Kohnen' ,'Kohnen  ','        ', -75.00,   0.07, 2892., 1,  ! 187
     .     'ElizBe' ,'Princess',' Elizab ', -71.90,  23.33, 1390., 2/  ! 188


C +--ANT (Balises Glacioclim Samba, Antarctique)
C +  ---

      data (AWS_0(n),AWS_1(n),AWS_2(n)
     .     ,AWSla(n),AWSlo(n),AWS_z(n),AWSio(n),n=189,n_AWS)
C +...      LABel      AWS LABELS            Latit. Longit. Alti. PR
C +                                                                0 => No IO
C +                                                                1 => OUTone
C +                                                                2 => OUTone
C +                                                                     ASCII
     . /'B____1' ,'Balise S','AMBA   1',-66.69592,139.8985,    0., 1,  ! 189
     .  'B____2' ,'Balise S','AMBA   2',-66.69861,139.8915,    0., 1,  ! 190
     .  'B____3' ,'Balise S','AMBA   3',-66.69849,139.8801,    0., 1,  ! 191
     .  'B____4' ,'Balise S','AMBA   4',-66.69848,139.8690,    0., 1,  ! 192
     .  'B____5' ,'Balise S','AMBA   5',-66.69933,139.8545,    0., 1,  ! 193
     .  'B____6' ,'Balise S','AMBA   6',-66.70067,139.8398,    0., 1,  ! 194
     .  'B____7' ,'Balise S','AMBA   7',-66.70199,139.8236,    0., 1,  ! 195
     .  'B____8' ,'Balise S','AMBA   8',-66.70349,139.8088,    0., 1,  ! 196
     .  'B____9' ,'Balise S','AMBA   9',-66.70551,139.8021,    0., 1,  ! 197
     .  'B___10' ,'Balise S','AMBA  10',-66.70689,139.7838,    0., 1,  ! 198
     .  'B___11' ,'Balise S','AMBA  11',-66.70894,139.7746,    0., 1,  ! 199
     .  'B___12' ,'Balise S','AMBA  12',-66.71090,139.7628,    0., 1,  ! 200
     .  'B___13' ,'Balise S','AMBA  13',-66.71597,139.7400,    0., 1,  ! 201
     .  'B___14' ,'Balise S','AMBA  14',-66.72215,139.7237,    0., 1,  ! 202
     .  'B___15' ,'Balise S','AMBA  15',-66.72684,139.6999,    0., 1,  ! 203
     .  'B___16' ,'Balise S','AMBA  16',-66.73164,139.6770,    0., 1,  ! 204
     .  'B___17' ,'Balise S','AMBA  17',-66.73676,139.6557,    0., 1,  ! 205
     .  'B___18' ,'Balise S','AMBA  18',-66.74189,139.6336,    0., 1,  ! 206
     .  'B___19' ,'Balise S','AMBA  19',-66.74656,139.6087,    0., 1,  ! 207
     .  'B___20' ,'Balise S','AMBA  20',-66.75196,139.5914,    0., 1,  ! 208
     .  'B___21' ,'Balise S','AMBA  21',-66.76015,139.5783,    0., 1,  ! 209
     .  'B___22' ,'Balise S','AMBA  22',-66.76746,139.5677,    0., 1,  ! 210
     .  'B___23' ,'Balise S','AMBA  23',-66.77496,139.5568,    0., 1,  ! 211
     .  'B___24' ,'Balise S','AMBA  24',-66.78358,139.5459,    0., 1,  ! 212
     .  'B___25' ,'Balise S','AMBA  25',-66.79026,139.5321,    0., 1,  ! 213
     .  'B___26' ,'Balise S','AMBA  26',-66.79795,139.5204,    0., 1,  ! 214
     .  'B___27' ,'Balise S','AMBA  27',-66.80594,139.5093,    0., 1,  ! 215
     .  'B___28' ,'Balise S','AMBA  28',-66.81308,139.4968,    0., 1,  ! 216
     .  'B___29' ,'Balise S','AMBA  29',-66.82068,139.4857,    0., 1,  ! 217
     .  'B___30' ,'Balise S','AMBA  30',-66.82845,139.4750,    0., 1,  ! 218
     .  'B___31' ,'Balise S','AMBA  31',-66.83596,139.4623,    0., 1,  ! 219
     .  'B___32' ,'Balise S','AMBA  32',-66.84335,139.4495,    0., 1,  ! 220
     .  'B___33' ,'Balise S','AMBA  33',-66.85136,139.4383,    0., 1,  ! 221
     .  'B___34' ,'Balise S','AMBA  34',-66.85864,139.4251,    0., 1,  ! 222
     .  'B___35' ,'Balise S','AMBA  35',-66.86616,139.4135,    0., 1,  ! 223
     .  'B___36' ,'Balise S','AMBA  36',-66.87332,139.4004,    0., 1,  ! 224
     .  'B___37' ,'Balise S','AMBA  37',-66.88036,139.3867,    0., 1,  ! 225
     .  'B___38' ,'Balise S','AMBA  38',-66.88902,139.3759,    0., 1,  ! 226
     .  'B___39' ,'Balise S','AMBA  39',-66.89655,139.3638,    0., 1,  ! 227
     .  'B___40' ,'Balise S','AMBA  40',-66.90511,139.3541,    0., 1,  ! 228
     .  'B___41' ,'Balise S','AMBA  41',-66.91162,139.3414,    0., 1,  ! 229
     .  'B___42' ,'Balise S','AMBA  42',-66.92023,139.3318,    0., 1,  ! 230
     .  'B___43' ,'Balise S','AMBA  43',-66.92771,139.3174,    0., 1,  ! 231
     .  'B___44' ,'Balise S','AMBA  44',-66.93586,139.3041,    0., 1,  ! 232
     .  'B___45' ,'Balise S','AMBA  45',-66.94995,139.2799,    0., 1,  ! 233
     .  'B___46' ,'Balise S','AMBA  46',-66.96581,139.2567,    0., 1,  ! 234
     .  'B___47' ,'Balise S','AMBA  47',-66.98064,139.2319,    0., 1,  ! 235
     .  'B___48' ,'Balise S','AMBA  48',-66.99638,139.2094,    0., 1,  ! 236
     .  'B___49' ,'Balise S','AMBA  49',-67.01013,139.1807,    0., 1,  ! 237
     .  'B___50' ,'Balise S','AMBA  50',-67.02596,139.1579,    0., 1,  ! 238
     .  'B___51' ,'Balise S','AMBA  51',-67.04180,139.1366,    0., 1,  ! 239
     .  'B___52' ,'Balise S','AMBA  52',-67.05727,139.1129,    0., 1,  ! 240
     .  'B___53' ,'Balise S','AMBA  53',-67.07246,139.0880,    0., 1,  ! 241
     .  'B___54' ,'Balise S','AMBA  54',-67.08774,139.0636,    0., 1,  ! 242
     .  'B___55' ,'Balise S','AMBA  55',-67.10892,139.0396,    0., 1,  ! 243
     .  'B___56' ,'Balise S','AMBA  56',-67.13006,139.0164,    0., 1,  ! 244
     .  'B___57' ,'Balise S','AMBA  57',-67.15158,138.9922,    0., 1,  ! 245
     .  'B___58' ,'Balise S','AMBA  58',-67.17299,138.9688,    0., 1,  ! 246
     .  'B___59' ,'Balise S','AMBA  59',-67.19441,138.9449,    0., 1,  ! 247
     .  'B___60' ,'Balise S','AMBA  60',-67.21529,138.9217,    0., 1,  ! 248
     .  'B___61' ,'Balise S','AMBA  61',-67.23701,138.8970,    0., 1,  ! 249
     .  'B___62' ,'Balise S','AMBA  62',-67.25777,138.8741,    0., 1,  ! 250
     .  'B___63' ,'Balise S','AMBA  63',-67.27934,138.8516,    0., 1,  ! 251
     .  'B___64' ,'Balise S','AMBA  64',-67.29985,138.8272,    0., 1,  ! 252
     .  'B___65' ,'Balise S','AMBA  65',-67.32085,138.8031,    0., 1,  ! 253
     .  'B___66' ,'Balise S','AMBA  66',-67.34199,138.7810,    0., 1,  ! 254
     .  'B___67' ,'Balise S','AMBA  67',-67.36324,138.7575,    0., 1,  ! 255
     .  'B___68' ,'Balise S','AMBA  68',-67.38330,138.7339,    0., 1,  ! 256
     .  'B___69' ,'Balise S','AMBA  69',-67.39895,138.6845,    0., 1,  ! 257
     .  'B___70' ,'Balise S','AMBA  70',-67.41148,138.6308,    0., 1,  ! 258
     .  'B___71' ,'Balise S','AMBA  71',-67.42337,138.5791,    0., 1,  ! 259
     .  'B___72' ,'Balise S','AMBA  72',-67.43371,138.5271,    0., 1,  ! 260
     .  'B___73' ,'Balise S','AMBA  73',-67.44520,138.4770,    0., 1,  ! 261
     .  'B___74' ,'Balise S','AMBA  74',-67.45652,138.4266,    0., 1,  ! 262
     .  'B___75' ,'Balise S','AMBA  75',-67.46742,138.3745,    0., 1,  ! 263
     .  'B___76' ,'Balise S','AMBA  76',-67.48021,138.3261,    0., 1,  ! 264
     .  'B___77' ,'Balise S','AMBA  77',-67.49033,138.2753,    0., 1,  ! 265
     .  'B___78' ,'Balise S','AMBA  78',-67.50198,138.2254,    0., 1,  ! 266
     .  'B___79' ,'Balise S','AMBA  79',-67.51268,138.1755,    0., 1,  ! 267
     .  'B___80' ,'Balise S','AMBA  80',-67.52428,138.1233,    0., 1,  ! 268
     .  'B___81' ,'Balise S','AMBA  81',-67.53552,138.0752,    0., 1,  ! 269
     .  'B___82' ,'Balise S','AMBA  82',-67.54633,138.0226,    0., 1,  ! 270
     .  'B___83' ,'Balise S','AMBA  83',-67.55824,137.9738,    0., 1,  ! 271
     .  'B___84' ,'Balise S','AMBA  84',-67.56938,137.9211,    0., 1,  ! 272
     .  'B___85' ,'Balise S','AMBA  85',-67.57919,137.8671,    0., 1,  ! 273
     .  'B___86' ,'Balise S','AMBA  86',-67.59188,137.8209,    0., 1,  ! 274
     .  'B___87' ,'Balise S','AMBA  87',-67.60346,137.7698,    0., 1,  ! 275
     .  'B___88' ,'Balise S','AMBA  88',-67.61475,137.7186,    0., 1,  ! 276
     .  'B___89' ,'Balise S','AMBA  89',-67.62682,137.6693,    0., 1,  ! 277
     .  'B___90' ,'Balise S','AMBA  90',-67.64194,137.5968,    0., 1,  ! 278
     .  'B___91' ,'Balise S','AMBA  91',-67.66007,137.5244,    0., 1/  ! 279
C +         |          |         |             |       |        |  |
C +         |          |         |             |       |        |  v_
C +         |          |         |             |       |        |  OUTPUT = 0: All    OUTPUT are    prohibited 
C +         |          |         |             |       |        |  OUTPUT = 1: netcdf OUTPUT decided in AWSloc
C +         |          |         |             |       |        |  OUTPUT = 2: netcdf OUTPUT decided in AWSloc
C +         |          |         |             |       |        |              ASCII  OUTPUT decided in AWSloc
C +         |          |         |             |       |        v                           (see #WV in SISVAT)
C +         |          |         |             |       v        ALTITUDE of the Station
C +         |          |         |             v       LONGITUDE         of the Station
C +         |          v         v             LATITUDE                  of the Station
C +         v          ATTRIBUTE of the Station, will be written in a title of the corresponding netcdf file
C +         LABEL of the Station,    will be used as the first 3 characters of the corresponding netcdf file


C +  *******
C +--CAUTION: DO'NT FORGET TO MODIFY THE parameter n_AWS in AWSloc       IF YOU ADD NEW STATIONS!
C +  *******                                                AWS_nc
C +                                                         AWS_nc_DATA 
 
      end
