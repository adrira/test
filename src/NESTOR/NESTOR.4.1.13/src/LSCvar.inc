
!-Input model
! -----------

      CHARACTER*3 LSCmod
      LOGICAL     SPHgrd,REGgrd
    
! ....LSCmod : acronym of the large-scale model
! ....SPHgrd : stereographic grid
! ....REGgrd : regular grid (in latitude and longitude) 


!-Horizontal and vertical grid
! ----------------------------

      REAL LSC1Dx(ni   ),LSC1Dy(   nj),LSC__z(nk),                      &
     &                   LSC1Vy(  njv),                                 &
     &     LSC__x(ni,nj),LSC__y(ni,nj),LSCgdz(nk)

! ....LSC__x : X-coordinates
! ....LSC1Dx : X-coordinates (for regular input grid)
! ....LSC__y : Y-coordinates
! ....LSC1Dy : Y-coordinates (for regular input grid)
! ....LSC1Vy : Y-coordinates (for regular grid, wind)
! ....LSC__z : Z-coordinates (hybrid)
! ....LSCgdz : Vertical grid of the LSC model


!-2-D surface variables
! ---------------------

      REAL LSC_st(ni,nj),LSCdst(ni,nj),LSC_sw(ni,nj),                   &
     &     LSCdsw(ni,nj),LSC_sp(ni,nj),LSC_dt(ni,nj),                   &
     &     LSC_sh(ni,nj),LSC_pr(ni,nj),LSCppr(ni,nj),                   &
     &     LSC_sn(ni,nj),LSCtcc(ni,nj),LSCuts(ni,nj),                   &
     &     LSCsic(ni,nj),LSCsst(ni,nj),LSClsm(ni,nj)

! ....LSC_st : soil or sea surface temperature
! ....LSCdst : deep soil temperature
! ....LSC_sw : soil wetness
! ....LSCdsw : deep soil wetness
! ....LSC_sp : surface pressure
! ....LSC_dt : temperature diff. between 1st lev. and surf.
! ....LSC_sh : surface elevation
! ....LSC_pr : rain precipitation at the current  time step
! ....LSCppr : rain precipitation at the previous time step
! ....LSC_sn : snow precipitation
! ....LSCtcc : total cloud cover
! ....LSCuts : surface heat flux
! ....LSCsic : Sea Ice Fraction
! ....LSCsst : Sea Surface Temperature
! ....LSClsm : Land Sea Mask


!-3-D atmospheric variables (storred on 1 level = 2D)
! -------------------------

      REAL LSC__p(ni,nj),                                               &
     &     LSC__u(ni,nj),LSC__v(ni,njv),LSC__w(ni,nj),                  &
     &     LSC_pt(ni,nj),LSC__t(ni,nj) ,LSC_qv(ni,nj),                  &
     &     LSCtke(ni,nj),LSC_qt(ni,nj) ,LSCtmp(ni,nj),                  &
     &     LSCtm2(ni,nj),LSCgv1(ni,njv),LSCgv2(ni,njv),                 &
     &     LSC_rh(ni,nj)

! ....LSC__p : pressure
! ....LSC__u : U-wind
! ....LSC__v : V-wind
! ....LSC__w : W-wind
! ....LSC_pt : potential temperature
! ....LSC__t : real temperature
! ....LSC_qv : specific humidity
! ....LSCtke : turbulent kinetic energy
! ....LSC_qt : total cloud water
! ....LSCtmp : temporary array
! ....LSCtm2 : temporary array


      COMMON/LSCvar_c/LSCmod
      
      COMMON/LSCvar_l/SPHgrd,REGgrd
      
      COMMON/LSCvar_r/LSC1Dx,LSC1Dy,LSC__z,LSC1Vy,LSC__x,LSC__y,        &
     &                LSCgdz,LSC_st,LSCdst,LSC_sw,LSCdsw,LSC_sp,        &
     &                LSC_dt,LSC_sh,LSC_pr,LSCppr,LSC_sn,LSCtcc,        &
     &                LSCuts,LSC__p,LSC__u,LSC__v,LSC__w,LSC_pt,        &
     &                LSC__t,LSC_qv,LSCtke,LSC_qt,LSCtmp,LSCtm2,        &
     &                LSCgv1,LSCgv2,LSCsic,LSCsst,LSC_rh,LSClsm
   
