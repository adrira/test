      subroutine z_orog(imx,jmy,xi,yj,dx,hhxy,
     .           dx_res,hhav,soltyp,zoro)

C +----------------------------------------------------------------------------+
C |                                                     NESTOR 14 October 2004 |
C |   subroutine z_orog computes the orographic roughness                      |
C |                                                                            |
C +----------------------------------------------------------------------------+

      IMPLICIT NONE

      LOGICAL   FLott
      INTEGER   soltyp
      INTEGER   im   ,jm
      INTEGER   imx  ,jmy
      INTEGER   imxmx,jmymy
      PARAMETER(imxmx=100,jmymy=100)
      INTEGER   ijmx   (imxmx,jmymy)
      REAL      dx                  ,dx_res
      REAL      xi     (imxmx,jmymy)
      REAL      yj     (imxmx,jmymy)
      REAL      hhxy   (imxmx,jmymy),hhav  ,hhrg
      REAL      hhsg   (imxmx,jmymy),hhx1  ,hhx2
      REAL      rSlopX              ,rSlopY
      REAL      zx_zx_h(imxmx,jmymy),zx_zx
      REAL      zy_zy_h(imxmx,jmymy),zy_zy
      REAL      zx_zy_h(imxmx,jmymy),zx_zy
      REAL      max__hh
      REAL      grid_hh
      REAL      land_hh
      REAL      sum__hh
      REAL      sumsqhh
      REAL      zoro   ,zstd  ,zsig  
      REAL      xk     ,xl    ,xm    
      REAL      xp     ,xq    ,xw

      REAL      offset
      REAL      pSlope
      REAL      subghh
      REAL      Z0_log
      REAL      Z0_ANT

      DATA      FLott /.FALSE./                  ! Parameterization Switch
c #   DATA      offset/75.000 /                  ! d(Height) | significant MAX
      DATA      Z0_ANT/ 0.001 /                  ! Z0_ANT (Guess)


      offset =  100.00
      include  'USRant.offset'


C +--F.Lott Model
C +  ============

      IF (FLott)                                                    THEN


C +--Resolved Slopes (Mesh average)
C +  ------------------------------

              rSlopX         =  0.
        DO jm=2,jmy-1
              rSlopX         =  rSlopX
     .                       +  hhxy(imx-1,jm   )-hhxy(2    ,jm   )
        ENDDO
              rSlopX         =  rSlopX
     .                       / (dx *(imx-3) *(jmy-2))

              rSlopY         =  0.
        DO im=2,imx-1
              rSlopY         =  rSlopY
     .                       +  hhxy(im   ,jmy-1)-hhxy(2    ,jm   )
        ENDDO
              rSlopY         =  rSlopY
     .                       / (dx *(jmy-3) *(imx-2))


C +--Resolved Slopes
C +  ---------------

        DO jm=2,jmy-1
        DO im=2,imx-1
c #SL         rSlopX         = (hhxy(imx-1,jm   )-hhxy(2    ,jm   ))
c #SL.                        /(dx *(imx-3)     )
c #SL         rSlopY         = (hhxy(im   ,jmy-1)-hhxy(2    ,jm   ))
c #SL.                      /(dx *(jmy-3)     )


C +--Slope Correlations
C +  ------------------

              zx_zx_h(im,jm) = (hhxy(im +1,jm   )-hhxy(im -1,jm   ))
     .                        /(2.*dx)
     .                       -  rSlopX
              zy_zy_h(im,jm) = (hhxy(im   ,jm +1)-hhxy(im   ,jm -1))
     .                        /(2.*dx)
     .                       -  rSlopY
        ENDDO
        ENDDO

              land_hh        =                  0.
              grid_hh        =                  0.
              sum__hh        =                  0.
              sumsqhh        =                  0.
              zx_zx          =                  0.
              zy_zy          =                  0.
              zx_zy          =                  0.
        DO jm=2,jmy-1
        DO im=2,imx-1
              zx_zy_h(im,jm) = zx_zx_h(im,jm) * zy_zy_h(im,jm)
              zx_zx_h(im,jm) = zx_zx_h(im,jm) * zx_zx_h(im,jm)
              zy_zy_h(im,jm) = zy_zy_h(im,jm) * zy_zy_h(im,jm)
          IF (   hhxy(im,jm).GT.1.  )                               THEN
              land_hh        = land_hh        + 1.
          END IF
              grid_hh        = grid_hh        + 1.
              sum__hh        = sum__hh        + hhxy   (im,jm)
              sumsqhh        = sumsqhh        + hhxy   (im,jm)
     .                                         *hhxy   (im,jm)
              zx_zx          = zx_zx          + zx_zx_h(im,jm)
              zy_zy          = zy_zy          + zy_zy_h(im,jm)
              zx_zy          = zx_zy          + zx_zy_h(im,jm)
        ENDDO
        ENDDO


C +--Mean Orography:
C +  --------------

             sum__hh         = sum__hh        / grid_hh
             sumsqhh         = sumsqhh        / grid_hh
             zx_zx           = zx_zx          / grid_hh
             zy_zy           = zy_zy          / grid_hh
             zx_zy           = zx_zy          / grid_hh


C +--Standard deviation:
C +  ------------------

             zstd            = SQRT(MAX(0.,sumsqhh-sum__hh*sum__hh))


C +--Coefficients K, L et M (FLott Model):
C +  ------------------------------------

             xk=(zx_zx+zy_zy)*0.5
             xl=(zx_zx-zy_zy)*0.5
             xm= zx_zy
             xp= xk-sqrt(xl**2+xm**2)
             xq= xk+sqrt(xl**2+xm**2)
             xw= 1.e-8
             if (xp.le.xw)      xp=0.
             if (xq.le.xw)      xq=xw
             if (abs(xm).le.xw) xm=xw*sign(1.,xm)


C +--Slope:
C +  -----

             zsig=sqrt(xq)


C +--Orographic Roughness
C +  --------------------

        IF (soltyp.GT.2)                                            THEN
             zoro= MAX(1.e-6,zstd*zsig*0.5)
        ELSE
             zoro=     0.
        ENDIF
c #TEST      zoro=sqrt(rSlopX*rSlopX+rSlopY*rSlopY)               ! TEST
c #TEST      zoro= MAX(1.e-6,zstd*zsig*0.5)                       ! TEST


C +--ECMWF Model
C +  ===========

      ELSE


C +--Slopes effects of the Resolved Topography are substracted
C +  ---------------------------------------------------------

        DO jm=1,jmy
        DO im=1,imx
               hhxy(im,jm) = 
     .                max(0.,hhxy(im ,jm  ))
        ENDDO
        ENDDO

        DO jm=2,jmy-1
        DO im=2,imx-1
               hhx1        = hhxy(  1,   1) 
     .                     +(hhxy(  1,jmy )-hhxy(  1,    1))
     .                     * real(    jm-1)/real(    jmy-1)
               hhx2        = hhxy(imx,   1) 
     .                     +(hhxy(imx,jmy )-hhxy(imx,    1))
     .                     * real(    jm-1)/real(    jmy-1)
               hhrg        = hhx1
     .                     +(hhx2          -hhx1           )
     .                     * real(    im-1)/real(    imx-1)
               hhsg(im,jm) = hhxy(im ,jm  )
     .                      -hhrg
     .                      +hhav
        ENDDO
        ENDDO
        DO im=1,imx
               hhsg(im ,  1)=hhav
               hhsg(im ,jmy)=hhav
        ENDDO
        DO jm=1,jmy
               hhsg(  1,jm )=hhav
               hhsg(imx,jm )=hhav
        ENDDO


C +--Subgrid Topography Maxima
C +  -------------------------

               sum__hh =  0.
               sumsqhh =  0.
               grid_hh =  0.
               max__hh = -1.
        DO jm=2,jmy-1
        DO im=2,imx-1
               sum__hh = sum__hh + hhsg(im,jm)
          IF ( hhxy(im,jm).GT.0.                          )         THEN
               sumsqhh = sumsqhh + hhsg(im,jm) *hhsg(im,jm)
          END IF
               grid_hh = grid_hh + 1.
          IF ((hhsg(im,jm).GT.hhsg(im+1,jm  )+offset).AND.
     .        (hhsg(im,jm).GT.hhsg(im-1,jm  )+offset).AND.
     .        (hhsg(im,jm).GT.hhsg(im  ,jm+1)+offset).AND.
     .        (hhsg(im,jm).GT.hhsg(im  ,jm-1)+offset).AND.
     .        (hhsg(im,jm).GT.hhsg(im-1,jm-1)+offset).AND.
     .        (hhsg(im,jm).GT.hhsg(im-1,jm+1)+offset).AND.
     .        (hhsg(im,jm).GT.hhsg(im+1,jm-1)+offset).AND.
     .        (hhsg(im,jm).GT.hhsg(im+1,jm+1)+offset)     )         THEN
               max__hh = max__hh + 1.
               ijmx(im,jm) =  ijmx(im  ,jm)  +1
          END IF
        ENDDO
        ENDDO
               max__hh = 
     .            max(0.,max__hh - 1.)


C +--Mean Orography:
C +  --------------

               sum__hh = sum__hh / max(grid_hh,1.e-6)
               sumsqhh = sumsqhh / max(grid_hh,1.e-6)


C +--Standard deviation:
C +  ------------------

               zstd    = SQRT(MAX(0.,sumsqhh-sum__hh*sum__hh))


C +--Slope Parameter
C +  ---------------

               pSlope  = 4. * zstd * sqrt(max__hh) / dx_res


C +--Roughness Length
C +  ----------------

             subghh  =  4.0*zstd                       ! subgrid mean orography
        IF  (subghh.GT.     offset  .AND.
     .       pSlope.GT. 0.01)                                       THEN
             Z0_log  =  0.4/ log(1.0+subghh /(2.0   *Z0_ANT))
             Z0_log  =  0.4/sqrt(0.2*pSlope + Z0_log*Z0_log )
             zoro    =  0.5*subghh /     (exp(Z0_log) - 1.0 ) 
             zoro    =       min(    subghh , zoro          )
        ELSE
             zoro    =  0.
        END IF

c #WR   IF (max__hh.GT.0.) 
c #WR.  write(6,6001) hhav,subghh,max__hh,dx_res,pSlope,Z0_log,zoro
 6001   format('sh=',f9.3,3x,'Subgrid=',f9.3
     .                   ,3x,'Nb max =',f3.0,3x,'dx [m]=',f9.3,3x
     .                   ,3x,'pSlope =',e9.3,3x,'Z0_log=',E9.3
     .                   ,3x,'z0_oro =',e9.3)
c #TEST        hhav    = max__hh
c #TEST        hhav    = sum__hh - hhav
c #TEST        hhav    = pSlope 

      END IF


      return
      end
