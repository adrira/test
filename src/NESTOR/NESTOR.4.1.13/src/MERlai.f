C   +-------------------------------------------------------------------+
C   |  Subroutine MERlai                              Jun 2017  NESTING |
C   +-------------------------------------------------------------------+

      SUBROUTINE MERlai

      IMPLICIT none


C +---General variables
C +   -----------------

      INCLUDE 'NSTdim.inc'
      INCLUDE 'NSTvar.inc'
      INCLUDE 'LOCfil.inc'
      INCLUDE 'NetCDF.inc'
      INCLUDE 'NESTOR.inc'

      real   ,parameter :: reso_y=0.5
      real   ,parameter :: reso_x=0.625

      integer,parameter :: cx  = 576
      integer,parameter :: cy  = 361
      integer,parameter :: nbr = 25

      integer  in,jn,i,j,k,l,kk,ll,n,dx1,dx2,dy1,dy2
      integer  NET_ID_LAI,NET_ID_GLF,NETcid,Rcode,start(4),count(4)
      integer  ii(mx,my,nbr),jj(mx,my,nbr),nn(mx,my),nbr_day

      real     wrk1,ww,di,vv,distance_MERlai,dd(mx,my,nbr)
      real     MERRA2_lai(cx,cy),MERRA2_glf(cx,cy)

      integer   DATiyr,DATmma,DATjda,DATjhu     

      CALL DATcnv (DATiyr,DATmma,DATjda,DATjhu,DATtim,.false.)

      nbr_day=0

      do i=1,DATmma-1
       if(i==1.or.i==3.or.i==5.or.i==7.or.i==8.or.i==10.or.i==12) 
     .                                        nbr_day=nbr_day+31
       if(i==4.or.i==6.or.i==9.or.i==11)      nbr_day=nbr_day+30
       if(i==2)                               nbr_day=nbr_day+28
      enddo
    
      nbr_day=nbr_day+DATjda

!-----------------------------------------------------------------------

      NETcid     = NCOPN("input/VEGE/MERRA2-lai-glf.nc",NCNOWRIT,Rcode)
      NET_ID_LAI = NCVID(NETcid,'LAI',Rcode)
      NET_ID_GLF = NCVID(NETcid,'GLF',Rcode)

      start(1)=1
      start(2)=1
      start(3)=nbr_day ! time step
      count(1)=cx
      count(2)=cy
      count(3)=1

      Rcode = nf_get_vara_real(NETcid,NET_ID_LAI,start,count,MERRA2_lai) 
      Rcode = nf_get_vara_real(NETcid,NET_ID_GLF,start,count,MERRA2_glf)
      CALL NCCLOS(NETcid, RCODE)


!-----------------------------------------------------------------------

      write(6,*) 'MERRA2 LAI-GLF data set'
      write(6,*) '~~~~~~~~~~~~~~~~~~~~~~~'
      write(6,*) ' '

      nn=0

      DO j=1,my
      WRITE(6,'(i4,$)') j
      DO i=1,mx

C +   *****
      IF(NSTsol(i,j)>=3.and.NST__y(i,j)>-60.)THEN
C +   *****

       in=nint((NST__x(i,j)+180.)/reso_x)
       jn=nint((NST__y(i,j)+ 90.)/reso_y)  

       dx1=1 ; dx2=1 ; dy1=1 ; dy2=1    

       do while (nn(i,j)<=0)
  
      
        do k=in-dx1,in+dx2 ; do l=jn-dy1,jn+dy2

         kk=max(1,min(cx,k))
         ll=max(1,min(cy,l)) 

         if(MERRA2_glf(kk,ll)>=0.and.MERRA2_glf(kk,ll)<=1.
     .  and.MERRA2_lai(kk,ll)>=0.and.MERRA2_lai(kk,ll)<=20) then


          nn(i,j)=min(nbr,nn(i,j)+1)
          ii(i,j,nn(i,j))=kk
          jj(i,j,nn(i,j))=ll
          dd(i,j,nn(i,j))=distance_MERlai(NST__x(i,j),NST__y(i,j),
     .                    real(kk)*reso_x-180.,real(ll)*reso_y-90.)
          dd(i,j,nn(i,j))=dd(i,j,nn(i,j))**2
         endif       

         if(nn(i,j)==0) then
          dx1=dx1+1
          dx2=dx2+1
          dy1=dy1+1
          dy2=dy2+1
         endif 

        enddo ; enddo     


       enddo

!      -----------------------------------------------
 
       vv=0 ; ww=0

       do n=1,nn(i,j)

        ww=ww+1./dd(i,j,n)
        vv=vv+1./dd(i,j,n)
     .    *MERRA2_lai(ii(i,j,n),jj(i,j,n))
       enddo

       DO l=1,nvx
        NSTlai(i,j,l)=min(10.,max(0.,vv/ww))
       ENDDO

!      -----------------------------------------------

       vv=0 ; ww=0

       do n=1,nn(i,j)

        ww=ww+1./dd(i,j,n)
        vv=vv+1./dd(i,j,n)
     .    *MERRA2_glf(ii(i,j,n),jj(i,j,n))

       enddo

       DO l=1,nvx
        NSTglf(i,j,l)=min(1.,max(0.,vv/ww))
       ENDDO

!      -----------------------------------------------

       DO l=1,nvx
        IF (NSTsvt(i,j,l).eq. 0) NSTlmx(i,j,l) = 0.0
        IF (NSTsvt(i,j,l).eq. 1) NSTlmx(i,j,l) = 0.6
        IF (NSTsvt(i,j,l).eq. 2) NSTlmx(i,j,l) = 0.9
        IF (NSTsvt(i,j,l).eq. 3) NSTlmx(i,j,l) = 1.2
        IF (NSTsvt(i,j,l).eq. 4) NSTlmx(i,j,l) = 0.7
        IF (NSTsvt(i,j,l).eq. 5) NSTlmx(i,j,l) = 1.4
        IF (NSTsvt(i,j,l).eq. 6) NSTlmx(i,j,l) = 2.0
        IF (NSTsvt(i,j,l).eq. 7.or.NSTsvt(i,j,l).eq.10)
     .    NSTlmx(i,j,l) = 3.0
        IF (NSTsvt(i,j,l).eq. 8.or.NSTsvt(i,j,l).eq.11)
     .    NSTlmx(i,j,l) = 4.5
        IF (NSTsvt(i,j,l).eq. 9.or.NSTsvt(i,j,l).eq.12)
     .    NSTlmx(i,j,l) = 6.0

       ENDDO

!      -----------------------------------------------

       DO l=1,nvx

        NSTlai(i,j,l)  = NSTlai(i,j,l) * 
     .        max(1.,min(2.,(1.+(NSTlmx(i,j,l)-3.)/12.))) ! 25% corectif factor

        ! MERRA lai = mean lai over 50 x 50 km2 
        ! it is a bit corrected here in fct of vegetation.
 
        NSTlai(i,j,l)  =max(0.,min(1.25*NSTlmx(i,j,l),NSTlai(i,j,l)))
        ! maximum values are a bit too low in respect to literature

        if(NSTsvt(i,j,l)==0.or.NSTsvt(i,j,l)==13) then
         NSTlai(i,j,l) = 0.0
         NSTglf(i,j,l) = 0.0
        endif

       ENDDO

!      -----------------------------------------------

C +   ****
      ELSE   ! Ocean, ice, snow
C +   ****

       DO l=1,nvx
        NSTlai(i,j,l) = 0.0
        NSTglf(i,j,l) = 0.0
       ENDDO

C +   *****
      ENDIF  ! Continental areas
C +   *****

      ENDDO
      ENDDO

      END SUBROUTINE


!--------------------------------------------------------------------------------------------------------------------------

      function distance_MERlai(lon2i,lat2i,lon1i,lat1i)  

      implicit none

      real :: lon1,lat1,lon1i,lat1i
      real :: lon2,lat2,lon2i,lat2i
      real :: dlat,dlon,a,c,distance_MERlai

      real,parameter :: pi   = 3.141592
      real,parameter :: R    = 6371.

      lon1=lon1i*pi/180.
      lat1=lat1i*pi/180.
      lon2=lon2i*pi/180.
      lat2=lat2i*pi/180.


      dlat = (lat2-lat1)
      dlon = (lon2-lon1)
         a =  sin(dLat/2.) * sin(dLat/2.) + cos(lat1) * cos(lat2) 
     .     *  sin(dLon/2.) * sin(dLon/2.)
         c =  2. * atan2(sqrt(a), sqrt(1.-a))

      distance_MERlai =  max(1.,R * c)

      end function
!--------------------------------------------------------------------------------------------------------------------------


