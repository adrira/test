!-SPECIFIC VARIABLES FOR MAR
!-(Modele Atmospherique Regional)
!-===============================

!-Parameters
! ----------

      INTEGER mzz
      PARAMETER (mzz=mz+1)


!-Variables of MARgrd.inc
! -----------------------

      REAL    dx,dy,dt,dtquil,tequil
! ....        dx    : horizontal grid size (x direction)
! ....        dy    : horizontal grid size (y direction)
! ....        dt    : time step
! ....        dtquil: Time Step for 1-D Initialisation (s)
! ....        tequil: Time Span for 1-D Initialisation (s)

      INTEGER imez,jmez
! ....        imez  : reference grid point (x direction)
! ....        jmez  : reference grid point (y direction)

      REAL    sigma(mz),sh(mx,my)
! ....        sigma : independant variable sigma on sigma levels (k)
! ....        sh    : surface height (m)

! #CO COMMON / MARgrd / dx,dy,dt,dtquil,tequil,imez,jmez,sigma,sh


!-Variables of MAR_GE.inc
! -----------------------

      REAL    GElat0,GElon0,GEddxx
! ....        GElat0: Latitude  (Degrees)
! ....        GElon0: Longitude (Degrees)
! ....        GEddxx: Direction x-axis

! #CO COMMON / MAR_GE1 / GElat0,GElon0
      COMMON / MAR_GE2 / GEddxx

!-Variables of MAR_DY.inc
! -----------------------

      INTEGER iyrDYN,mmaDYN,jdaDYN,jhuDYN
! ....        iyrDYN: Year
! ....        mmaDYN: Month
! ....        jdaDYN: Day
! ....        jhuDYN: Hour  (UT)

      REAL    pstDY (mx,my)    ,ptopDY          ,                       &
     &        fcorDY(mx,my)                                             
! ###&       ,uairDY(mx,my,mz) ,vairDY(mx,my,mz),                       &
! ###&       ,pktaDY(mx,my,mzz),tairDY(mx,my,mz),                       &
! ###&       ,qvDY  (mx,my,mz)
! ....        uairDY: x-wind speed component                       (m/s)
! ....        vairDY: y-wind speed component                       (m/s)
! ....        pktaDY: potential temperature divided by 100.[kPa]**(R/Cp)
! ....        tairDY: real      temperature                          (K)
! ....        qvDY  :            Specific Humidity               (kg/kg)
! ....        qsatDY: Saturation Specific Humidity               (kg/kg)
! ....        pstDY1: Model Pressure Depth at INITIAL  Time Step   (kPa)
! ....        pstDY : Model Pressure Depth at current  Time Step   (kPa)
! ....        ptopDY:       Pressure       at Model Top            (kPa)
! ....        fcorDY: Coriolis Parameter                           (s-1)

! #CO COMMON / MAR_DY / iyrDYN,mmaDYN,jdaDYN,jhuDYN,uairDY,vairDY,      &
! #CO&                  pktaDY,tairDY,pkDY  ,qvDY  ,pstDY ,ptopDY,      &
! #CO&                  fcorDY


!-Variables of MAR_SL.inc
! -----------------------

      INTEGER isolSL(mx,my),maskSL(mx,my),nSLsrf(mx,my)
! ....        isolSL : Surface Type : 1 -> open ocean
! ....                                2 -> glacier + ice sheet + snow
! ....                                3 -> sea ice            (+ snow)
! ....                                4 -> soil               (+ snow)
! ....                                5 -> soil + vegetation
! ....        maskSL : Land--Sea Mask 0 -> Continent
! ....                                1 -> Ocean
! ....        nSLsrf : Number of  Sectors in a Grid Box

      REAL    zs_SL,zn_SL,zl_SL,cs2SL,sst_SL,dtagSL,                    &
     &        wk0SL,wx0SL,w20SL,wg0SL
! ....        zs_SL : Typical Sea  Roughness Length (m)
! ....        zn_SL : Typical Snow Roughness Length (m)
! ....        zl_SL : Typical Land Roughness Length (m)
! ....        cs2SL : Soil Temperature Variation Time Scale (s)
! ....                 (usually 86400 s, i.e. diurnal cycle)
! ....        sst_SL: Sea Surface Temperature
! ....        dtagSL: Air-Surface Temperature Difference    (K)
! ....        w**SL : Initial soil humidity variables

      REAL    tairSL(mx,my)   ,                                         &
     &        tsrfSL(mx,my,mw),qvapSL(mx,my)   ,                        &
     &        alb0SL(mx,my)   ,albeSL(mx,my)   ,                        &
     &        albsSL(mx,my)   ,eps0SL(mx,my)   ,                        &
     &        SLsrfl(mx,my,mw),SL_z0(mx,my,mw) ,                        &
     &        SL_r0 (mx,my,mw),SLlmo(mx,my)    ,                        &
     &        SLlmol(mx,my,mw),SLuus(mx,my)    ,                        &
     &        SLuusl(mx,my,mw),SaltSL(mx,my)   ,                        &
     &        virSL (mx,my)   ,fracSL          ,                        &
     &        SLuts (mx,my)   ,SLutsl(mx,my,mw),                        &
     &        SLuqs (mx,my)   ,SLuqsl(mx,my,mw),                        &
     &        ch0SL (mx,my)   ,roseSL(mx,my)   ,                        &
     &        raerSL(mx,my)   ,rsurSL(mx,my)   ,                        &
     &        hmelSL(mx,my)   ,ro_SL0(mx,my)   ,                        &
     &        ro_SL (mx,my)   ,d1_SL (mx,my)   ,                        &
     &        t2_SL (mx,my)   ,w2_SL (mx,my)   ,                        &
     &        wg_SL (mx,my)   ,hsnoSL(mx,my)
! ....        tairSL : Extrapolation of the sounding tempature to the surface.(K)
! ....        tsrfSL : Surface Temperature                                    (K)
! ....        qvapSL : specific humidity close to the surface             (kg/kg)
! ....        alb0SL : Background Surface Albedo
! ....        albeSL :            Surface Albedo
! ....        albsSL : Underlaying   Soil Albedo
! ....        eps0SL : Surface IR Emissivity
! ....        SLsrfl : Normalized Sector  Area                         (m/s)
! ....        SL_z0  : Roughness     Length for Momentum               (m/s)
! ....        SL_r0  : Roughness     Length for Heat                   (m/s)
! ....        SLlmo  : Monin-Obukhov Length                (Average)   (m/s)
! ....        SLlmol : Monin-Obukhov Length                            (m/s)
! ....        SLuus  : Friction Velocity                   (Average)   (m/s)
! ....        SLuusl : Friction Velocity                               (m/s)
! ....        SaltSL : Friction Velocity Saltation   Threshold         (m/s)
! ....        virSL  : Air Loading for SBL Parameterization                           
! ....        fracSL : Fractional Time used in Blowing Snow Surface Flux Computation
! ....        SLuts  : Surface Potential Temperature Turbulent Flux  (Average) (K.m/s)
! ....        SLutsl : Surface Potential Temperature Turbulent Flux            (K.m/s)
! ....        SLuqs  : Surface Specific Humidity     Turbulent Flux  (Av.) (kg/kg.m/s)
! ....        SLuqsl : Surface Specific Humidity     Turbulent Flux        (kg/kg.m/s)
! ....        ch0SL  : Bulk  Aerodynamic Coefficient Air/Surface Humidity Flux
! ....        roseSL : Depletion of water vapor in the surface layer           (kg/kg)
! ....                 due to deposition of dew or rime on ground
! ....        raerSL :`Bulk' Stomatal Resistance (Thom & Oliver, 1977, p. 347)
! ....        rsurSL : Aerodynamic Resistance
! ....        hmelSL : cumulative snowmelt height (m water equivalent)
! ....        hsnoSL : cumulative snow accumulation height (m water equivalent)
! ....        ro_SL  : rhos               (Surface Density)                    (kg/m3)
! ....        ro_SL0 : rhos       (Initial Surface Density)                    (kg/m3)
! ....        d1_SL  : rhos * cs *(Depth diurnal Wave)                        (J/m2/K)
! ....        t2_SL  : Soil Deep Layers Temperature                                (K)
! ....        dtgSL  : Soil Temperature Variation during time interval dt          (K)
! ....                 Is renewed every 6 minutes
! ....        wg_SL and w2_SL Adimensional Numbers measuring Soil Water Content 
! ....        wg_SL  : ... near the     Surface
! ....        w2_SL  : ... over a large Soil Thickness 

! #CO COMMON / MAR_SL / isolSL,maskSL,nSLsrf,zs_SL,zn_SL,zl_SL,         &
! #CO&                  cs2SL,sst_SL,dtagSL,wk0SL,wx0SL,w20SL,          &
! #CO&                  wg0SL,tairSL,tsrfSL,qvapSL,alb0SL,albeSL,       &
! #CO&                  albsSL,eps0SL,SLsrfl,SL_z0,SL_r0,SLlmo,         &
! #CO&                  SLlmol,SLuus,SLuusl,SaltSL,virSL,fracSL,        &
! #CO&                  SLuts,SLutsl,SLuqs,SLuqsl,ch0SL,roseSL,         &
! #CO&                  raerSL,rsurSL,hmelSL,ro_SL0,ro_SL,d1_SL,        &
! #CO&                  t2_SL,w2_SL,wg_SL


!-Variables of MAR_TV.inc
! -----------------------

      INTEGER isolTV(mx,my)
! ....        isolTV : Texture Type :  2 -> loamy sand
! ....                                 5 -> loam
! ....                                11 -> clay


!-Variables of MAR_TU.inc
! -----------------------

      REAL    TUkhff,TUkhmx
! ....        TUkhff: Horiz.vKar**2 (horizontal diffusion)
! ....        TUkhmx: Upper sponge

! #CO COMMON / MAR_TU / TUkhff,TUkhmx


!-Variables of MAR_LB.inc
! -----------------------

      INTEGER iyr_LB,mma_LB,jda_LB,jhu_LB,jdh_LB
! ....        iyr_LB: Year
! ....        mma_LB: Month
! ....        jda_LB: Day
! ....        jhu_LB: Hour  (UT)
! ....        jdh_LB: Time Interval before next GCM/NWP LBC (hour)
! ....        jdh_LB=0 ==> NO further GCM/NWP LBC available

      REAL    vaxgLB(1:n7,my,mz,5),vaxdLB(mx-n6:mx ,my,mz,5),           &
     &        vayiLB(mx,1:n7,mz,5),vaysLB(mx,my-n6:my ,mz,5)
! ....        vaXX : large scale values of relevant dependant variables 
! ....          ^X=(x->x axis border, y->y axis border)
! ....           ^X=(g->x small, d->x large, b->y small, h->y large)

! #CO COMMON / MAR_LB / iyr_LB,mma_LB,jda_LB,jhu_LB,jdh_LB,             &
! #CO&                  vaxgLB,vaxdLB,vayiLB,vaysLB


!-Variables of MAR_PO.inc
! -----------------------

      REAL    uocnPO(mx,my),vocnPO(mx,my),aPOlyn(mx,my)
! ....        uocnPO: Oceanic Current (prescribed, x-direction)
! ....        vocnPO: Oceanic Current (prescribed, y-direction)
! ....        aPOlyn: Initial (observed) lead fraction
! .
! #CO COMMON / MAR_PO / uocnPO,vocnPO,aPOlyn


!-Variables of MAR_IO.inc
! -----------------------

      INTEGER igrdIO(5),jgrdIO(5)
! ....        igrdIO: i (x-direc.) Index Ref. Grid Point (for detailed Output)
! ....        jgrdIO: j (y-direc.) Index Ref. Grid Point (for detailed Output)

      CHARACTER*3 explIO

! #CO COMMON / MAR_IO / igrdIO,jgrdIO,explIO


!-Variables of MAR_FI.inc
! -----------------------

      REAL    FIslot,FIslou,FIslop,FIkhmn
! ....        FIslot: Implicit Filter Parameter (Temperature)
! ....        FIslou: Implicit Filter Parameter (Wind Speed)
! ....        FIslop: Implicit Filter Parameter (Pressure)
! ....        FIkhmn: Horizontal Diffusion Coefficient

! #CO COMMON / MAR_FI / FIslot,FIslou,FIslop,FIkhmn


!-Other variables
! ---------------

      INTEGER   maptyp
      INTEGER*8 itexpe
! ....          maptyp: Projection Type
! ....          itexpe: Iteration number

      REAL    zmin,aavu,bbvu,ccvu
! ....        zmin  : height of the first model level from the surface (m)
! ....        aavu  : grid parameter (geometric progression)
! ....        bbvu  : grid parameter (geometric progression)
! ....        ccvu  : grid parameter (geometric progression)

      LOGICAL vertic

! #CO COMMON / MARoth / itexpe,zmin,aavu,bbvu,ccvu,vertic
      COMMON / MARmap / maptyp
