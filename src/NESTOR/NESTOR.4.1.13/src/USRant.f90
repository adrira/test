!-------------------------------------------------------------------+
!  subroutine USRant          June 2003 (HG) Oct 2014 (CA)  NESTING |
!-------------------------------------------------------------------+
! USRant adapt NESTOR to Antarctic region                           |
! See bellow a new version (AntTOPO) with Bamber (2009)             |
! f77 --> f90 by H.Gallee, 25-Apr-2017                              |
!                                                                   |
! Input : - subnam : Name of the subroutine                         |
! ^^^^^^^            where USRant is called                         |
!                                                                   |
! Maintainer : Hubert Gallée, Cécile Agosta                         |
! ^^^^^^^^^^^^                                                      |
!                                                                   |
!-------------------------------------------------------------------+

      subroutine USRant(subnam)


      implicit none


!-General variables
! -----------------

      include 'NSTdim.inc'
      include 'NSTvar.inc'
      include 'NESTOR.inc'
      include 'LSCvar.inc'

!-local variables
! ---------------

      CHARACTER*6 subnam
  

! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!-Topography for Antarctic    
! ========================

      if (subnam.eq.'NESTOR') call ANTOPO  

      if (subnam.eq.'Bamber' .or. subnam.eq.'bedmap') then
          call AntTOPO(subnam)  !+ CA +!
      endif

      if (subnam.eq.'Racmo2') call AntRACMO !+ CA +!

      END subroutine USRant

! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
      
!-------------------------------------------------------------------+
      subroutine AntRACMO
!-------------------------------------------------------------------+

!-Netcdf specifications
! ---------------------
      include 'NetCDF.inc'
      
!-NST variables
! -------------

      include 'NSTdim.inc'
      include 'NSTvar.inc'
      include 'NESTOR.inc'
      include 'LOCfil.inc'
      
      real NST_dh
      
!.Reading dem file        
      integer fID
      character*80 dem_file
      integer dem_mx,dem_my
      parameter(dem_mx=110,dem_my=91)
      integer dem_xmin,dem_ymin,dem_xmax,dem_ymax
      parameter(dem_xmin=-2600,dem_ymin=-2200)
      parameter(dem_xmax= 2850,dem_ymax= 2300)
      character*80   var_units
      real           dem_sh(dem_mx,dem_mx),dem_msk(dem_mx,dem_mx)
      
      integer i,j,ii,jj

      NST_dh  = NST_dx/1000.
      
      write(6,*) 'Topography : RACMO-27 interpolated on 50km MAR grid'
      write(6,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
    
      dem_file = trim(PFXdir)//'AntTOPO/RACMO-ANT27onMAR-50km.sh.nc'
      write(6,*) "> Read ", dem_file,"  ..."
      write(6,*)
      
      CALL UNropen (dem_file,fID,title)
      CALL UNsread(fID,'sh',1,1,1,1,                                    &
     &                    dem_mx,dem_my,1,                              &
     &                    var_units,dem_sh)
     
      CALL UNsread(fID,'msk',1,1,1,1,                                   &
     &                    dem_mx,dem_my,1,                              &
     &                    var_units,dem_msk)

      do j=1,my
      do i=1,mx
        if ( NSTgdx(i).ge.dem_xmin .and. NSTgdy(j).ge.dem_ymin          &
     &  .and.NSTgdx(i).le.dem_xmax .and. NSTgdy(j).le.dem_ymax) then
          ii = int((NSTgdx(i)-dem_xmin)/NST_dh) +1
          jj = int((NSTgdy(j)-dem_ymin)/NST_dh) +1
!           print*,i,j,ii,jj
          NST_sh(i,j)=dem_sh (ii,jj)
          NSTice(i,j)=dem_msk(ii,jj)*100.
        else
          NST_sh(i,j)=0
          NSTice(i,j)=0.
        endif
        ! +---No atmosphere below sea level
        ! +   -----------------------------
        NST_sh(i,j) = max(NST_sh(i,j),0.)
        if (NSTice(i,j).gt.50.) then
          NSTsol(i,j) = 3
        else
          NSTsol(i,j) = 1
        endif
      enddo
      enddo
      
      end subroutine AntRACMO
!-------------------------------------------------------------------+


!-------------------------------------------------------------------+
!  subroutine ANTOPO                          October 2002  NESTING |
!             ANTARTIC TOPOGRAPHY specific Assimilation             |
!-------------------------------------------------------------------+
!                                                                   |
! Input : NST__x : longitude (degree) of the NST grid               |
! ^^^^^^^ NST__y : latitude  (degree) of the NST grid               |
!                                                                   |
! Output: NST_sh: surface elevation                                 |
! ^^^^^^^ NSTsol: land (4) / sea (1) mask                           |
!                                                                   |
! Method: Divide each nested Grid Cell in elementary Meshes         |
! ^^^^^^^ much small than either the nested or the DTM Mesh         |
!         Compute geographic Coordinates of each elementary Mesh    |
!         Compute Distance of this Mesh to 4 closest DTM Meshes     |
!         Compute Height of this Mesh by kriging                    |
!         Nested Grid Cell Height is the Average                    |
!                                 of the elementary Meshes Heights  |
!                                                                   |
! DATA Source: Radarsat Antarctic Mapping Project Digital           |
! ^^^^^^^^^^^^ Digital  Elevation Model   Version 2                 |
!              ftp site : sidads.colorado.edu                       |
!              directory: /pub/DATASETS/RAMP/DEM_V2/1KM/ASCII       |
!              file     : ramp1kmdem_wgsosu_v2.txt.gz               |
! Reference:   Liu, H., K. Jezek, B. Li, and Z. Zhao. 2001.         |
! ^^^^^^^^^^   Radarsat Antarctic Mapping Project                   |
!              Digital Elevation Model Version 2.                   |
!              Boulder, CO: National Snow and Ice Data Center.      |
!              Digital media.                                       |
!                                                                   |
!-------------------------------------------------------------------+


      subroutine ANTOPO


      implicit none


!-General and local variables
! ---------------------------

      include 'NSTdim.inc'
      include 'NSTvar.inc'
      include 'NESTOR.inc'
      include 'MARvar.inc'
      include 'LOCfil.inc'
      include 'NetCDF.inc'

      INTEGER   nxdata,nydata ,nwdata,nndata
!HG   PARAMETER(nxdata=13670204) 
      INTEGER   ii,i,j

!HGal--v
!HG   REAL      ANT_sh(nxdata)
!HG   REAL      ANTlat(nxdata),ANTlon(nxdata)

      REAL, allocatable ::  ANT_sh(:)
      REAL, allocatable ::  ANTlat(:)     ,ANTlon(:)
!HGal--^

      REAL      LOC_sh(100000),ddxx          ,ddll       ,dd_min
      REAL      LOC__x(100000),LOC__y(100000)

      REAL      pi           ,degrad       ,t_grad       ,t__rad
      REAL      phi          ,angl
      REAL      x_st_i       ,y_st_j       ,x_st_n       ,y_st_n
      REAL                                  x_st_d       ,y_st_d
      REAL      di_lon       ,di_lat       ,dj_lon       ,dj_lat
      REAL      inilon       ,inilat
      REAL      curlon       ,curlat       ,earthr
      REAL      minLON       ,minLAT       ,difLON       ,difLAT
      INTEGER   inx          ,jny
      INTEGER   in           ,jn

      LOGICAL   dd00
      INTEGER   nn           ,iinn(4)      ,jjnn(4)      ,n0
      REAL      x0           ,y0           ,ddxxi        ,ddxxj
      REAL      xx           ,yy           ,xxii         ,yyii
      REAL      dd           ,ddnn(4)                    ,hh


      DATA      earthr/6371.e3/           ! Earth Radius
      DATA      t_grad/ 360.  /           ! 


      pi     =  acos( -1.)
      degrad =  pi / 180.
      t__rad =  pi *   2.
      nndata =         0

!HGal--v
                       nxdata = 13670204
      allocate (ANT_sh(nxdata))
      allocate (ANTlat(nxdata))
      allocate (ANTlon(nxdata))
!HGal--^

 
!-INPUT
! -----

      if (TOP30.eq.'a   ')                                        THEN
      write(6,*)  'ANTOPO: INPUT File rampxkmdem_wgsosu_v2.dat openING'
      open (unit=1,status='old',file='rampxkmdem_wgsosu_v2.dat')
      rewind     1
        DO  j=1,my
        DO  i=1,mx
        read(1,100) NST_sh(i,j),NSTsol(i,j)
 100    format(e15.6,i15)
        END DO
        END DO
      write(6,*)  'ANTOPO: INPUT File rampxkmdem_wgsosu_v2.dat IN'
      close(unit=1)
      ELSE
      write(6,*)  'ANTOPO: INPUT File ramp1kmdem_wgsosu_v2.bin openING'
      open (unit=1,status='old',file='ramp1kmdem_wgsosu_v2.bin',        &
     &      form='unformatted')
      rewind     1
           ii= 0
 1001   CONTINUE
           ii=1+ii
! #DO   DO ii=1,nxdata
        read(1,end=1000) ANTlat(ii),ANTlon(ii),ANT_sh(ii)
        GO TO 1001
 1000   CONTINUE
! #DO   END DO
                nwdata = ii
      close(unit=1)
      write(6,*)  'ANTOPO: INPUT File ramp1kmdem_wgsosu_v2.bin IN'
      write(6,*)  '        Nb DATA: ',nwdata
      write(6,*)  '                 '


!-Finest Resolution
! -----------------

            ddll= abs(NST__x(2,2)-NST__x(2,1))
        if (ddll .GT. t_grad)                                           &
     &      ddll=ddll-t_grad
            ddxx =                                                      &
     &         ((ddll*cos(NST__y(2,2)*degrad     )*degrad)**2           &
     &         +(     abs(NST__y(2,2)-NST__y(2,1))*degrad)**2)
            ddxx =   sqrt(ddxx)      *earthr
            inx  =        ddxx       *1.e-3
            jny  =        ddxx       *1.e-3
            write(6,600)  ddxx       *1.e-3,inx
 600        format(8x,'dx =',f9.3,'km   inx =',i6)


! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      DO j=1,my                               ! Loop on NST grid points
      DO i=1,mx

! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


!-Interpolation/Average
! ---------------------
          ddll   = earthr * cos(NST__y(i,j)*degrad)
          angl   = 0.5*pi -     NST__x(i,j)*degrad
          x_st_i = ddll   * cos(angl)
          y_st_j = ddll   * sin(angl)


!-Interpolation/Average
! ---------------------

!-Relevant Points
! ~~~~~~~~~~~~~~~
          nndata = nndata + 1
          nydata = 0
          dd_min = 1.e15 
        DO ii=1,nwdata
          phi    =              ANTlat(ii) *degrad
          ddll   = earthr * cos(phi)
          angl   = 0.5*pi -     ANTlon(ii) *degrad
          x_st_n = ddll   * cos(angl)
          y_st_n = ddll   * sin(angl)
          x_st_d =(x_st_n - x_st_i)
          y_st_d =(y_st_n - y_st_j)
          ddll   = x_st_d * x_st_d                                      &
     &           + y_st_d * y_st_d
          ddll   = sqrt(ddll)
          dd_min =  min(ddll,dd_min)
          if (ddll .LT. max(0.71*ddxx,4.e3))                        THEN
              nydata         = nydata + 1
              LOC__x(nydata) = x_st_n
              LOC__y(nydata) = y_st_n
              LOC_sh(nydata) = ANT_sh(ii)
          END if
        ENDDO
        if (mod(nndata,20).eq.1)                                        &
     &    write(6,601) NST__x(i,j),x_st_i*1.e-3,                        &
     &                 NST__y(i,j),y_st_j*1.e-3,                        &
     &                 dd_min*1.e-3,nydata
 601      format(9x,'LON,LAT =',2(f12.3,'  (',f9.3,')'),                &
     &              '  (',f9.3,')',i8,' Points')


!-Elementary Meshes characteristics
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        NST_sh(i,j) = 0.

          x0 = x_st_i - 0.5 *ddxx
          y0 = y_st_j - 0.5 *ddxx
          ddxxi = ddxx/inx
          ddxxj = ddxx/jny 
        DO jn=1,jny
        DO in=1,inx
          xx    = x0 + in*ddxxi
          yy    = y0 + jn*ddxxj

!-Distances to the current elementary Mesh
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            DO nn=1,4
                  ddnn(nn)=nn*earthr     ! arbitrary large distance
            END DO
          DO ii=1,nydata
            xxii = xx - LOC__x(ii)
            yyii = yy - LOC__y(ii)
            dd   = xxii*xxii + yyii*yyii
            dd   = sqrt(dd)

!-closest DTM points
! ~~~~~~~~~~~~~~~~~~
                  dd00     =.true.
            DO nn=1,4
              if (dd.lt.ddnn(nn).AND.dd00)                        THEN
                if (nn.lt.4)                                      THEN
                  DO n0=4,nn+1,-1
                  ddnn(n0) = ddnn(n0-1)
                  iinn(n0) = iinn(n0-1)
                  END DO
                END if
                  ddnn(nn) = dd
                  iinn(nn) = ii
                  dd00     =.false.
              END if
            END DO
          ENDDO


!-Kriging
! ~~~~~~~
                  dd = 0.
                  hh = 0.
            DO nn=1,4
            if   (ddnn(nn).LT.1500.)                                THEN
                  hh = hh                                               &
     &               + LOC_sh(iinn(nn))/ddnn(nn)
                  dd = dd    +      1. /ddnn(nn)
            END if
            END DO
            if   (dd      .GT.   0.)                                    &
     &            hh = hh                       /dd
                  NST_sh(i,j) = NST_sh(i,j)     +hh
        ENDDO
        ENDDO

!-Nested Grid Cell Average
! ~~~~~~~~~~~~~~~~~~~~~~~~
                  NST_sh(i,j) = NST_sh(i,j) / (inx*jny)
        if (mod(nndata,20).eq.1)                                        &
     &    write(6,602)   i,j,   NST_sh(i,j)
 602      format(9x,i3,i4,f14.3)


!-Distinction between land and sea (further refined)
! --------------------------------

       if (NST_sh(i,j).lt.0.01) THEN
        NSTsol(i,j)=1
       ELSE
        NSTsol(i,j)=3
       ENDif


!-No atmosphere below sea level...
! --------------------------------

       if (NST_sh(i,j).lt.0.0) THEN
        NST_sh(i,j)= 0.0
       ENDif


! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      ENDDO                                   ! Loop on NST grid points
      ENDDO

! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


!-OUTPUT
! ------

      write(6,*) 'ANTOPO: OUTPUT File rampxkmdem_wgsosu_v2.dat openING'
      open (unit=2,status='new',file='rampxkmdem_wgsosu_v2.dat')
      rewind     2
        DO  j=1,my
        DO  i=1,mx
        write(2,100) NST_sh(i,j),NSTsol(i,j)
        END DO
        END DO
      close(unit=1)
      write(6,*) 'ANTOPO: OUTPUT File rampxkmdem_wgsosu_v2.dat OUT'
      write(6,*) ' '
      write(6,*) ' '


      END if

!HGal--v
      deallocate (ANT_sh)
      deallocate (ANTlat)
      deallocate (ANTlon)
!HGal--^


      return
      END subroutine ANTOPO
      
!-------------------------------------------------------------------+
      subroutine AntTOPO(name)
! ANTARTIC TOPOGRAPHY specific Assimilation - Bamber 2009           |
!-------------------------------------------------------------------+
! Cecile Agosta, October 2012                                       |
! Input : NST__x : longitude (degree) of the NST grid               |
! ^^^^^^^ NST__y : latitude  (degree) of the NST grid               |
!                                                                   |
! Output: NST_sh: surface elevation                                 |
! ^^^^^^^ NSTsol: land (4) / ice(3) / sea (1) mask                  |
!         NSTice: percentage of grid cell covered by ice [0-100]    |
!                                                                   |
! Data  : name = 'Bamber'                                           |
! ^^^^^^^                                                           |
! File name        : krigged_dem_nsidc.bin                          |
! Dataset Title    : Antarctic 1 km Digital Elevation Model (DEM)   |
!                    from Combined ERS-1 Radar                      |
!                    and ICESat Laser Satellite Altimetry           |
! Dataset Creator  : Bamber, Jonathan L., Jose Luis Gomez-Dans,     |
!                    and Jennifer A. Griggs                         |
! Dataset Release Place: Boulder, Colorado USA                      |
! Dataset Publisher: NSIDC > National Snow and Ice Data Center      |
! Online Resource  : http://nsidc.org/data/nsidc-0422.html          |
!                                                                   |
! Reference:  Bamber, J.L., J.L. Gomez-Dans, and J.A. Griggs. 2009. |
!             A New 1 km Digital Elevation Model of the Antarctic   |
!             Derived from Combined Satellite Radar and Laser Data  |
!             Part 1: Data and Methods.                             |
!             The Cryosphere, 3, 101-111.                           |
!                                                                   |
!             Griggs, J. A. and J. L. Bamber. 2009.                 |
!             A New 1 km Digital Elevation Model of Antarctica      | 
!             Derived from Combined SatelliteRadar and Laser Data   |
!             Part 2: Validation and Error Estimates.               |
!             The Cryosphere, 3, 113-123.                           |
!                                                                   |
! Metadata :  krigged_dem_nsidc.bin.hdr                             |
!    description = { File Imported into ENVI.}                      |
!    samples = 5601                                                 |
!    lines   = 5601                                                 |
!    bands   = 1                                                    |
!    header offset = 0                                              |
!    file type = ENVI Standard                                      |
!    data type = 4                                                  |
!    interleave = bsq                                               |
!    sensor type = Unknown                                          |
!    byte order = 0                                                 |
!    map info = {Polar Stereographic South, 1, 1,                   |
!           -2800500., 2800500., 1000., 1000., WGS-84, units=Meters}|
!    projection info = {31, 6378137.0, 6356752.3, -71., 0.,         |
!         0.0, 0.0, WGS-84, Polar Stereographic South, units=Meters}|
!                                                                   |
! Data  : name = 'bedmap'                                           |
! ^^^^^^^                                                           |
! File name        : bedmap2.nc                                     |
! Dataset Title    : Bedmap2                                        |
!                                                                   |
! Dataset Creator  : British Antarctic Survey                       |
!                                                                   |
! Dataset Release Place: Cambridge, United Kingdom                  |
! Dataset Publisher: British Antarctic Survey                       |
! Online Resource  : http://www.antarctica.ac.uk//bas_research/     |
!                    our_research/az/bedmap2/                       |
!                                                                   |
! Reference: Fretwell P., Pritchard H.D., Vaughan D.G., Bamber J.L.,|
!                 Barrand N.E., Bell R.E., Bianchi C., Bingham R.G.,|
!               Blankenship D.D.,Casassa G., Catania G., Callens D.,|
!            Conway H., Cook A.J., Corr H.F.J., Damaske D., Damm V.,|
!                    Ferraccioli F., Forsberg R., Fujita S., Gim Y.,|
!           Gogineni P., Griggs J.A., Hindmarsh R.C.A., Holmlund P.,|
!          Holt J.W., Jacobel R.W., Jenkins A., Jokat W., Jordan T.,|
!                   King E.C., Kohler J., Krabill W., Riger-Kusk M.,|
!          Langley K.A., Leitchenkov G., Leuschen C., Luyendyk B.P.,|
!        Matsuoka K., Mouginot J., Nitsche F.O., Nogi Y., Nost O.A.,|
!         Popov S.V., Rignot E., Rippin D.M., Rivera A., Roberts J.,|
!     Ross N., Siegert M.J., Smith A.M., Steinhage D., Studinger M.,|
!             Sun B., Tinto B.K., Welch B.C., Wilson D., Young D.A.,|
!                                Xiangbin C., & Zirizzotti A. (2013)|
!      Bedmap2: improved ice bed, surface and thickness datasets for|
!                            Antarctica. The Cryosphere, 7, 375-393.|
!        http://www.the-cryosphere.net/7/375/2013/tc-7-375-2013.pdf |
!                                                                   |
!-------------------------------------------------------------------+

      implicit none

!-Netcdf specifications
! ---------------------

      include 'NetCDF.inc'


!-NST variables
! -------------

      include 'NSTdim.inc'
      include 'NSTvar.inc'
      include 'NESTOR.inc'
      include 'LOCfil.inc'
      
      character*6, intent(in)::name
      
!-Local variables
! ---------------
      integer      i,j,in,jn,ii,jj,maptyp
      integer      dem_ii,dem_jj,di0,dj0,ndh
      integer      imez,jmez
      real         xx_max,xx_min
      real         dem_dh
      real         dem_x0,dem_y0
      real         lon,lat,GEddxx,xx,yy
      real         sh,ice,grd,rck
      real         nul
      real x0,y0
      real xl,xr,yl,yu
      real ref_area,area
! ....Size of full dem file
      integer      dem_mx,dem_my
! ....dem variables      
      real*4, allocatable::dem_sh(:,:), dem_msk(:,:), dem_rck(:,:)
! ....Reading dem file
      character*80 dem_file, var_units
      integer recl, fID ! Record length, file ID
! ....Reading constant file
      character*80 file
      logical exist
      character*3  dhc,mxc,myc
      character*10 TypeGL
! ....Local NST variables
      real         NST_dh
      real         NST_xx(mx,my),NST_yy(mx,my),NSTsol_tmp(mx,my)
      
      NST_dh  = NST_dx/1000.
      
      ! file name
      write(mxc,'(i3)') mx
      if(mx<100) write(mxc,'(i2)') mx
      write(myc,'(i3)') my
      if(my<100) write(myc,'(i2)') my
      write(dhc,'(i3)') int(NST_dh)
      if(NST_dh<100) write(dhc,'(i2)') int(NST_dh)
      if(NST_dh<10)  write(dhc,'(i1)') int(NST_dh)
      
      TypeGL="AN"//trim(dhc)//"km"
      file=trim(PFXdir)//'AntTOPO/MARcst-'                              &
     &       //trim(TypeGL)//'-'//                                      &
     &       trim(mxc)//'x'//trim(myc)//'.cdf'  
      
      inquire(file=file, exist=exist)
      if (exist) then
          write(6,*) 'Reading topography and masks'
          write(6,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
          write(6,*) "> Read ", file
          write(6,*)
          call UNropen (file,fID,file)
          call UNsread(fID,'SH',1,1,1,1,                                &
     &                 mx,my,1,                                         &
     &                 var_units,NST_sh)
          call UNsread(fID,'ICE',1,1,1,1,                               &
     &                 mx,my,1,                                         &
     &                 var_units,NSTice)
          call UNsread(fID,'SOL',1,1,1,1,                               &
     &                 mx,my,1,                                         &
     &                 var_units,NSTsol_tmp)
      NSTsol = int(NSTsol_tmp)
          call UNsread(fID,'GROUND',1,1,1,1,                            &
     &                 mx,my,1,                                         &
     &                 var_units,NSTgrd)
          call UNsread(fID,'AREA',1,1,1,1,                              &
     &                 mx,my,1,                                         &
     &                 var_units,NSTarea)
          call UNsread(fID,'ROCK',1,1,1,1,                              &
     &                 mx,my,1,                                         &
     &                 var_units,NSTrck)
      else


!-open BSQ binary file
! --------------------

!-characteristics of the DEM grid
! -------------------------------
          if (name.eq.'Bamber') then
              dem_mx = 5601
              dem_my = 5601
              dem_x0 = -2800.
              dem_y0 =  2800.
              dem_dh =  1.
          else if (name.eq.'bedmap') then
              dem_mx = 6667
              dem_my = 6667
              dem_x0 = -3333.
              dem_y0 = -3333.
              dem_dh  =  1.
          endif
          allocate(dem_sh(dem_mx,dem_my))
          allocate(dem_msk(dem_mx,dem_my))
          allocate(dem_rck(dem_mx,dem_my))
      
          if (name.eq.'Bamber') then

!-    opening and reading Bamber 2009 data file
!     =========================================
              write(6,*) 'Topography : Bamber(2009) Antarctic 1km DEM'
              write(6,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
              dem_file = trim(PFXdir)//'AntTOPO/krigged_dem_nsidc.bin'
              write(6,*) "> Read ", dem_file
              write(6,*)
              Inquire(iolength=recl) dem_sh
              open (unit=10,status='old',file=dem_file,                &
     &              form='unformatted',access='direct',recl=recl)
              Read (10,rec=1) dem_sh
              close(10)
              dem_msk = 0.
              dem_rck = 0.
          else if (name.eq.'bedmap') then
!-    opening and reading bedmap2 data file
!     =========================================
              write(6,*) 'Topography : bedmap2 Antarctic 1km DEM'
              write(6,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
              dem_file = trim(PFXdir)//'AntTOPO/bedmap2.nc'
              write(6,*) "> Read ", dem_file
              write(6,*)
              call UNropen (dem_file,fID,dem_file)
              call UNsread(fID,'sh',1,1,1,1,                            &
     &                     dem_mx,dem_my,1,                             &
     &                     var_units,dem_sh)
              call UNsread(fID,'icemask',1,1,1,1,                       &
     &                     dem_mx,dem_my,1,                             &
     &                     var_units,dem_msk)
              call UNsread(fID,'rockmask',1,1,1,1,                      &
     &                     dem_mx,dem_my,1,                             &
     &                     var_units,dem_rck)
               do jj=1,dem_my
               do ii=1,dem_mx
                   if (dem_msk(ii,jj).lt.-999) then
                       dem_msk(ii,jj) = 0.
                   else if (dem_msk(ii,jj).eq.0.) then
                        dem_msk(ii,jj) = 1.
                   else if (dem_msk(ii,jj).eq.1.) then
                        dem_msk(ii,jj) = 0.
                   endif
                   if (dem_rck(ii,jj).lt.-999) then
                        dem_rck(ii,jj) = 0.
                   else if (dem_rck(ii,jj).eq.0.) then
                         dem_rck(ii,jj) = 1.
                   endif
                enddo
                enddo
          endif
!-Verify than the map projection  used for MAR 
!                  is the same than in the DEM
! ============================================
          open (unit=51,status='old',file='MARgrd.ctr')
           read (51,*) !- - - - - - - - - - - - - - - - - -
           read (51,*) !- - - - - - - - - - - - - - - - - -
           read (51,*) !- - - - - - - - - - - - - - - - - -
           read (51,*) !- - - - - - - - - - - - - - - - - -
           read (51,*) maptyp
           read (51,*) !- - - - - - - - - - - - - - - - - -
           read (51,*) nul
           read (51,*) imez  
           read (51,*) nul
           read (51,*) jmez 
           read (51,*) !- - - - - - - - - - - - - - - - - -
           read (51,*) nul
           read (51,*) !- - - - - - - - - - - - - - - - - -
           read (51,*) GEddxx
           read (51,*) !- - - - - - - - - - - - - - - - - -
          close(unit=51)
          
          if (maptyp.ne.0) then
            Print*, "++++++++++++++++++++++++++++++++++"
            Print*, "Routine USRant.f ----> Warning !!!"
            Print*, "For Antarctica,                   "
            Print*, "choose map type = 0 in MARgrd.ctr "
            Print*, "(Standard stereo south projection)"
            Print*, "NESTOR stopped NOW !!!            "
            Print*, "++++++++++++++++++++++++++++++++++"
            STOP
          endif
          
          if (GEddxx.eq.90) then
            do j=1,my
            do i=1,mx
              NST_yy(i,j) = NSTgdy(j)
              NST_xx(i,j) = NSTgdx(i)
            enddo
            enddo
          else
            do j=1,my
            do i=1,mx
              lon = NST__x(i,j)
              lat = NST__y(i,j)
              call StereoSouth_inverse(lon,lat,90.,xx,yy)
              NST_xx(i,j) = xx
              NST_yy(i,j) = yy
            enddo
            enddo
          endif
          
          call StereoSouth_inverse(0.,-71.,GEddxx,x0,y0)
          xl = x0 - NST_dh/2.
          xr = x0 + NST_dh/2.
          yl = y0 - NST_dh/2.
          yu = y0 + NST_dh/2.
          call areaLambertAzimuthal(xl,xr,yl,yu,GEddxx,ref_area)
          do j=1,my
          do i=1,mx
              xl = NSTgdx(i) - NST_dh/2.
              xr = NSTgdx(i) + NST_dh/2.
              yl = NSTgdy(j) - NST_dh/2.
              yu = NSTgdy(j) + NST_dh/2.
              call areaLambertAzimuthal(xl,xr,yl,yu,GEddxx,area)
              NSTarea(i,j) = area/ref_area
          enddo
          enddo
!-Average the DEM on the MAR grid
! ===============================
          ndh     = nint(NST_dh/dem_dh)
          di0     = int((ndh-1)/2.)
          dj0     = int((ndh-1)/2.)
          xx_min = dem_x0 + NST_dh/2.
          xx_max = -dem_x0 - NST_dh/2.
          print*, ndh,di0,dj0,xx_min,xx_max
          do j=1,my
          do i=1,mx

            if (NST__y(i,j).lt.-58) then !ckittel /Bedmap2 over Antarctica, ETOPOGO elsewhere
             sh  = 0.
             grd = 0.
             rck = 0.
             ice = 0.
              if (     NST_xx(i,j).lt.xx_min                            &
     &            .or. NST_xx(i,j).gt.xx_max                            &
     &            .or. NST_yy(i,j).lt.xx_min                            &
     &            .or. NST_yy(i,j).gt.xx_max) then
               NSTgrd(i,j) = 0.
               NST_sh(i,j) = 0.
               NSTice(i,j) = 0.
               NSTrck(i,j) = 0.
               NSTsol(i,j) = 1
              else
               dem_ii = nint(NST_xx(i,j) - dem_x0)
               if (name.eq.'Bamber') then
                  dem_jj = nint(dem_y0 - NST_yy(i,j))
               else if (name.eq.'bedmap') then
                  dem_jj = nint(NST_yy(i,j) - dem_y0)
               endif
               do jn=1,ndh
               do in=1,ndh
                  ii = dem_ii - di0 + in
                  jj = dem_jj - dj0 + jn
                  if (dem_sh(ii,jj).ge.-990.) then
                   ice = ice + 1
                   sh  = sh + dem_sh(ii,jj)
                  endif
                  grd = grd + dem_msk(ii,jj)
                  rck = rck + dem_rck(ii,jj)
               enddo
               enddo
               NST_sh(i,j) = sh  / (ndh*ndh)
               ! +---No atmosphere below sea level
               ! +   -----------------------------
               NST_sh(i,j) = max(NST_sh(i,j),0.)
               NSTice(i,j) = ice / (ndh*ndh) *100.
               NSTgrd(i,j) = grd / (ndh*ndh) *100.
               NSTrck(i,j) = rck / (ndh*ndh) *100.
               if (NSTice(i,j).ge.30.) then
                 NSTsol(i,j) = 3
               else
                 NSTsol(i,j) = 1
                 NST_sh(i,j) = 0.
               endif
             endif
           endif
          enddo
          enddo
      endif

      return
      END subroutine AntTOPO
