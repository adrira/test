C +   BEGIN     for2bam.inc
      integer   nx    ,ny    ,nz    ,nw
      parameter(nx=5601,ny=5601,nz=001,nw=100)
      real      x_axis(nx   )
      real      y_axis(   ny)
      real      z_axis(       nz)
      real      w_axis(              nw)
      real      Longit(nx,ny)
      real      Latitu(nx,ny)
      real      OroOBS(nx,ny)
c #2D real      OroSIM(nx,ny)
      real      SolTyp(nx,ny)
c #3D real      fxyz_0(nx,ny,nz)
c #3D real      fxyz_1(nx,ny,nz)
c #3D real      fxyz_2(nx,ny,nz)
c #3D real      fxyz_3(nx,ny,nz)
c #3D real      fxyz_4(nx,ny,nz)
c #3D real      fxyz_5(nx,ny,nz)
c #3D real      fxyz_6(nx,ny,nz)
c #3D real      fxyz_7(nx,ny,nz)
c #3D real      fxyz_8(nx,ny,nz)
c #3D real      fxyz_9(nx,ny,nz)
c #3D real      fxyw_0(nx,ny,nw)
c #3D real      fxyw_1(nx,ny,nw)
c #3D real      fxyw_2(nx,ny,nw)
c #3D real      fxyw_3(nx,ny,nw)
c #3D real      fxyw_4(nx,ny,nw)
c #3D real      fxyw_5(nx,ny,nw)
c #3D real      fxyw_6(nx,ny,nw)
c #3D real      fxyw_7(nx,ny,nw)
c #3D real      fxyw_8(nx,ny,nw)
c #3D real      fxyw_9(nx,ny,nw)
      common   /for2cdf_var/x_axis,y_axis,z_axis,w_axis
     .                     ,Longit,Latitu,OroOBS
c #3D.                                          ,OroSIM,SolTyp
c #3D.                     ,fxyz_0,fxyz_1,fxyz_2,fxyz_3,fxyz_4
c #3D.                     ,fxyz_5,fxyz_6,fxyz_7,fxyz_8,fxyz_9
c #3D.                     ,fxyw_0,fxyw_1,fxyw_2,fxyw_3,fxyw_4
c #3D.                     ,fxyw_5,fxyw_6,fxyw_7,fxyw_8,fxyw_9
C +   END       for2bam.inc
