C   +-------------------------------------------------------------------+
C   |  subroutine USRant          June 2003 (HG) Oct 2014 (CA)  NESTING |
C   +-------------------------------------------------------------------+
C   | USRant adapt NESTOR to Antarctic region                           |
C   | See bellow a new version (AntTOPO) with Bamber (2009)             |
C   |                                                                   |
C   | Input : - subnam : Name of the subroutine                         |
C   | ^^^^^^^            where USRant is called                         |
C   |                                                                   |
C   | Maintainer : Hubert Gallée, Cécile Agosta                         |
C   | ^^^^^^^^^^^^                                                      |
C   |                                                                   |
C   +-------------------------------------------------------------------+

      subroutine USRant(subnam)


      implicit none

C +---General variables
C +   -----------------

      include 'NSTdim.inc'
      include 'NSTvar.inc'
      include 'NESTOR.inc'
      include 'LSCvar.inc'

C +---local variables
C +   ---------------

      CHARACTER*6 subnam
  
C +   +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

C +---Topography for Antarctic    
C +   ========================

      if (subnam.eq.'NESTOR') call ANTOPO  

      if (subnam.eq.'Bamber' .or. subnam.eq.'bedmap') then
          call AntTOPO(subnam)  !+ CA +!
      endif

      if (subnam.eq.'Racmo2') call AntRACMO !+ CA +!

      END subroutine USRant

C +   +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
      
C   +-------------------------------------------------------------------+
      subroutine AntRACMO
C   +-------------------------------------------------------------------+

C +---Netcdf specifications
C +   ---------------------
      include 'NetCDF.inc'
      
C +---NST variables
C +   -------------

      include 'NSTdim.inc'
      include 'NSTvar.inc'
      include 'NESTOR.inc'
      include 'LOCfil.inc'
      
      real NST_dh
      
C +...Reading dem file        
      integer fID
      character*80 dem_file
      integer dem_mx,dem_my
      parameter(dem_mx=110,dem_my=91)
      integer dem_xmin,dem_ymin,dem_xmax,dem_ymax
      parameter(dem_xmin=-2600,dem_ymin=-2200)
      parameter(dem_xmax= 2850,dem_ymax= 2300)
      character*80   var_units
      real           dem_sh(dem_mx,dem_mx),dem_msk(dem_mx,dem_mx)
      
      integer i,j,ii,jj

      NST_dh  = NST_dx/1000.
      
      write(6,*) 'Topography : RACMO-27 interpolated on 50km MAR grid'
      write(6,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
    
      dem_file = trim(PFXdir)//'AntTOPO/RACMO-ANT27onMAR-50km.sh.nc'
      write(6,*) "> Read ", dem_file,"  ..."
      write(6,*)
      
      CALL UNropen (dem_file,fID,title)
      CALL UNsread(fID,'sh',1,1,1,1,
     .                    dem_mx,dem_my,1,
     .                    var_units,dem_sh)
     
      CALL UNsread(fID,'msk',1,1,1,1,
     .                    dem_mx,dem_my,1,
     .                    var_units,dem_msk)

      do j=1,my
      do i=1,mx
        if ( NSTgdx(i).ge.dem_xmin .and. NSTgdy(j).ge.dem_ymin
     .  .and.NSTgdx(i).le.dem_xmax .and. NSTgdy(j).le.dem_ymax) then
          ii = int((NSTgdx(i)-dem_xmin)/NST_dh) +1
          jj = int((NSTgdy(j)-dem_ymin)/NST_dh) +1
C           print*,i,j,ii,jj
          NST_sh(i,j)=dem_sh (ii,jj)
          NSTice(i,j)=dem_msk(ii,jj)*100.
        else
          NST_sh(i,j)=0
          NSTice(i,j)=0.
        endif
        ! +---No atmosphere below sea level
        ! +   -----------------------------
        NST_sh(i,j) = max(NST_sh(i,j),0.)
        if (NSTice(i,j).gt.50.) then
          NSTsol(i,j) = 3
        else
          NSTsol(i,j) = 1
        endif
      enddo
      enddo
      
      end subroutine AntRACMO
C   +-------------------------------------------------------------------+


C   +-------------------------------------------------------------------+
C   |  subroutine ANTOPO                          October 2002  NESTING |
C   |             ANTARTIC TOPOGRAPHY specific Assimilation             |
C   +-------------------------------------------------------------------+
C   |                                                                   |
C   | Input : NST__x : longitude (degree) of the NST grid               |
C   | ^^^^^^^ NST__y : latitude  (degree) of the NST grid               |
C   |                                                                   |
C   | Output: NST_sh: surface elevation                                 |
C   | ^^^^^^^ NSTsol: land (4) / sea (1) mask                           |
C   |                                                                   |
C   | Method: Divide each nested Grid Cell in elementary Meshes         |
C   | ^^^^^^^ much small than either the nested or the DTM Mesh         |
C   |         Compute geographic Coordinates of each elementary Mesh    |
C   |         Compute Distance of this Mesh to 4 closest DTM Meshes     |
C   |         Compute Height of this Mesh by kriging                    |
C   |         Nested Grid Cell Height is the Average                    |
C   |                                 of the elementary Meshes Heights  |
C   |                                                                   |
C   | DATA Source: Radarsat Antarctic Mapping Project Digital           |
C   | ^^^^^^^^^^^^ Digital  Elevation Model   Version 2                 |
C   |              ftp site : sidads.colorado.edu                       |
C   |              directory: /pub/DATASETS/RAMP/DEM_V2/1KM/ASCII       |
C   |              file     : ramp1kmdem_wgsosu_v2.txt.gz               |
C   | Reference:   Liu, H., K. Jezek, B. Li, and Z. Zhao. 2001.         |
C   | ^^^^^^^^^^   Radarsat Antarctic Mapping Project                   |
C   |              Digital Elevation Model Version 2.                   |
C   |              Boulder, CO: National Snow and Ice Data Center.      |
C   |              Digital media.                                       |
C   |                                                                   |
C   +-------------------------------------------------------------------+


      subroutine ANTOPO


      implicit none


C +---General and local variables
C +   ---------------------------

      include 'NSTdim.inc'
      include 'NSTvar.inc'
      include 'NESTOR.inc'
      include 'MARvar.inc'
      include 'LOCfil.inc'
      include 'NetCDF.inc'

      INTEGER   nxdata,nydata ,nwdata,nndata
      PARAMETER(nxdata=13670204) 
      INTEGER   ii,i,j

      REAL      ANT_sh(nxdata)
      REAL      ANTlat(nxdata),ANTlon(nxdata)

      REAL      LOC_sh(100000),ddxx          ,ddll       ,dd_min
      REAL      LOC__x(100000),LOC__y(100000)

      REAL      pi           ,degrad       ,t_grad       ,t__rad
      REAL      phi          ,angl
      REAL      x_st_i       ,y_st_j       ,x_st_n       ,y_st_n
      REAL                                  x_st_d       ,y_st_d
      REAL      di_lon       ,di_lat       ,dj_lon       ,dj_lat
      REAL      inilon       ,inilat
      REAL      curlon       ,curlat       ,earthr
      REAL      minLON       ,minLAT       ,difLON       ,difLAT
      INTEGER   inx          ,jny
      INTEGER   in           ,jn

      LOGICAL   dd00
      INTEGER   nn           ,iinn(4)      ,jjnn(4)      ,n0
      REAL      x0           ,y0           ,ddxxi        ,ddxxj
      REAL      xx           ,yy           ,xxii         ,yyii
      REAL      dd           ,ddnn(4)                    ,hh


      DATA      earthr/6371.e3/           ! Earth Radius
      DATA      t_grad/ 360.  /           ! 


      pi     =  acos( -1.)
      degrad =  pi / 180.
      t__rad =  pi *   2.
      nndata =         0

 
C +---INPUT
C +   -----

      if (TOP30.eq.'a   ')                                        THEN
      write(6,*)  'ANTOPO: INPUT File rampxkmdem_wgsosu_v2.dat openING'
      open (unit=1,status='old',file='rampxkmdem_wgsosu_v2.dat')
      rewind     1
        DO  j=1,my
        DO  i=1,mx
        read(1,100) NST_sh(i,j),NSTsol(i,j)
 100    format(e15.6,i15)
        END DO
        END DO
      write(6,*)  'ANTOPO: INPUT File rampxkmdem_wgsosu_v2.dat IN'
      close(unit=1)
      ELSE
      write(6,*)  'ANTOPO: INPUT File ramp1kmdem_wgsosu_v2.bin openING'
      open (unit=1,status='old',file='ramp1kmdem_wgsosu_v2.bin',
     .      form='unformatted')
      rewind     1
           ii= 0
 1001   CONTINUE
           ii=1+ii
c #DO   DO ii=1,nxdata
        read(1,end=1000) ANTlat(ii),ANTlon(ii),ANT_sh(ii)
        GO TO 1001
 1000   CONTINUE
c #DO   END DO
                nwdata = ii
      close(unit=1)
      write(6,*)  'ANTOPO: INPUT File ramp1kmdem_wgsosu_v2.bin IN'
      write(6,*)  '        Nb DATA: ',nwdata
      write(6,*)  '                 '


C +---Finest Resolution
C +   -----------------

            ddll= abs(NST__x(2,2)-NST__x(2,1))
        if (ddll .GT. t_grad) 
     .      ddll=ddll-t_grad
            ddxx = 
     .         ((ddll*cos(NST__y(2,2)*degrad     )*degrad)**2
     .         +(     abs(NST__y(2,2)-NST__y(2,1))*degrad)**2)
            ddxx =   sqrt(ddxx)      *earthr
            inx  =        ddxx       *1.e-3
            jny  =        ddxx       *1.e-3
            write(6,600)  ddxx       *1.e-3,inx
 600        format(8x,'dx =',f9.3,'km   inx =',i6)


C +   +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      DO j=1,my                               ! Loop on NST grid points
      DO i=1,mx

C +   +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


C +---Interpolation/Average
C +   ---------------------
          ddll   = earthr * cos(NST__y(i,j)*degrad)
          angl   = 0.5*pi -     NST__x(i,j)*degrad
          x_st_i = ddll   * cos(angl)
          y_st_j = ddll   * sin(angl)


C +---Interpolation/Average
C +   ---------------------

C +---Relevant Points
C +   ~~~~~~~~~~~~~~~
          nndata = nndata + 1
          nydata = 0
          dd_min = 1.e15 
        DO ii=1,nwdata
          phi    =              ANTlat(ii) *degrad
          ddll   = earthr * cos(phi)
          angl   = 0.5*pi -     ANTlon(ii) *degrad
          x_st_n = ddll   * cos(angl)
          y_st_n = ddll   * sin(angl)
          x_st_d =(x_st_n - x_st_i)
          y_st_d =(y_st_n - y_st_j)
          ddll   = x_st_d * x_st_d 
     .           + y_st_d * y_st_d
          ddll   = sqrt(ddll)
          dd_min =  min(ddll,dd_min)
          if (ddll .LT. max(0.71*ddxx,4.e3))                      THEN
              nydata         = nydata + 1
              LOC__x(nydata) = x_st_n
              LOC__y(nydata) = y_st_n
              LOC_sh(nydata) = ANT_sh(ii)
          END if
        ENDDO
        if (mod(nndata,20).eq.1)
     .    write(6,601) NST__x(i,j),x_st_i*1.e-3,
     .                 NST__y(i,j),y_st_j*1.e-3,
     .                 dd_min*1.e-3,nydata
 601      format(9x,'LON,LAT =',2(f12.3,'  (',f9.3,')'),
     .              '  (',f9.3,')',i8,' Points')


C +---Elementary Meshes characteristics
C +   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        NST_sh(i,j) = 0.

          x0 = x_st_i - 0.5 *ddxx
          y0 = y_st_j - 0.5 *ddxx
          ddxxi = ddxx/inx
          ddxxj = ddxx/jny 
        DO jn=1,jny
        DO in=1,inx
          xx    = x0 + in*ddxxi
          yy    = y0 + jn*ddxxj

C +---Distances to the current elementary Mesh
C +   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            DO nn=1,4
                  ddnn(nn)=nn*earthr     ! arbitrary large distance
            END DO
          DO ii=1,nydata
            xxii = xx - LOC__x(ii)
            yyii = yy - LOC__y(ii)
            dd   = xxii*xxii + yyii*yyii
            dd   = sqrt(dd)

C +---closest DTM points
C +   ~~~~~~~~~~~~~~~~~~
                  dd00     =.true.
            DO nn=1,4
              if (dd.lt.ddnn(nn).AND.dd00)                        THEN
                if (nn.lt.4)                                      THEN
                  DO n0=4,nn+1,-1
                  ddnn(n0) = ddnn(n0-1)
                  iinn(n0) = iinn(n0-1)
                  END DO
                END if
                  ddnn(nn) = dd
                  iinn(nn) = ii
                  dd00     =.false.
              END if
            END DO
          ENDDO


C +---Kriging
C +   ~~~~~~~
                  dd = 0.
                  hh = 0.
            DO nn=1,4
            if   (ddnn(nn).LT.1500.)                           THEN
                  hh = hh 
     .               + LOC_sh(iinn(nn))/ddnn(nn)
                  dd = dd    +      1. /ddnn(nn)
            END if
            END DO
            if   (dd      .GT.   0.)
     .            hh = hh                       /dd
                  NST_sh(i,j) = NST_sh(i,j)     +hh
        ENDDO
        ENDDO

C +---Nested Grid Cell Average
C +   ~~~~~~~~~~~~~~~~~~~~~~~~
                  NST_sh(i,j) = NST_sh(i,j) / (inx*jny)
        if (mod(nndata,20).eq.1)
     .    write(6,602)   i,j,   NST_sh(i,j)
 602      format(9x,i3,i4,f14.3)


C +---Distinction between land and sea (further refined)
C +   --------------------------------

       if (NST_sh(i,j).lt.0.01) THEN
        NSTsol(i,j)=1
       ELSE
        NSTsol(i,j)=3
       ENDif


C +---No atmosphere below sea level...
C +   --------------------------------

       if (NST_sh(i,j).lt.0.0) THEN
        NST_sh(i,j)= 0.0
       ENDif


C +   +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      ENDDO                                   ! Loop on NST grid points
      ENDDO

C +   +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


C +---OUTPUT
C +   ------

      write(6,*) 'ANTOPO: OUTPUT File rampxkmdem_wgsosu_v2.dat openING'
      open (unit=2,status='new',file='rampxkmdem_wgsosu_v2.dat')
      rewind     2
        DO  j=1,my
        DO  i=1,mx
        write(2,100) NST_sh(i,j),NSTsol(i,j)
        END DO
        END DO
      close(unit=1)
      write(6,*) 'ANTOPO: OUTPUT File rampxkmdem_wgsosu_v2.dat OUT'
      write(6,*) ' '
      write(6,*) ' '


      END if


      return
      END subroutine ANTOPO
      
C    +-------------------------------------------------------------------+
      subroutine AntTOPO(name)
C    | ANTARTIC TOPOGRAPHY specific Assimilation - Bamber 2009           |
C    +-------------------------------------------------------------------+
C    | Cecile Agosta, October 2012                                       |
C    | Input : NST__x : longitude (degree) of the NST grid               |
C    | ^^^^^^^ NST__y : latitude  (degree) of the NST grid               |
C    |                                                                   |
C    | Output: NST_sh: surface elevation                                 |
C    | ^^^^^^^ NSTsol: land (4) / ice(3) / sea (1) mask                  |
C    |         NSTice: percentage of grid cell covered by ice [0-100]    |
C    |                                                                   |
C    | Data  : name = 'Bamber'                                           |
C    | ^^^^^^^                                                           |
C    | File name        : krigged_dem_nsidc.bin                          |
C    | Dataset Title    : Antarctic 1 km Digital Elevation Model (DEM)   |
C    |                    from Combined ERS-1 Radar                      |
C    |                    and ICESat Laser Satellite Altimetry           |
C    | Dataset Creator  : Bamber, Jonathan L., Jose Luis Gomez-Dans,     |
C    |                    and Jennifer A. Griggs                         |
C    | Dataset Release Place: Boulder, Colorado USA                      |
C    | Dataset Publisher: NSIDC > National Snow and Ice Data Center      |
C    | Online Resource  : http://nsidc.org/data/nsidc-0422.html          |
C    |                                                                   |
C    | Reference:  Bamber, J.L., J.L. Gomez-Dans, and J.A. Griggs. 2009. |
C    |             A New 1 km Digital Elevation Model of the Antarctic   |
C    |             Derived from Combined Satellite Radar and Laser Data  |
C    |             Ð Part 1: Data and Methods.The Cryosphere,3,101-111.  |
C    |                                                                   |
C    |             Griggs, J. A. and J. L. Bamber. 2009.                 |
C    |             A New 1 km Digital Elevation Model of Antarctica      | 
C    |             Derived from Combined Radar and Laser Data Ð Part 2:  |
C    |             Validation and Error Estimates.The Cryosphere,3,113Ð123.
C    |                                                                   |
C    | Metadata :  krigged_dem_nsidc.bin.hdr                             |
C    |    description = { File Imported into ENVI.}                      |
C    |    samples = 5601                                                 |
C    |    lines   = 5601                                                 |
C    |    bands   = 1                                                    |
C    |    header offset = 0                                              |
C    |    file type = ENVI Standard                                      |
C    |    data type = 4                                                  |
C    |    interleave = bsq                                               |
C    |    sensor type = Unknown                                          |
C    |    byte order = 0                                                 |
C    |    map info = {Polar Stereographic South, 1, 1,                   |
C    |           -2800500., 2800500., 1000., 1000., WGS-84, units=Meters}|
C    |    projection info = {31, 6378137.0, 6356752.3, -71., 0.,         |
C    |         0.0, 0.0, WGS-84, Polar Stereographic South, units=Meters}|
C    |                                                                   |
C    | Data  : name = 'bedmap'                                           |
C    | ^^^^^^^                                                           |
C    | File name        : bedmap2.nc                                     |
C    | Dataset Title    : Bedmap2                                        |
C    |                                                                   |
C    | Dataset Creator  : British Antarctic Survey                       |
C    |                                                                   |
C    | Dataset Release Place: Cambridge, United Kingdom                  |
C    | Dataset Publisher: British Antarctic Survey                       |
C    | Online Resource  : http://www.antarctica.ac.uk//bas_research/     |
C    |                    our_research/az/bedmap2/                       |
C    |                                                                   |
C    | Reference: Fretwell P., Pritchard H.D., Vaughan D.G., Bamber J.L.,|
C    |                 Barrand N.E., Bell R.E., Bianchi C., Bingham R.G.,|
C    |               Blankenship D.D.,Casassa G., Catania G., Callens D.,|
C    |            Conway H., Cook A.J., Corr H.F.J., Damaske D., Damm V.,|
C    |                    Ferraccioli F., Forsberg R., Fujita S., Gim Y.,|
C    |           Gogineni P., Griggs J.A., Hindmarsh R.C.A., Holmlund P.,|
C    |          Holt J.W., Jacobel R.W., Jenkins A., Jokat W., Jordan T.,|
C    |                   King E.C., Kohler J., Krabill W., Riger-Kusk M.,|
C    |          Langley K.A., Leitchenkov G., Leuschen C., Luyendyk B.P.,|
C    |        Matsuoka K., Mouginot J., Nitsche F.O., Nogi Y., Nost O.A.,|
C    |         Popov S.V., Rignot E., Rippin D.M., Rivera A., Roberts J.,|
C    |     Ross N., Siegert M.J., Smith A.M., Steinhage D., Studinger M.,|
C    |             Sun B., Tinto B.K., Welch B.C., Wilson D., Young D.A.,|
C    |                                Xiangbin C., & Zirizzotti A. (2013)|
C    |      Bedmap2: improved ice bed, surface and thickness datasets for|
C    |                            Antarctica. The Cryosphere, 7, 375–393.|
C    |        http://www.the-cryosphere.net/7/375/2013/tc-7-375-2013.pdf |
C    |                                                                   |
C    +-------------------------------------------------------------------+

      implicit none

C +---Netcdf specifications
C +   ---------------------

      include 'NetCDF.inc'


C +---NST variables
C +   -------------

      include 'NSTdim.inc'
      include 'NSTvar.inc'
      include 'NESTOR.inc'
      include 'LOCfil.inc'
      
      character*6, intent(in)::name
      
C +---Local variables
C +   ---------------
      integer      i,j,in,jn,ii,jj,maptyp
      integer      dem_ii,dem_jj,di0,dj0,ndh
      integer      imez,jmez
      real         xx_max,xx_min
      real         dem_dh
      real         dem_x0,dem_y0
      real         lon,lat,GEddxx,xx,yy
      real         sh,ice,grd,rck
      real         nul
      real x0,y0
      real xl,xr,yl,yu
      real ref_area,area
C +...Size of full dem file
      integer      dem_mx,dem_my
C +...dem variables      
      real*4, allocatable::dem_sh(:,:), dem_msk(:,:), dem_rck(:,:)
C +...Reading dem file
      character*80 dem_file, var_units
      integer recl, fID ! Record length, file ID
C +...Reading constant file
      character*80 file
      logical exist
      character*3  dhc,mxc,myc
      character*10 TypeGL
C +...Local NST variables
      real         NST_dh
      real         NST_xx(mx,my),NST_yy(mx,my),NSTsol_tmp(mx,my)
      
      NST_dh  = NST_dx/1000.
      
      ! file name
      write(mxc,'(i3)') mx
      if(mx<100) write(mxc,'(i2)') mx
      write(myc,'(i3)') my
      if(my<100) write(myc,'(i2)') my
      write(dhc,'(i3)') int(NST_dh)
      if(NST_dh<100) write(dhc,'(i2)') int(NST_dh)
      if(NST_dh<10)  write(dhc,'(i1)') int(NST_dh)
      
      TypeGL="AN"//trim(dhc)//"km"
      file=trim(PFXdir)//'AntTOPO/MARcst-'
     .       //trim(TypeGL)//'-'//
     .       trim(mxc)//'x'//trim(myc)//'.cdf'
      
      inquire(file=file, exist=exist)
      if (exist) then
          write(6,*) 'Reading topography and masks'
          write(6,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
          write(6,*) "> Read ", file
          write(6,*)
          call UNropen (file,fID,file)
          call UNsread(fID,'SH',1,1,1,1,
     .                 mx,my,1,
     .                 var_units,NST_sh)
          call UNsread(fID,'ICE',1,1,1,1,
     .                 mx,my,1,
     .                 var_units,NSTice)
          call UNsread(fID,'SOL',1,1,1,1,
     .                 mx,my,1,
     .                 var_units,NSTsol_tmp)
      NSTsol = int(NSTsol_tmp)
          call UNsread(fID,'GROUND',1,1,1,1,
     .                 mx,my,1,
     .                 var_units,NSTgrd)
          call UNsread(fID,'AREA',1,1,1,1,
     .                 mx,my,1,
     .                 var_units,NSTarea)
          call UNsread(fID,'ROCK',1,1,1,1,
     .                 mx,my,1,
     .                 var_units,NSTrck)
      else
C +---open BSQ binary file
C +   --------------------
C +---characteristics of the DEM grid
C +   -------------------------------
          if (name.eq.'Bamber') then
              dem_mx = 5601
              dem_my = 5601
              dem_x0 = -2800.
              dem_y0 =  2800.
              dem_dh =  1.
          else if (name.eq.'bedmap') then
              dem_mx = 6667
              dem_my = 6667
              dem_x0 = -3333.
              dem_y0 = -3333.
              dem_dh  =  1.
          endif
          allocate(dem_sh(dem_mx,dem_my))
          allocate(dem_msk(dem_mx,dem_my))
          allocate(dem_rck(dem_mx,dem_my))
      
          if (name.eq.'Bamber') then
C +---    opening and reading Bamber 2009 data file
C +       =========================================
              write(6,*) 'Topography : Bamber(2009) Antarctic 1km DEM'
              write(6,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
              dem_file = trim(PFXdir)//'AntTOPO/krigged_dem_nsidc.bin'
              write(6,*) "> Read ", dem_file
              write(6,*)
              Inquire(iolength=recl) dem_sh
              open (unit=10,status='old',file=dem_file,
     .              form='unformatted',access='direct',recl=recl)
              Read (10,rec=1) dem_sh
              close(10)
              dem_msk = 0.
              dem_rck = 0.
          else if (name.eq.'bedmap') then
C +---    opening and reading bedmap2 data file
C +       =========================================
              write(6,*) 'Topography : bedmap2 Antarctic 1km DEM'
              write(6,*) '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
              dem_file = trim(PFXdir)//'AntTOPO/bedmap2.nc'
              write(6,*) "> Read ", dem_file
              write(6,*)
              call UNropen (dem_file,fID,dem_file)
              call UNsread(fID,'sh',1,1,1,1,
     .                     dem_mx,dem_my,1,
     .                     var_units,dem_sh)
              call UNsread(fID,'icemask',1,1,1,1,
     .                     dem_mx,dem_my,1,
     .                     var_units,dem_msk)
              call UNsread(fID,'rockmask',1,1,1,1,
     .                     dem_mx,dem_my,1,
     .                     var_units,dem_rck)
               do jj=1,dem_my
               do ii=1,dem_mx
                   if (dem_msk(ii,jj).lt.-999) then
                       dem_msk(ii,jj) = 0.
                   else if (dem_msk(ii,jj).eq.0.) then
                        dem_msk(ii,jj) = 1.
                   else if (dem_msk(ii,jj).eq.1.) then
                        dem_msk(ii,jj) = 0.
                   endif
                   if (dem_rck(ii,jj).lt.-999) then
                        dem_rck(ii,jj) = 0.
                   else if (dem_rck(ii,jj).eq.0.) then
                         dem_rck(ii,jj) = 1.
                   endif
                enddo
                enddo
          endif
C +---Verify than the map projection used for MAR 
C +   is the same than in the DEM
C +   ========================================
          open (unit=51,status='old',file='MARgrd.ctr')
           read (51,*) !- - - - - - - - - - - - - - - - - -
           read (51,*) !- - - - - - - - - - - - - - - - - -
           read (51,*) !- - - - - - - - - - - - - - - - - -
           read (51,*) !- - - - - - - - - - - - - - - - - -
           read (51,*) maptyp
           read (51,*) !- - - - - - - - - - - - - - - - - -
           read (51,*) nul
           read (51,*) imez  
           read (51,*) nul
           read (51,*) jmez 
           read (51,*) !- - - - - - - - - - - - - - - - - -
           read (51,*) nul
           read (51,*) !- - - - - - - - - - - - - - - - - -
           read (51,*) GEddxx
           read (51,*) !- - - - - - - - - - - - - - - - - -
          close(unit=51)
          
          if (maptyp.ne.0) then
            Print*, "++++++++++++++++++++++++++++++++++"
            Print*, "Routine USRant.f ----> Warning !!!"
            Print*, "For Antarctica,                   "
            Print*, "choose map type = 0 in MARgrd.ctr "
            Print*, "(Standard stereo south projection)"
            Print*, "NESTOR stopped NOW !!!            "
            Print*, "++++++++++++++++++++++++++++++++++"
            STOP
          endif
          
          if (GEddxx.eq.90) then
            do j=1,my
            do i=1,mx
              NST_yy(i,j) = NSTgdy(j)
              NST_xx(i,j) = NSTgdx(i)
            enddo
            enddo
          else
            do j=1,my
            do i=1,mx
              lon = NST__x(i,j)
              lat = NST__y(i,j)
              call StereoSouth_inverse(lon,lat,90.,xx,yy)
              NST_xx(i,j) = xx
              NST_yy(i,j) = yy
            enddo
            enddo
          endif
          
          call StereoSouth_inverse(0.,-71.,GEddxx,x0,y0)
          xl = x0 - NST_dh/2.
          xr = x0 + NST_dh/2.
          yl = y0 - NST_dh/2.
          yu = y0 + NST_dh/2.
          call areaLambertAzimuthal(xl,xr,yl,yu,GEddxx,ref_area)
          do j=1,my
          do i=1,mx
              xl = NSTgdx(i) - NST_dh/2.
              xr = NSTgdx(i) + NST_dh/2.
              yl = NSTgdy(j) - NST_dh/2.
              yu = NSTgdy(j) + NST_dh/2.
              call areaLambertAzimuthal(xl,xr,yl,yu,GEddxx,area)
              NSTarea(i,j) = area/ref_area
          enddo
          enddo
C +---Average the DEM on the MAR grid
C +   ===============================
          ndh     = nint(NST_dh/dem_dh)
          di0     = int((ndh-1)/2.)
          dj0     = int((ndh-1)/2.)
          xx_min = dem_x0 + NST_dh/2.
          xx_max = -dem_x0 - NST_dh/2.
          print*, ndh,di0,dj0,xx_min,xx_max
          do j=1,my
          do i=1,mx

            if (NST__y(i,j).lt.-58) then !ckittel /Bedmap2 over Antarctica, ETOPOGO elsewhere
             sh  = 0.
             grd = 0.
             rck = 0.
             ice = 0.
              if (     NST_xx(i,j).lt.xx_min 
     .            .or. NST_xx(i,j).gt.xx_max
     .            .or. NST_yy(i,j).lt.xx_min 
     .            .or. NST_yy(i,j).gt.xx_max) then
               NSTgrd(i,j) = 0.
               NST_sh(i,j) = 0.
               NSTice(i,j) = 0.
               NSTrck(i,j) = 0.
               NSTsol(i,j) = 1
              else
               dem_ii = nint(NST_xx(i,j) - dem_x0)
               if (name.eq.'Bamber') then
                  dem_jj = nint(dem_y0 - NST_yy(i,j))
               else if (name.eq.'bedmap') then
                  dem_jj = nint(NST_yy(i,j) - dem_y0)
               endif
               do jn=1,ndh
               do in=1,ndh
                  ii = dem_ii - di0 + in
                  jj = dem_jj - dj0 + jn
                  if (dem_sh(ii,jj).ge.-990.) then
                   ice = ice + 1
                   sh  = sh + dem_sh(ii,jj)
                  endif
                  grd = grd + dem_msk(ii,jj)
                  rck = rck + dem_rck(ii,jj)
               enddo
               enddo
               NST_sh(i,j) = sh  / (ndh*ndh)
               ! +---No atmosphere below sea level
               ! +   -----------------------------
               NST_sh(i,j) = max(NST_sh(i,j),0.)
               NSTice(i,j) = ice / (ndh*ndh) *100.
               NSTgrd(i,j) = grd / (ndh*ndh) *100.
               NSTrck(i,j) = rck / (ndh*ndh) *100.
               if (NSTice(i,j).ge.30.) then
                 NSTsol(i,j) = 3
               else
                 NSTsol(i,j) = 1
                 NST_sh(i,j) = 0.
               endif
             endif
           endif
          enddo
          enddo
      endif

      return
      END subroutine AntTOPO

! +----------------------------------------------------------------------+
      subroutine StereoSouth_inverse (lon,lat,lonE,E,N)
! |  Compute Oblique Stereographic Projection from lon,lat coordinates   |
! |  Written by Cecile Agosta                                 17-05-10   |
! |  EPSG Polar Stereographic transformation Variant B                   |
! |  (http://www.epsg.org/guides/docs/G7-2.pdf)                          |
! |  Equivalent to EPSG 3031 (WGS-84 ellipsoid)                          |
! +----------------------------------------------------------------------+
! |                                                                      |
! | INPUT : lon     : Longitude (deg)                                    |
! | ^^^^^^^ lat     : Latitude  (deg)                                    |
! |         lon0    : Longitude of X axis (90 = East, clockwise)         |
! |        [lat true = 71 S]                                             |
! |                                                                      |
! | OUTPUT : E      : Stereo coordinate on the East  (X, km)             |
! | ^^^^^^^^ N      : Stereo coordinate on the North (Y, km)             |
! |                                                                      |
! +----------------------------------------------------------------------+
      implicit none

      include 'NSTdim.inc'

! +-- General Variables
! +   -----------------
      real,intent(in ) :: lon,lat,lonE
      real,intent(out) :: E,N

! +-- Local Variables
! +   ---------------
      real  costru,ddista

! +-- Constants
! +   ---------
      real  aa,ex,pi,degrad,latF,FE,FN,tF,mF,k0,t,rho,lonrad,latrad
      real  lon0,trulat
      
      aa     = 6378.1370         ! aa (km) = demi grand axe
      ex     = 0.081819190842621 ! excentricity WGS-84 : 0.081 819 190 842 622 0.081 819 190 842 621
      trulat = -71.              ! Latitude of standard parallel, 71 S for ESPG 3031
      pi     = 4. * atan(1.)
      degrad = pi / 180.

      latF   = trulat*degrad ! Latitude of standard parallel, 71 for ESPG 3031  
      lon0   = (lonE - 90.)*degrad
      lonrad = lon *degrad
      latrad = lat *degrad      
      
      FE = 0. !False Easting
      FN = 0. !False Northing

! +
! +- Polar Stereographic Projection
! +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! +
      tF  = tan (pi/4 + latF  /2) / 
     .      ( (1 + ex*sin(latF  )) / (1 - ex*sin(latF  )) )**(ex/2)
      mF  = cos(latF) / (1 - ex**2 * sin(latF)**2)**0.5
      k0  = mF*( (1+ex)**(1+ex) * (1-ex)**(1-ex) )**0.5 / (2*tF)
      
      t   = tan (pi/4 + latrad/2) / 
     .      ( (1 + ex*sin(latrad)) / (1 - ex*sin(latrad)) )**(ex/2)
      rho = 2*aa*k0*t / ( (1+ex)**(1+ex) * (1-ex)**(1-ex) )**0.5
      
      E = FE + rho*sin (lonrad - lon0)
      N = FN + rho*cos (lonrad - lon0)

      return
      end subroutine StereoSouth_inverse
C +------------------------------------------------------------------------+

! +----------------------------------------------------------------------+
      subroutine LambertAzimuthalEqualArea_inv(lon,lat,xx,yy)
! |  Compute Oblique Stereographic Projection from lon,lat coordinates   |
! |  Written by Cecile Agosta                                 17-05-10   |
! |  EPSG Polar Stereographic transformation Variant B                   |
! |  (http://www.epsg.org/guides/docs/G7-2.pdf)                          |
! |  Equivalent to EPSG 3031 (WGS-84 ellipsoid)                          |
! +----------------------------------------------------------------------+
! |                                                                      |
! | INPUT : lon     : Longitude (deg)                                    |
! | ^^^^^^^ lat     : Latitude  (deg)                                    |
! |                                                                      |
! | OUTPUT : xx     : coordinate on the East  (X, km)                    |
! | ^^^^^^^^ yy     : coordinate on the North (Y, km)                    |
! |                                                                      |
! +----------------------------------------------------------------------+
      implicit none

      include 'NSTdim.inc'

! +-- General Variables
! +   -----------------
      real,intent(in ) :: lon, lat
      real,intent(out) :: xx, yy

      real lonrad,latrad
      real kp1,kp

! +-- Constants
! +   ---------
      real pi,degrad
      real lon0,lat0

      pi     = 4. * atan(1.)
      degrad = pi / 180.

      lon0   = 0.*degrad
      lat0   = -90.*degrad

      lonrad = lon *degrad
      latrad = lat *degrad 

! +
! +- Polar Stereographic Projection
! +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! +
      kp1 = 1 + sin(lat0)*sin(latrad) + 
     .          cos(lat0)*cos(latrad)*cos(lonrad-lon0)
      kp = sqrt(2/kp1)
      
      xx = kp*cos(latrad)*sin(lonrad-lon0)
      yy = kp*(cos(lat0)*sin(latrad) - 
     .         sin(lat0)*cos(latrad)*cos(lonrad-lon0))
      
      return
      end subroutine LambertAzimuthalEqualArea_inv
C +------------------------------------------------------------------------+
      
      subroutine areaLambertAzimuthal(xl,xr,yl,yu,GEddxx,area)
          
          implicit none
          
          real,intent(in)::xl,xr,yl,yu,GEddxx
          real,intent(out)::area
          
          real lonll,latll,lonlu,latlu
          real lonrl,latrl,lonru,latru
          real xll,yll,xlu,ylu
          real xrl,yrl,xru,yru
          real l1,l2,l3
          real dp
          
          call StereoSouth(xl,yl,GEddxx,lonll,latll)
          call StereoSouth(xl,yu,GEddxx,lonlu,latlu)
          call StereoSouth(xr,yl,GEddxx,lonrl,latrl)
          call StereoSouth(xr,yu,GEddxx,lonru,latru)
          
          call LambertAzimuthalEqualArea_inv(lonll,latll,xll,yll)
          call LambertAzimuthalEqualArea_inv(lonlu,latlu,xlu,ylu)
          call LambertAzimuthalEqualArea_inv(lonrl,latrl,xrl,yrl)
          call LambertAzimuthalEqualArea_inv(lonru,latru,xru,yru)
          
          l1 = sqrt((xll-xlu)**2+(yll-ylu)**2)
          l2 = sqrt((xll-xrl)**2+(yll-yrl)**2)
          l3 = sqrt((xlu-xrl)**2+(ylu-yrl)**2)
          dp = (l1+l2+l3)/2.
          area = sqrt(dp*(dp-l1)*(dp-l2)*(dp-l3))
          l1 = sqrt((xru-xlu)**2+(yru-ylu)**2)
          l2 = sqrt((xru-xrl)**2+(yru-yrl)**2)
          dp = (l1+l2+l3)/2.
          area = area + sqrt(dp*(dp-l1)*(dp-l2)*(dp-l3))
      end subroutine areaLambertAzimuthal
