      program Nb_Proc_PERMIS

!------------------------------------------------------------------------------+
!                                                                              |
!     Nb_Proc_PERMIS: marp: Table des Processeurs autorises                    |
!     mxtt            is prescribed in MAR_control.dat                         |
!     mxxtt = mxtt+1  is the nb of points along the x-axis, minus 1            |
!     mxxtt+1         is the nb of points along the x-axis,                    |
!                     is prescribed in NSTdim.inc                              |
!                                                                              |
!------------------------------------------------------------------------------+

      integer           ::    mxxtt
      integer           ::    nb_pr
      integer           ::    nbrOx = 2
      integer           ::    mxxt
      integer           ::    mxx
      character(len=1)  ::    ch

          WRITE(*,6)nbrOx,(nb_pr,nb_pr=1,24)
 6        format(/,'Nb Pts / Proc-1 (i.e., mxx), HALO =',i2,           &
     &           /,'Nb Proc-->    ', 25i5)
      DO             mxxtt = 11,650      
        IF      (mod(mxxtt,20).EQ.0)                                THEN
          WRITE(*,7)
 7        FORMAT(8x,'(1st,2nd,3rd) column = (mx in NSTdim.inc,'        &
     &          ,' mxtt in MAR_control.dat, mxx if NO subdomain)')
          WRITE(*,6)nbrOx,(nb_pr,nb_pr=1,24)
        END IF

        IF      (mod(mxxtt,20).EQ.0)                                THEN
          WRITE(*,1) mxxtt+1,mxxtt-1
 1        format('Nb Pts|',2i4,$)
        ELSE IF (mod(mxxtt,20).EQ.1)                                THEN
          WRITE(*,2) mxxtt+1,mxxtt-1
 2        format('MARdim|',2i4,$)
        ELSE IF (mod(mxxtt,20).EQ.2)                                THEN
          WRITE(*,3) mxxtt+1,mxxtt-1
 3        format('(.NE. |',2i4,$)
        ELSE IF (mod(mxxtt,20).EQ.3)                                THEN
          WRITE(*,4) mxxtt+1,mxxtt-1
 4        format(' Dom.)v',2i4,$)
        ELSE
          WRITE(*,5) mxxtt+1,mxxtt-1
 5        format('       ',2i4,$)
        END  IF

        DO                                        nb_pr = 1,24
            i    =           mod(mxxtt+1-2*nbrOx ,nb_pr)
            mxxt =   mxxtt - mod(mxxtt+1-2*nbrOx ,nb_pr)
            mxt  =   mxxt  - 1
            mxx  = ((mxxt +  1 -2*nbrOx)/nb_pr)+2*nbrOx-1
          IF (i .EQ. 0 .AND. mxx .GT. 7)                            THEN
            mxxt =   mxxtt - mod(mxxtt+1-2*nbrOx ,nb_pr)
            mxt  =   mxxt  - 1
            mxx  = ((mxxt +  1 -2*nbrOx)/nb_pr)+2*nbrOx-1
            ch   = '#'
          ELSE
            mxxt =   0
            mxx  =   0
            ch   = ' '
          END IF
          WRITE(*,10)mxx,ch
 10       format(i4,a1,$)
        ENDDO
          WRITE(*,*) " "
      ENDDO

      END
